/*
 * This is a script to add features to pages created with Tiendanube
 * This script is created by @ArenasAgustin
 * The actual version is 3.1.0
 */

// Default values
let maximumOrder = null,
  availableCart = null;
const stringWarning = `
<div class="alert alert-warning" role="alert">
    Tenes un monto máximo de compra por mes de $0,00
</div>
`;

// Funtion to format te price
const formatPrice = (price) => {
  // Parse the price to float
  let string = `$${price.toFixed(2)}`;

  // Replace the dots for commas and the commas for dots
  string = string.replace(/\B(?=(\d{3})+(?!\d))/g, '_');
  return string.replace('.', ',').replace(/_/g, '.');
};

// Funtion to set te maximum alert
const setMaximumAlert = (alert) => {
  // Get the inner HTML
  let innerHTML = alert.innerHTML.trim().split(' ');

  // Set the price
  if (availableCart) {
    innerHTML.forEach((element, index) => {
      if (element.includes('$')) innerHTML[index] = formatPrice(availableCart);
    });
  }

  // Set the new text
  alert.innerHTML = innerHTML.join(' ');
};

// Function to disable the cart
const disableCart = (subTotal, alert) => {
  // Get the button
  const btn = document.getElementById('btnFinishCheckout');

  if ((maximumOrder && availableCart > subTotal) || maximumOrder === 0) {
    // If the maximum order exist and the avalible cart is greater than subtotal or the maximum order is 0
    // show the buttion and hide the alert
    btn.setAttribute('style', '');
    if (alert.length)
      alert.forEach((element) => {
        element.parentNode.setAttribute('style', 'display: none;');
        setMaximumAlert(element);
      });
  } else {
    // Else hide the button and show the alert
    btn.setAttribute('style', 'display: none;');
    if (alert.length)
      alert.forEach((element) => {
        element.parentNode.setAttribute('style', 'text-align: center;');
        setMaximumAlert(element);
      });
  }
};

// Function to get the minimum amount of the cart, the minimum units and the type of minimum
const getMaximun = async (customer_id, store_id, subTotal, alerts) => {
  await fetch('https://api.sellerslatam.com/affiliates/check_maximum/', {
    method: 'POST',
    body: JSON.stringify({ store_id, customer_id }),
    async: true,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
    .then((response) => response.json())
    .then((data) => {
      maximumOrder = data.data.maximum_order;
      availableCart = data.data.available;
      disableCart(subTotal, alerts);
    })
    .catch((e) => console.error(e));
};

// Function to add event in the dom
const domReady = (fn) => {
  document.addEventListener('DOMContentLoaded', fn);
  if (document.readyState === 'interactive' || document.readyState === 'complete') fn();
};

// Function that is executed when the dom is ready
domReady(async () => {
  // Default variables
  const customer_id = LS?.customer,
    store_id = LS?.store?.id,
    subTotal = LS?.cart?.subtotal / 100;

  // Get the alerts
  let alerts = document.querySelectorAll('.alert.alert-warning');

  if (!alerts.length) {
    // If alerts is empty get the container
    let alertContainer = document.querySelectorAll('.summary-coupon');

    // Create an alert
    const div = document.createElement('div');
    div.innerHTML = stringWarning;
    div.setAttribute('class', 'js-ajax-cart-minimum clear-both p-top-half');
    div.setAttribute('style', 'display: none;');

    // Insert the alerts in the dom
    alertContainer.forEach((element) => element.appendChild(div));
    alerts = document.querySelectorAll('.alert.alert-warning');
  }

  // If customer id get the maximum avalible to buy
  if (customer_id) await getMaximun(customer_id, store_id, subTotal, alerts);
});
