/*
 * This is a script to add features to pages created with Tiendanube
 * This script is created by @FrancoDodera
 * The current version is 0.0.0.3
 */

(function () {
  useJquery().then(() => {
    try {
      console.info('Script version: 0.0.0.3');
      console.info('Script created by @FrancoDodera');

      /**
       * Function to check if two elements are equal.
       *
       * @param {*} a - First element.
       * @param {*} b - Second element.
       * @returns {boolean} - Returns true if two elements are equal, false otherwise.
       */
      const equals = (a, b) => a == b;

      /**
       * Configuration object for different themes.
       * @typedef {Object} ThemeConfig
       * @property {string} productContainer - Selector for the product container element.
       * @property {string} switchClass - Selector for the class used for switching.
       * @property {string} switchClassSearch - Selector for the search switch class.
       * @property {string} switchClassHome - Selector for the home switch class.
       * @property {string} inputPriceSelector - Selector for the input price element.
       * @property {string} cartQuantityInputSelector - Selector for the cart quantity input element.
       * @property {string} cardsSelector - Selector for the cards container.
       * @property {string} cardImgSelector - Selector for the card image element.
       * @property {string} cardDescriptionSelector - Selector for the card description element.
       * @property {string} stringSwitchStyles - String representing the switch styles.
       * @property {string} stringPriceClasses - String representing the price classes.
       * @property {string} stringBtnAddToCartStyles - String representing the button styles.
       * @property {string} stringQuantityInput - String representing the quantity input.
       * @property {string} stringVariants - String representing the variants.
       * @property {string} stringHeaders - String representing the headers.
       * @property {string} stringFormInner - String representing the inner form.
       */

      /**
       * Default variables to store data.
       *
       * @type {{
       *   defaultCatalog: HTMLElement,
       *   productsNotSaved: number,
       *   isSearch: boolean,
       *   pathName: string,
       *   cartsData: Array<any>,
       *   cartQuantity: number,
       *   minimumCart: null|number,
       *   minimumUnits: null|number,
       *   minimumType: null|boolean,
       *   require_login: null|boolean,
       * }}
       */

      let defaultVariables = {
        defaultCatalog: null,
        productsNotSaved: 0,
        isSearch: false,
        pathName: '',
        cartsData: [],
        cartQuantity: 0,
        minimumCart: null,
        minimumUnits: null,
        minimumType: null,
        require_login: false,
      };

      /**
       * Theme name obtained from localStorage.
       *
       * @type {string}
       */
      const themeName = LS.theme.name;

      /**
       * Language code obtained from localStorage.
       *
       * @type {string}
       */
      const language = LS.langCode;

      /**
       * Spanish translations.
       * @type {Object}
       * @property {string} imageTxt - Texto para la imagen.
       * @property {string} nameTxt - Texto para el nombre.
       * @property {string} priceTxt - Texto para el precio.
       * @property {string} amountTxt - Texto para el monto total.
       * @property {string} stockTxt - Texto para el stock.
       * @property {string} quantityTxt - Texto para la cantidad.
       * @property {string} addToCartTxt - Texto para agregar al carrito.
       * @property {string} clearCartTxt - Texto para vaciar el carrito.
       * @property {string} moneyTxt - Símbolo monetario.
       * @property {string} switchTxt - Texto para cambiar a compra rápida.
       * @property {string} switchTxt2 - Texto para cambiar a vista minorista.
       * @property {string} productLinkTxt - Texto para enlace a productos.
       * @property {string} buyLinkTxt - Texto para enlace de compra.
       * @property {string} alertMunimumTxt - Texto para la alerta de monto mínimo.
       * @property {string} auxShippingTxt - Texto adicional para costo de envío.
       * @property {string} auxAmoutTxt - Texto adicional para monto.
       * @property {string} notAvalibleTxt - Texto para no disponible.
       * @property {string} notStockTxt - Texto para sin stock.
       * @property {string} loadingTxt - Texto para cargando.
       * @property {string} alertBeforeTxt - Texto para alerta antes de salir.
       * @property {string} seeVariatsTxt - Texto para ver variantes.
       * @property {string} buyTxt - Texto para comprar.
       * @property {string} readyTxt - Texto para listo.
       * @property {string} addTxt - Texto para agregando.
       */

      /**
       * Translations object containing language-specific text.
       * @type {Object.<string, Object>}
       */
      const translations = {
        /**
         * Spanish translations.
         * @type {Object}
         */
        es: {
          imageTxt: 'Imagen',
          nameTxt: 'Nombre',
          priceTxt: 'Precio',
          amountTxt: 'Monto total',
          stockTxt: 'Stock',
          quantityTxt: 'Cantidad',
          addToCartTxt: 'Agregar al carrito',
          clearCartTxt: 'Vaciar carrito',
          moneyTxt: '$',
          switchTxt: 'Cambiar a compra rápida',
          switchTxt2: 'Cambiar a vista minorista',
          productLinkTxt: 'productos',
          buyLinkTxt: 'comprar',
          alertMunimumTxt: 'El monto mínimo de compra es de $0,00 sin incluir el costo de envío',
          auxShippingTxt: 'sin incluir el costo de envío',
          auxAmoutTxt: 'comprar',
          notAvalibleTxt: 'No disponible',
          notStockTxt: 'Sin stock',
          loadingTxt: 'Cargando',
          alertBeforeTxt: '¿Estás seguro de que quieres salir, tienes productos para agregar al carrito?',
          seeVariatsTxt: 'Ver variantes',
          buyTxt: 'Comprar',
          readyTxt: 'Listo',
          addTxt: 'Agregando',
        },
        /**
         * Portuguese translations.
         * @type {Object}
         */
        pt: {
          imageTxt: 'Imagem',
          nameTxt: 'Nome',
          priceTxt: 'Preço',
          amountTxt: 'Valor total',
          stockTxt: 'Estoque',
          quantityTxt: 'Quantia',
          addToCartTxt: 'Adicionar ao carrinho',
          clearCartTxt: 'Limpar o carrinho',
          moneyTxt: 'R$',
          switchTxt: 'Formato de compra por atacado',
          switchTxt2: 'Mudar para o formato clássico',
          productLinkTxt: 'produtos',
          buyLinkTxt: 'comprar',
          alertMunimumTxt: 'O valor mínimo de compra é R$0,00 não incluindo o custo de envio',
          auxShippingTxt: 'sem considerar o custo de frete',
          auxAmoutTxt: 'valor',
          notAvalibleTxt: 'Não disponível',
          notStockTxt: 'Sem estoque',
          loadingTxt: 'Carregando',
          alertBeforeTxt: 'Tem certeza que deseja finalizar a compra, tem produtos para adicionar ao carrinho?',
          seeVariatsTxt: 'Ver variantes',
          buyTxt: 'Comprar',
          readyTxt: 'Pronto',
          addTxt: 'Adicionando',
        },
        /**
         * Default translations.
         * @type {Object}
         */
        default: {
          imageTxt: 'Image',
          nameTxt: 'Name',
          priceTxt: 'Price',
          amountTxt: 'Total amount',
          stockTxt: 'Stock',
          quantityTxt: 'Quantity',
          addToCartTxt: 'Add to cart',
          clearCartTxt: 'Clear cart',
          moneyTxt: '$',
          switchTxt: 'Switch to quick buy',
          switchTxt2: 'Switch to retail view',
          productLinkTxt: 'products',
          buyLinkTxt: 'buy',
          alertMunimumTxt: 'The minimum purchase amount is $0.00 not including shipping costs',
          auxShippingTxt: 'not including shipping costs',
          auxAmoutTxt: 'amount',
          notAvalibleTxt: 'Not available',
          notStockTxt: 'Not stock',
          loadingTxt: 'Loading',
          alertBeforeTxt: 'Are you sure you want to finish the purchase, do you have products to add to the cart?',
          seeVariatsTxt: 'See variants',
          buyTxt: 'Buy',
          readyTxt: 'Ready',
          addTxt: 'Adding',
        },
      };

      /**
       * Language-specific text translations.
       * @type {Object}
       */
      const translation = translations[language] || translations['default'];

      // Destructuring language-specific text translations
      /**
       * Language-specific text translations for easier access.
       * @type {{
       *   imageTxt: string,
       *   nameTxt: string,
       *   priceTxt: string,
       *   amountTxt: string,
       *   stockTxt: string,
       *   quantityTxt: string,
       *   addToCartTxt: string,
       *   clearCartTxt: string,
       *   moneyTxt: string,
       *   switchTxt: string,
       *   switchTxt2: string,
       *   productLinkTxt: string,
       *   buyLinkTxt: string,
       *   alertMunimumTxt: string,
       *   auxShippingTxt: string,
       *   auxAmoutTxt: string,
       *   notAvalibleTxt: string,
       *   notStockTxt: string,
       *   loadingTxt: string,
       *   alertBeforeTxt: string,
       *   seeVariatsTxt: string,
       *   buyTxt: string,
       *   readyTxt: string,
       *   addTxt: string
       * }}
       */
      const { imageTxt, nameTxt, priceTxt, amountTxt, stockTxt, quantityTxt, addToCartTxt, clearCartTxt, moneyTxt, switchTxt, switchTxt2, productLinkTxt, buyLinkTxt, alertMunimumTxt, auxShippingTxt, auxAmoutTxt, notAvalibleTxt, notStockTxt, loadingTxt, alertBeforeTxt, seeVariatsTxt, buyTxt, readyTxt, addTxt } = translation;

      // This is the HTML of the variants of the products
      const stringVariantsSimple = `
     <div class="col-auto full-width item-container d-flex ">
       <div class="js-item-image-container item-image-container span2 overflow-hidden" style="height: 100px;" id="item-image">
         <div class="js-item-image-padding p-relative" style="padding-bottom: 100%; padding-left: 20%;"></div>
       </div>
       <div class="span10 align-items-center" style="display: flex !important; margin: 0 !important;">
         <div class="js-item-name span3 item-name" style="margin: 0 !important;"></div>
         <div class="span1 px-2" style="margin: 0 !important;"></div>
         <div class="span4 px-2" style="margin: 0 !important;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
         <div class="span2 px-2" style="margin: 0 !important;"></div>
         <div class="span3" style="margin: 0 !important;">
           <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="cart-quantity-btn-substract">
             <div class="cart-quantity-svg-icon svg-icon-text" data-component="quantity.minus"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
           </span>
           <div class="cart-quantity-input-container d-inline-block pull-left">
             <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 5<path d="M483.515 28.485L431.35 80.65C386.475 35.767 324.485 8 256 8 123.228 8 14.824 112.338 8.31 243.493 7.971 250.311 13.475 256 20.301 256h28.045c6.353 0 11.613-4.952 11.973-11.294C66.161 141.649 151.453 60 256 60c54.163 0 103.157 21.923 138.614 57.386l-54.128 54.129c-7.56 7.56-2.206 20.485 8.485 20.485H492c6.627 0 12-5.373 12-12V36.971c0-10.691-12.926-16.045-20.485-8.486zM491.699 256h-28.045c-6.353 0-11.613 4.952-11.973 11.294C445.839 370.351 360.547 452 256 452c-54.163 0-103.157-21.923-138.614-57.386l54.128-54.129c7.56-7.56 2.206-20.485-8.485-20.485H20c-6.627 0-12 5.373-12 12v143.029c0 10.691 12.926 16.045 20.485 8.485L80.65 431.35C125.525 476.233 187.516 504 256 504c132.773 0 241.176-104.338 247.69-235.493.339-6.818-5.165-12.507-11.991-12.507z"></p</svg></span>
             <input class="cart-quantity-input"id="cart-quantity-input"type="number" name="quantity_aux_variants" value="0" min="0" step="1" style="width: 90px; height: 18px; border-top: 1px solid rgba(67, 67, 67, 0.8); border-bottom: 1px solid rgba(67, 67, 67, 0.8);"/>
           </div>
           <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="cart-quantity-btn-add">
             <div class="cart-quantity-svg-icon svg-icon-text"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
           </span>
         </div>
       </div>
     </div>
     `;

      // This is the HTML of the quantity input
      const stringQuantityInputSimple = `
          <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="substract-btn">
            <div class="cart-quantity-svg-icon svg-icon-text" data-component="quantity.minus"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
          </span>
          <div class="cart-quantity-input-container d-inline-block pull-left">
            <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M483.515 28.485L431.35 80.65C386.475 35.767 324.485 8 256 8 123.228 8 14.824 112.338 8.31 243.493 7.971 250.311 13.475 256 20.301 256h28.045c6.353 0 11.613-4.952 11.973-11.294C66.161 141.649 151.453 60 256 60c54.163 0 103.157 21.923 138.614 57.386l-54.128 54.129c-7.56 7.56-2.206 20.485 8.485 20.485H492c6.627 0 12-5.373 12-12V36.971c0-10.691-12.926-16.045-20.485-8.486zM491.699 256h-28.045c-6.353 0-11.613 4.952-11.973 11.294C445.839 370.351 360.547 452 256 452c-54.163 0-103.157-21.923-138.614-57.386l54.128-54.129c7.56-7.56 2.206-20.485-8.485-20.485H20c-6.627 0-12 5.373-12 12v143.029c0 10.691 12.926 16.045 20.485 8.485L80.65 431.35C125.525 476.233 187.516 504 256 504c132.773 0 241.176-104.338 247.69-235.493.339-6.818-5.165-12.507-11.991-12.507z"></path></svg></span>
            <input class="cart-quantity-input"type="number" name="quantity_aux" value="0" min="0" step="1" style="width: 70px; height: 18px; border-top: 1px solid rgba(67, 67, 67, 0.8); border-bottom: 1px solid rgba(67, 67, 67, 0.8);"/>
          </div>
          <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="add-btn">
            <div class="cart-quantity-svg-icon svg-icon-text"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
          </span>
          `;

      const themesConfig = {
        default: {
          productContainer: '.js-product-container',
          switchClass: '.js-category-breadcrumbs',
          switchClassSearch: '.breadcrumb',
          switchClassHome: '.subtitle-container',
          inputPriceSelector: '#item-price',
          cartQuantityInputSelector: '.cart-quantity-input',
          cardsSelector: '.item-container',
          cardImgSelector: '.js-item-image-container',
          cardDescriptionSelector: '.item-info-container',
          stringSwitchStyles: 'max-width:350px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:350px',
          stringQuantityInput: stringQuantityInputSimple,
          stringVariants: stringVariantsSimple,


          minimumCartSelector: 'h4',
        },
      };

      /**
       * Adds styles to an element.
       * @param {HTMLElement} element - The element to which styles will be applied.
       * @param {string} styles - The CSS styles to be applied.
       */
      const addStyles = (element, styles) => {
        element?.setAttribute('style', styles);
      };



      /**
       * Theme configuration object obtained based on the selected theme name.
       * @type {ThemeConfig}
       */
      const themeConfig = themesConfig[themeName] || themesConfig['default'];

      const { productContainer, switchClass, switchClassSearch, switchClassHome, btnStartBuy, inputPriceSelector, cartQuantityInputSelector, cardsSelector, cardImgSelector, cardDescriptionSelector, stringSwitchStyles, stringPriceClasses, stringBtnAddToCartStyles, stringQuantityInput, stringVariants, stringHeaders, stringFormInner, minimumCartSelector } = themeConfig;

      /**
       * Retrieves the quantity of items in the cart and updates the global variable `cartQuantity`.
       * @async
       */
      const saveQuantityCart = async () => {
        let data = 0;

        $('.js-cart-item').map((_, element) => {
          let aux = element.querySelector('.js-cart-quantity-input');
          data += parseInt(aux.value);
        });

        defaultVariables.cartQuantity = data;
      };

      /**
       * Determines the container class for the switch button based on the current path.
       * @returns {string} - The class name for the container of the switch button.
       */
      const setContainerSwitch = () => {
        if (equals(defaultVariables.pathName, '/search/')) {
          return switchClassSearch;
        }

        return equals(defaultVariables.pathName, '/') ? switchClassHome : switchClass;
      };

      /**
       * Handles the display of the minimum cart alert based on the provided elements.
       * @param {HTMLElement[]} elements - The elements to which the alert will be appended.
       */
      const handleMinimumCartAlert = (elements) => {
        const containerSwitchSelector = setContainerSwitch();
        const containerSwitch = document.querySelector(containerSwitchSelector);

        if (elements.length === 1) {
          let newWarning = createMinimumCartAlert(elements[0]);
          if (containerSwitch) {
            containerSwitch.appendChild(newWarning);
          }
        }
      };

            /**
       * Creates a minimum cart alert element based on the provided element.
       * @param {HTMLElement} element - The element to be cloned and appended to the alert.
       * @returns {HTMLElement} - The created minimum cart alert element.
       */
            const createMinimumCartAlert = (element) => {
              const newWarning = document.createElement('div');
              if (['Amazonas', 'Atlántico', 'Bahia', 'Cubo', 'Idea', 'Rio', 'Simple'].includes(themeName)) {
                newWarning.appendChild(element.cloneNode(true));
                addStyles(newWarning, 'width: 100% !important; display: flex !important; justify-content: center; align-items: center; text-decoration: none; margin-top: 10px; font-size: 14px;');
              } else {
                newWarning.appendChild(element.cloneNode(true));
                addStyles(newWarning?.children[0], 'max-width:400px;');
              }
              return newWarning;
            };


      const getMinimunCart = async () => {
        try {
          const response = await $.ajax({
            url: `https://api.sellerslatam.com/affiliates/minimum_cart?id=${LS.store.id}`,
            method: 'GET',
            dataType: 'json',
          });

          defaultVariables.minimumCart = response.data.minimum_cart;
          defaultVariables.minimumUnits = response.data.minimum_units;
          defaultVariables.minimumType = response.data.minimum_type ? 'units' : 'price';
          defaultVariables.require_login = response.data.require_login;
        } catch (error) {
          console.error(error);
        }
      };

      /**
       * Disables the button to start the purchase process and attaches event listeners to relevant elements.
       * This function waits for the minimum cart information to be retrieved before attaching event listeners.
       * @async
       */
      const disableBtnStartBuy = async () => {
        // Get the minimum amount of the cart
        await getMinimunCart();
        await saveQuantityCart();
        /**
         * Adds a click event listener to the given element if not already added
         * @param {HTMLElement} element - The element to add the click event to
         */
        const addClickEvent = (element) => {
          if (!element?.addClick && element) {
            element.addEventListener('click', disableBtnStartBuy);
            element.addClick = true;
          }
        };

        /**
         * Adds a change event listener to the given element if not already added
         * @param {HTMLElement} element - The element to add the change event to
         */
        const addChangeEvent = (element) => {
          if (!element?.addChange && element) {
            element.addEventListener('change', disableBtnStartBuy);
            element.addChange = true;
          }
        };

        /**
         * Attaches click and change event listeners to specified buttons and quantity input elements
         */
        const attachEvents = () => {
          const buttons = ['.ajax-cart-btn-delete', '.js-prod-submit-form', '.cart-item-delete', '.btn.btn-link.btn-link-underline.ml-4', '.ajax-cart-quantity-btn:not(.not-cart-quantity-btn)', '.js-cart-quantity-btn:not(.not-cart-quantity-btn)', '.cart-item-delete .btn'];

          // Attach click event listeners to buttons
          buttons.forEach((selector) => {
            document.querySelectorAll(selector).forEach(addClickEvent);
          });

          // Attach change event listeners to quantity input elements
          document.querySelectorAll('.js-cart-quantity-input').forEach(addChangeEvent);
        };

        // Attach event listeners
        attachEvents();

        // Delay execution of functionToDisableBtnStartBuy and re-attach event listeners
        setTimeout(() => {
          functionToDisableBtnStartBuy();
          attachEvents();
        }, 2000);
      };

      // Obtén la URL actual
      var url = window.location.href;

      // Divide la URL por "/"
      var urlParts = url.split('/');

      // Encuentra la posición de "productos" en las partes de la URL
      var indexProducts = urlParts.indexOf('productos');

      // Verifica si "productos" está en la URL y si hay algo después de "productos"
      if (indexProducts !== -1 && indexProducts < urlParts.length - 1) {
        // Hay algo después de "productos"
        var somethingAfterProducts = urlParts[indexProducts + 1];

        // Verifica si somethingAfterProducts comienza con "?"
        if (somethingAfterProducts.startsWith('?')) {
        } else {
          // Estás en un producto específico
          getMinimunCart();
          disableBtnStartBuy();
        }
      }

      /**
       * Function to disable the button to start the purchase
       * @async
       */
      const functionToDisableBtnStartBuy = async () => {
        await saveQuantityCart();
        const btnStartBuy = document.querySelector('#ajax-cart-submit-div')?.children[0];
        const subTotal = (parseInt(document.querySelector('.js-cart-subtotal')?.dataset?.priceraw) / 100).toFixed(2) || 0;
        const btnStartBuy2 = document.querySelector('a.btn.btn-primary.btn-medium.w-100.px-1.d-block');
        const aux = document.querySelectorAll('#ajax-cart-minumum-div');

        if (!btnStartBuy) {
          return;
        }

        const isGuestUser = LS.customer;
        const isMinimumCartViolated = subTotal < defaultVariables.minimumCart && defaultVariables.minimumType === 'price';
        const isMinimumUnitsViolated = defaultVariables.cartQuantity < defaultVariables.minimumUnits && defaultVariables.minimumType === 'units';

        if (!isGuestUser || isGuestUser) {
          handleMinimumCartAlert(aux);
          disableOrEnablePurchaseButton(aux, btnStartBuy, btnStartBuy2, isMinimumCartViolated, isMinimumUnitsViolated);
        } else {
          resetPurchaseButtonAndHideWarning(aux, btnStartBuy, btnStartBuy2);
        }
      };

      /**
       * Disables or enables the purchase button based on the cart minimum violation and updates the button's link.
       * @param {HTMLElement[]} aux - The auxiliary elements related to the purchase button.
       * @param {HTMLElement} btnStartBuy - The main purchase button element.
       * @param {HTMLElement} btnStartBuy2 - The secondary purchase button element.
       * @param {boolean} isMinimumCartViolated - Flag indicating if the minimum cart amount is violated.
       * @param {boolean} isMinimumUnitsViolated - Flag indicating if the minimum units requirement is violated.
       */
      const disableOrEnablePurchaseButton = (aux, btnStartBuy, btnStartBuy2, isMinimumCartViolated, isMinimumUnitsViolated) => {
        aux.forEach((element) => {
          if (isMinimumCartViolated || isMinimumUnitsViolated) {
            displayMinimumCartAlert(element, minimumCartSelector, auxShippingTxt, auxAmoutTxt, moneyTxt);
          } else {
            addStyles(element, 'display: none;');
          }
        });

        btnStartBuy.disabled = isMinimumCartViolated || isMinimumUnitsViolated;

        if (btnStartBuy2) {
          btnStartBuy2.setAttribute('href', isMinimumCartViolated || isMinimumUnitsViolated ? `/${buyLinkTxt}/` : '');
        }
      };

      /**
       * Displays the minimum cart alert message and updates its content based on the violation.
       * @param {HTMLElement} element - The element containing the alert message.
       * @param {string} minimumCartSelector - The selector for the minimum cart container.
       * @param {string} auxShippingTxt - The auxiliary shipping text.
       * @param {string} auxAmoutTxt - The auxiliary amount text.
       * @param {string} moneyTxt - The currency symbol.
       */
      const displayMinimumCartAlert = (element, minimumCartSelector, auxShippingTxt, auxAmoutTxt, moneyTxt) => {
        addStyles(element, 'display: flex; justify-content: center; align-items: center; width: 100%;');

        let txtContainer = null;
        let auxInnerHTML = null;
        let innerHTML = [];

        if (['Lifestyle', 'Material', 'Silent', 'Simple'].includes(themeName)) {
          txtContainer = element.querySelector(minimumCartSelector);
        } else {
          txtContainer = element;
        }

        if (txtContainer) {
          auxInnerHTML = equals(defaultVariables.minimumType, 'units') ? txtContainer.innerHTML.replace(`${auxShippingTxt}`, '') : txtContainer.innerHTML;
          innerHTML = auxInnerHTML.trim().replace(`${auxAmoutTxt}`, '').split(' ');

          innerHTML.forEach((element, index) => {
            if (element.includes(`${moneyTxt}`)) {
              innerHTML[index] = equals(defaultVariables.minimumType, 'units') ? `${defaultVariables.minimumUnits} unidades` : formatPrice(defaultVariables.minimumCart);
            }
          });

          txtContainer.innerHTML = innerHTML.join(' ');
        }
      };

      /**
       * Creates and inserts the alert for minimum cart amount if the theme is Cubo.
       * @returns {HTMLDivElement | null} The created alert element or null if theme is not Cubo.
       */
      const createAlertMinimunCart = () => {
        if (equals(themeName, 'Cubo')) {
          const alertMinimunCart = document.createElement('div');
          alertMinimunCart.id = 'ajax-cart-minumum-div';
          addStyles(alertMinimunCart, 'display: none;');
          alertMinimunCart.innerHTML = `<div class="alert alert-warning">${alertMunimumTxt}</div>`;

          let auxAlertMinimunCart = document.querySelector('.js-ajax-cart-submit').parentNode;
          auxAlertMinimunCart.parentNode.insertBefore(alertMinimunCart, auxAlertMinimunCart);

          return alertMinimunCart;
        }

        return null;
      };

      // Create and insert the alert for minimum cart amount
      const alertMinimunCart = createAlertMinimunCart();

      if (LS.variants.length > 1) {
        // Select elements
        const forms = document.querySelector('.js-product-form');
        if (forms) {
          forms.style.width = 'auto';
        }
      
        let newButton = document.createElement('input');
        newButton.setAttribute('type', 'button');
        newButton.setAttribute('value', 'Agregar al carrito');
        let cartParent = document.querySelector('[data-store="product-buy-button"]').parentElement;
        if (cartParent) {
          cartParent.appendChild(newButton);
        }
      
        let containers = document.getElementsByClassName('form-row mb-2');
      
        if (containers.length > 0) {
          let container = containers[0];
          container.style.marginTop = '-120px';
          container.style.marginBottom = '10px';
          container.style.flexDirection = 'column-reverse';
          container.style.alignContent = 'center';
          container.style.alignItems = 'center';
        }
      
        let latestProductsAvailable = document.querySelector('.js-latest-products-available');
        if (latestProductsAvailable) {
          latestProductsAvailable.style.display = 'none';
        }
      
        let jsQuantity = document.querySelector('.js-quantity');
        if (jsQuantity) {
          jsQuantity.style.display = 'none';
        }
      
        let jsProductStockElements = document.getElementsByClassName('font-small py-2 text-center');
        if (jsProductStockElements.length > 0) {
          jsProductStockElements[0].style.display = 'none'; // Ocultar el primer elemento
        }
      
        let productVariants = document.getElementsByClassName('js-product-variants');
        if (productVariants.length > 0) {
          productVariants[0].style.display = 'none';
        }
      
        let fontSmallTextCenter = document.getElementsByClassName('font-small pt-2 pb-3 text-center');
        if (fontSmallTextCenter.length > 0) {
          fontSmallTextCenter[0].style.display = 'none';
        }
      
        let lastProduct = document.querySelector('.js-last-product');
        if (lastProduct) {
          lastProduct.style.display = 'none';
        }
      
        let productStock = document.querySelector('.js-product-stock');
        if (productStock) {
          productStock.style.display = 'none';
        }
      
        let productDescription = document.querySelector('.product-description');
        if (productDescription) {
          productDescription.style.marginBottom = '100px';
        }
      
        let cartButton = document.querySelector('[data-store="product-buy-button"]');
        if (cartButton) {
          let buttonContainer = document.createElement('div');
          buttonContainer.classList.add('button-container');
          cartButton.parentNode.insertBefore(buttonContainer, cartButton);
          buttonContainer.appendChild(cartButton);
          buttonContainer.style.display = 'none';
      
          let buttonOriginalStyles = window.getComputedStyle(cartButton);
      
          newButton.style.backgroundColor = buttonOriginalStyles.backgroundColor;
          newButton.style.color = buttonOriginalStyles.color;
          newButton.style.border = buttonOriginalStyles.border;
          newButton.style.padding = buttonOriginalStyles.padding;
          newButton.style.fontSize = '16px';
          newButton.style.fontFamily = buttonOriginalStyles.fontFamily;
          newButton.style.fontWeight = buttonOriginalStyles.fontWeight;
          newButton.style.textTransform = buttonOriginalStyles.textTransform;
          newButton.style.cursor = 'pointer';
          newButton.style.marginTop = '10px';
          newButton.style.borderRadius = '10px';
      
          newButton.classList.add('button-addCart');
        }
      
        // Get the labels for variations
        let sizeLabel = document.querySelector('label[for="variation_1"]');
        let colorLabel = document.querySelector('label[for="variation_2"]');
      
        let sizeText = sizeLabel ? sizeLabel.innerText.trim().toLowerCase() : '';
        let colorText = colorLabel ? colorLabel.innerText.trim().toLowerCase() : '';
      
        // Determine if size is in columns or rows
        let sizeIsColumn = sizeText.includes('talle') || sizeText.includes('size');
        let colorIsRow = colorText.includes('color') || colorText.includes('color');
      
        // Variants and stock
        let variants = LS.variants;
        let colorList = [];
        let sizeList = [];
        let combinations = [];
        let stocks = [];
      
        // Adjustment in variant validation
        for (let i = 0; i < variants.length; i++) {
          let variant = variants[i];
      
          let color = variant.option0;
          let size = variant.option1 || 'Tamaño único';
      
          // Manejo del caso cuando `option1` es null
          if (variant.option1 === null) {
            color = 'Unico color';
            size = variant.option0 || 'Tamaño único';
          }
      
          colorList.push(color);
          sizeList.push(size);
          combinations.push(color + '&' + size);
      
          stocks.push(variant.stock);
        }
      
        // Create a single list for colors and sizes without duplicates
        colorList = [...new Set(colorList)];
        sizeList = [...new Set(sizeList)];
      
        let tableHTML = "<div class='responsive-table'><table style='border-collapse: collapse; border: 1px solid black; margin-bottom: 150px; text-align: center; width: 100%; box-sizing: border-box;'>";
      
        // Table header
        let headerRow = "<tr><td style='border: 1px solid white; padding: 5px; background-color: black; color: white; font-weight: bold;'>Color/Talle</td>";
        for (let i = 0; i < colorList.length; i++) {
          headerRow += "<td style='border: 1px solid white; padding: 5px; background-color: black; color: white; font-weight: bold;'>" + colorList[i] + '</td>';
        }
        headerRow += '</tr>';
        tableHTML += headerRow;
      
        // Rows of sizes
        for (let i = 0; i < sizeList.length; i++) {
          let size = sizeList[i];
          let row = "<tr><td style='border: 1px solid black; padding: 5px; font-weight: bold; text-align: center;'>" + size + '</td>';
      
          for (let j = 0; j < colorList.length; j++) {
            let color = colorList[j];
            let cell = "<td style='border: 1px solid black; padding: 5px; text-align: center;'>";
      
            let index = combinations.indexOf(color + '&' + size);
            if (index > -1 && stocks[index] !== 0) {
              if (stocks[index] === null) {
                cell += "<input type='number' min='0' value='0' data-color='" + color + "' data-size='" + size + "' data-field='1' class='cart-quantity' style='text-align: center;'>";
              } else {
                cell += "<input type='number' min='0' max='" + stocks[index] + "' value='0' data-color='" + color + "' data-size='" + size + "' data-field='1' class='cart-quantity' style='text-align: center;'>";
                cell += "<br><span style='font-size: 12px; color: black; text-align: center;'>De: " + stocks[index] + '</span>';
              }
            } else {
              cell += 'X';
            }
      
            cell += '</td>';
            row += cell;
          }
      
          row += '</tr>';
          tableHTML += row;
        }
      
        tableHTML += '</table></div>';
      
        if (productVariants.length > 0) {
          let variantsElement = productVariants[0];
          variantsElement.insertAdjacentHTML('afterend', tableHTML);
        }
      
        let numberInputs = document.querySelectorAll('.cart-quantity');
        numberInputs.forEach(function (input) {
          input.addEventListener('input', function () {
            let maxValue = parseInt(this.getAttribute('max'));
            let enteredValue = parseInt(this.value);
            if (maxValue && enteredValue > maxValue) {
              this.value = maxValue;
            }
          });
        });
      
        // Function to add products to the cart
        function addCart(color, size, quantity) {
          let sizeInput = document.getElementById('variation_2');
          let colorInput = document.getElementById('variation_1');
          if (sizeInput === null) {
            colorInput = 'Unico color';
            sizeInput = document.getElementById('variation_1');
          }
          if (sizeInput && colorInput) {
            // Set the values of size and color inputs
            sizeInput.value = size;
            colorInput.value = color;
      
            // Create a hidden input for quantity
            let quantityInput = document.createElement('input');
            quantityInput.setAttribute('type', 'hidden');
            quantityInput.setAttribute('name', 'quantity');
            quantityInput.setAttribute('value', quantity);
            forms.appendChild(quantityInput);
      
            // Simulate a click on the add to cart button
            cartButton.click();
      
            // Remove the hidden input after adding
            forms.removeChild(quantityInput);
          }
        }
      
        // Handle the add to cart button
        function addToCartHandler() {
          let inputs = document.querySelectorAll('.cart-quantity');
          let items = [];
      
          inputs.forEach(function (input) {
            let quantity = parseInt(input.value);
            if (quantity > 0) {
              let color = input.getAttribute('data-color');
              let size = input.getAttribute('data-size');
      
              let index = combinations.indexOf(color + '&' + size);
              if (index > -1) {
                let stock = stocks[index];
                if (stock !== null && quantity > stock) {
                  input.value = stock;
                  quantity = stock;
                }
              }
              items.push({ color: color, size: size, quantity: quantity });
            }
          });
      
          // Group items by color and size
          let groupedItems = {};
          items.forEach(function (item) {
            let key = item.color + '&' + item.size;
            if (groupedItems[key]) {
              groupedItems[key].quantity += item.quantity;
            } else {
              groupedItems[key] = { color: item.color, size: item.size, quantity: item.quantity };
            }
          });
      
          // Convert object to array
          let finalItems = Object.values(groupedItems);
      
          // Clear the form before adding products
          inputs.forEach((input) => (input.value = 0));
      
          // Add items to the cart
          finalItems.forEach(function (item) {
            addCart(item.color, item.size, item.quantity);
          });
        }
      
        newButton.addEventListener('click', addToCartHandler);
      
        // Additional responsive styles
        const style = document.createElement('style');
        style.textContent = `
          .responsive-table {
            width: 100%;
          }
          
          .responsive-table table {
            width: 100%;
            border-collapse: collapse;
          }
          
          .responsive-table td, .responsive-table th {
            padding: 8px;
            text-align: center;
          }
          
          .button-addCart {
            margin-top: 10px; /* Adjust top margin for the button */
          }
          
          @media (max-width: 768px) {
            .responsive-table td, .responsive-table th {
              font-size: 12px;
              padding: 4px;
            }
            
            .button-addCart {
              font-size: 14px;
              padding: 8px;
            }
          }
        `;
        document.head.appendChild(style);
      }
    } catch (error) {
      console.error(error);
    }
  });
})();
