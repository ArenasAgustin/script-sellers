/*
 * This is a script to add features to pages created with Tiendanube
 * This script is created by @ArenasAgustin
 * This script supports themes: Amazonas, Atlántico, Bahia, Cubo, Idea, Lifestyle, Material, Silent, Simple, Trend, and Rio
 * The current version is 3.1.4.40
 */

(function () {
  useJquery().then(() => {
    try {
      console.info('Script version: 3.1.4.40');
      console.info('Script created by @ArenasAgustin');
      console.info('This script supports themes: Amazonas, Atlántico, Bahia, Cubo, Idea, Lifestyle, Material, Silent, Simple, Trend, and Rio');

      /**
       * Function to check if two elements are equal.
       *
       * @param {*} a - First element.
       * @param {*} b - Second element.
       * @returns {boolean} - Returns true if two elements are equal, false otherwise.
       */
      const equals = (a, b) => a == b;

      /**
       * Default variables to store data.
       *
       * @type {{
       *   defaultCatalog: HTMLElement,
       *   productsNotSaved: number,
       *   isSearch: boolean,
       *   pathName: string,
       *   cartsData: Array<any>,
       *   cartQuantity: number,
       *   minimumCart: null|number,
       *   minimumUnits: null|number,
       *   minimumType: null|boolean,
       *   require_login: null|boolean,
       * }}
       */
      let defaultVariables = {
        defaultCatalog: null,
        productsNotSaved: 0,
        isSearch: false,
        pathName: '',
        cartsData: [],
        cartQuantity: 0,
        minimumCart: null,
        minimumUnits: null,
        minimumType: null,
        require_login: false,
      };

      /**
       * Get the default catalog.
       */
      if (document.querySelector('.js-product-table')) {
        defaultVariables.defaultCatalog = document.querySelector('.js-product-table').cloneNode(true);
      } else if (document.querySelector('.js-masonry-grid')) {
        defaultVariables.defaultCatalog = document.querySelector('.js-masonry-grid').cloneNode(true);
      } else {
        defaultVariables.defaultCatalog = document.createElement('div');
      }

      /**
       * Initialize view list and is changed from localStorage if not present.
       */
      if (!localStorage.getItem('viewList')) {
        localStorage.setItem('viewList', false);
      }
      if (!localStorage.getItem('isChanged')) {
        localStorage.setItem('isChanged', false);
      }

      // Get the theme and language from localStorage
      /**
       * Theme name obtained from localStorage.
       *
       * @type {string}
       */
      const themeName = LS.theme.name;

      /**
       * Language code obtained from localStorage.
       *
       * @type {string}
       */
      const language = LS.langCode;

      /**
       * Spanish translations.
       * @type {Object}
       * @property {string} imageTxt - Texto para la imagen.
       * @property {string} nameTxt - Texto para el nombre.
       * @property {string} priceTxt - Texto para el precio.
       * @property {string} amountTxt - Texto para el monto total.
       * @property {string} stockTxt - Texto para el stock.
       * @property {string} quantityTxt - Texto para la cantidad.
       * @property {string} addToCartTxt - Texto para agregar al carrito.
       * @property {string} clearCartTxt - Texto para vaciar el carrito.
       * @property {string} moneyTxt - Símbolo monetario.
       * @property {string} switchTxt - Texto para cambiar a compra rápida.
       * @property {string} switchTxt2 - Texto para cambiar a vista minorista.
       * @property {string} productLinkTxt - Texto para enlace a productos.
       * @property {string} buyLinkTxt - Texto para enlace de compra.
       * @property {string} alertMunimumTxt - Texto para la alerta de monto mínimo.
       * @property {string} auxShippingTxt - Texto adicional para costo de envío.
       * @property {string} auxAmoutTxt - Texto adicional para monto.
       * @property {string} notAvalibleTxt - Texto para no disponible.
       * @property {string} notStockTxt - Texto para sin stock.
       * @property {string} loadingTxt - Texto para cargando.
       * @property {string} alertBeforeTxt - Texto para alerta antes de salir.
       * @property {string} seeVariatsTxt - Texto para ver variantes.
       * @property {string} buyTxt - Texto para comprar.
       * @property {string} readyTxt - Texto para listo.
       * @property {string} addTxt - Texto para agregando.
       */

      /**
       * Translations object containing language-specific text.
       * @type {Object.<string, Object>}
       */
      const translations = {
        /**
         * Spanish translations.
         * @type {Object}
         */
        es: {
          imageTxt: 'Imagen',
          nameTxt: 'Nombre',
          priceTxt: 'Precio',
          amountTxt: 'Monto total',
          stockTxt: 'Stock',
          quantityTxt: 'Cantidad',
          addToCartTxt: 'Agregar al carrito',
          clearCartTxt: 'Vaciar carrito',
          moneyTxt: '$',
          switchTxt: 'Cambiar a compra rápida',
          switchTxt2: 'Cambiar a vista minorista',
          productLinkTxt: 'productos',
          buyLinkTxt: 'comprar',
          alertMunimumTxt: 'El monto mínimo de compra es de $0,00 sin incluir el costo de envío',
          auxShippingTxt: 'sin incluir el costo de envío',
          auxAmoutTxt: 'comprar',
          notAvalibleTxt: 'No disponible',
          notStockTxt: 'Sin stock',
          loadingTxt: 'Cargando',
          alertBeforeTxt: '¿Estás seguro de que quieres salir, tienes productos para agregar al carrito?',
          seeVariatsTxt: 'Ver variantes',
          buyTxt: 'Comprar',
          readyTxt: 'Listo',
          addTxt: 'Agregando',
        },
        /**
         * Portuguese translations.
         * @type {Object}
         */
        pt: {
          imageTxt: 'Imagem',
          nameTxt: 'Nome',
          priceTxt: 'Preço',
          amountTxt: 'Valor total',
          stockTxt: 'Estoque',
          quantityTxt: 'Quantia',
          addToCartTxt: 'Adicionar ao carrinho',
          clearCartTxt: 'Limpar o carrinho',
          moneyTxt: 'R$',
          switchTxt: 'Formato de compra por atacado',
          switchTxt2: 'Mudar para o formato clássico',
          productLinkTxt: 'produtos',
          buyLinkTxt: 'comprar',
          alertMunimumTxt: 'O valor mínimo de compra é R$0,00 não incluindo o custo de envio',
          auxShippingTxt: 'sem considerar o custo de frete',
          auxAmoutTxt: 'valor',
          notAvalibleTxt: 'Não disponível',
          notStockTxt: 'Sem estoque',
          loadingTxt: 'Carregando',
          alertBeforeTxt: 'Tem certeza que deseja finalizar a compra, tem produtos para adicionar ao carrinho?',
          seeVariatsTxt: 'Ver variantes',
          buyTxt: 'Comprar',
          readyTxt: 'Pronto',
          addTxt: 'Adicionando',
        },
        /**
         * Default translations.
         * @type {Object}
         */
        default: {
          imageTxt: 'Image',
          nameTxt: 'Name',
          priceTxt: 'Price',
          amountTxt: 'Total amount',
          stockTxt: 'Stock',
          quantityTxt: 'Quantity',
          addToCartTxt: 'Add to cart',
          clearCartTxt: 'Clear cart',
          moneyTxt: '$',
          switchTxt: 'Switch to quick buy',
          switchTxt2: 'Switch to retail view',
          productLinkTxt: 'products',
          buyLinkTxt: 'buy',
          alertMunimumTxt: 'The minimum purchase amount is $0.00 not including shipping costs',
          auxShippingTxt: 'not including shipping costs',
          auxAmoutTxt: 'amount',
          notAvalibleTxt: 'Not available',
          notStockTxt: 'Not stock',
          loadingTxt: 'Loading',
          alertBeforeTxt: 'Are you sure you want to finish the purchase, do you have products to add to the cart?',
          seeVariatsTxt: 'See variants',
          buyTxt: 'Buy',
          readyTxt: 'Ready',
          addTxt: 'Adding',
        },
      };

      /**
       * Language-specific text translations.
       * @type {Object}
       */
      const translation = translations[language] || translations['default'];

      // Destructuring language-specific text translations
      /**
       * Language-specific text translations for easier access.
       * @type {{
       *   imageTxt: string,
       *   nameTxt: string,
       *   priceTxt: string,
       *   amountTxt: string,
       *   stockTxt: string,
       *   quantityTxt: string,
       *   addToCartTxt: string,
       *   clearCartTxt: string,
       *   moneyTxt: string,
       *   switchTxt: string,
       *   switchTxt2: string,
       *   productLinkTxt: string,
       *   buyLinkTxt: string,
       *   alertMunimumTxt: string,
       *   auxShippingTxt: string,
       *   auxAmoutTxt: string,
       *   notAvalibleTxt: string,
       *   notStockTxt: string,
       *   loadingTxt: string,
       *   alertBeforeTxt: string,
       *   seeVariatsTxt: string,
       *   buyTxt: string,
       *   readyTxt: string,
       *   addTxt: string
       * }}
       */
      const { imageTxt, nameTxt, priceTxt, amountTxt, stockTxt, quantityTxt, addToCartTxt, clearCartTxt, moneyTxt, switchTxt, switchTxt2, productLinkTxt, buyLinkTxt, alertMunimumTxt, auxShippingTxt, auxAmoutTxt, notAvalibleTxt, notStockTxt, loadingTxt, alertBeforeTxt, seeVariatsTxt, buyTxt, readyTxt, addTxt } = translation;

      // InnerHTML of theme Amazonas
      // This is the HTML of the form to add to cart
      const stringFormInnerAmazonas = `
    <input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
    <input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />
    <input type="submit" class="js-addtocart js-prod-submit-form btn btn-primary btn-small w-100 mb-2 cart" value="${buyTxt}" />
    <div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-block btn-transition btn-small w-100 mb-2 disabled" style="display: none;">
        <div class="d-inline-block">
            <span class="js-addtocart-text">${buyTxt}</span>
            <span class="js-addtocart-success transition-container btn-transition-success-small"><svg class="icon-inline svg-icon-invert btn-transition-success-icon" viewBox="0 0 512 512"><path d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 48c110.532 0 200 89.451 200 200 0 110.532-89.451 200-200 200-110.532 0-200-89.451-200-200 0-110.532 89.451-200 200-200m140.204 130.267l-22.536-22.718c-4.667-4.705-12.265-4.736-16.97-.068L215.346 303.697l-59.792-60.277c-4.667-4.705-12.265-4.736-16.97-.069l-22.719 22.536c-4.705 4.667-4.736 12.265-.068 16.971l90.781 91.516c4.667 4.705 12.265 4.736 16.97.068l172.589-171.204c4.704-4.668 4.734-12.266.067-16.971z"></path></svg></span>
            <div class="js-addtocart-adding transition-container transition-soft btn-transition-progress">
                <div class="spinner-ellipsis invert">
                    <div class="point"></div><div class="point"></div><div class="point"></div><div class="point"></div>
                </div>
            </div>
        </div>
    </div>
`;

      // This is the HTML of the headers of the table
      const stringHeadersAmazonas = `
<div class="col-12 d-flex item-product-card">
    <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
    <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
        <div class="d-flex justify-content-around align-items-center w-100">
            <div class="col-4"><span class="item-name">${nameTxt}</span></div>
            <div class="col-3"><span class="item-name">${priceTxt}</span></div>
            <div class="col-3"><span class="item-name">${amountTxt}</span></div>
            <div class="col-3"><span class="item-name">${stockTxt}</span></div>
        </div>
        <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
    </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsAmazonas = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image">
    <div style="padding-bottom: 100%;" class="p-relative"></div>
  </div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name text-capitalize"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
    <div class="col-3">
      <div class="form-group float-left form-quantity cart-item-quantity small mb-0">
        <div class="row m-0 align-items-center ">
          <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-substract"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
          <div class="form-control-container col">
            <input class=" form-control js-cart-quantity-input text-center h6 form-control-inline" id="cart-quantity-input"type="number" name="quantity_aux_variants" value="0" min="0"step="1"  >
          </div>
          <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 24.103v8.169a11.995 11.995 0 0 0 9.698 11.768C396.638 63.425 472 150.461 472 256c0 118.663-96.055 216-216 216-118.663 0-216-96.055-216-216 0-104.534 74.546-192.509 174.297-211.978A11.993 11.993 0 0 0 224 32.253v-8.147c0-7.523-6.845-13.193-14.237-11.798C94.472 34.048 7.364 135.575 8.004 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.789 504 256c0-121.187-86.924-222.067-201.824-243.704C294.807 10.908 288 16.604 288 24.103z"></path></svg></span>
          <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-add"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
        </div>
      </div>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputAmazonas = `
<div class="form-group float-left form-quantity cart-item-quantity small mb-0">
  <div class="row m-0 align-items-center ">
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="substract-btn" id="substract-btn"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
    <div class="form-control-container col"><input type="number" class="form-control js-cart-quantity-input text-center h6 form-control-inline" name="quantity_aux" value="0" min="0" step="1" ></div>
    <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 24.103v8.169a11.995 11.995 0 0 0 9.698 11.768C396.638 63.425 472 150.461 472 256c0 118.663-96.055 216-216 216-118.663 0-216-96.055-216-216 0-104.534 74.546-192.509 174.297-211.978A11.993 11.993 0 0 0 224 32.253v-8.147c0-7.523-6.845-13.193-14.237-11.798C94.472 34.048 7.364 135.575 8.004 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.789 504 256c0-121.187-86.924-222.067-201.824-243.704C294.807 10.908 288 16.604 288 24.103z"></path></svg></span>
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
  </div>
</div>
`;

      // InnerHTML of theme Atlántico
      // This is the HTML of the form to add to cart
      const stringFormInnerAtlantico = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />
<div class="js-quickshop-add-container item-submit-container btn btn-primary">
  <input type="submit" class="js-addtocart js-prod-submit-form js-quickshop-icon-add btn btn-primary item-btn-quickshop cart" value="" alt="${addToCartTxt}" style="display: block"/>
  <svg class="js-quickshop-bag" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478 512"><path d="M443.62,170.67H340.48V102.4C340.48,45.85,294.3,0,237.34,0S134.2,45.85,134.2,102.4v68.27H31.06A34.25,34.25,0,0,0-3.32,204.8V477.87A34.25,34.25,0,0,0,31.06,512H443.62A34.25,34.25,0,0,0,478,477.87V204.8A34.25,34.25,0,0,0,443.62,170.67Zm-275-68.27c0-37.7,30.79-68.27,68.76-68.27S306.1,64.7,306.1,102.4v68.27H168.58Zm275,375.47H31.06V204.8H443.62V477.87Z"></path></svg>
</div>
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition disabled item-submit-container item-btn-quickshop" style="display: none">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active" >
      <svg class="icon-inline icon-lg svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478 512" ><path d="M443.62,170.67H340.48V102.4C340.48,45.85,294.3,0,237.34,0S134.2,45.85,134.2,102.4v68.27H31.06A34.25,34.25,0,0,0-3.32,204.8V477.87A34.25,34.25,0,0,0,31.06,512H443.62A34.25,34.25,0,0,0,478,477.87V204.8A34.25,34.25,0,0,0,443.62,170.67Zm-275-68.27c0-37.7,30.79-68.27,68.76-68.27S306.1,64.7,306.1,102.4v68.27H168.58Zm275,375.47H31.06V204.8H443.62V477.87Z"></path></svg>
    </span>
    <span class="js-addtocart-success transition-container btn-transition-success">
      <svg class="icon-inline icon-lg svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M444.96 159l-12.16-11c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L131.77 428 31.42 329c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L3.04 340C1.01 342.67 0 345.67 0 349s1.01 6 3.04 8l120.62 119c2.69 2.67 5.57 4 8.62 4s5.92-1.33 8.62-4l304.07-300c2.03-2 3.04-4.67 3.04-8s-1.02-6.33-3.05-9zM127.17 284.03c2.65 2.65 5.48 3.97 8.47 3.97s5.82-1.32 8.47-3.97L365.01 63.8c1.99-2 2.99-4.65 2.99-7.96s-1-6.29-2.99-8.94l-11.96-10.93c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97L135.14 236.34l-72.25-72.03c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97l-11.96 10.93C33 177.89 32 180.87 32 184.18s1 5.96 2.99 7.95l92.18 91.9z"></path></svg>
    </span>
    <div class="js-addtocart-adding transition-container btn-transition-progress mt-0 pr-3"><i class="spinner spinner-small col-4 offset-4"></i></div>
  </div>
</div>
`;

      // This is the HTML of the headers of the table
      const stringHeadersAtlantico = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsAtlantico = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image">
    <div style="padding-bottom: 100%;" class="p-relative"></div>
  </div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
    <div class="col-3">
      <div class="float-left form-quantity form-quantity-small mb-0">
        <span class="js-cart-quantity-btn form-quantity-icon btn p-2" style="left: 15px; bottom: -1px;" id="cart-quantity-btn-substract"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H0V245.33H320Z"></path></svg> </span>
        <input class="form-control form-control-quantity form-control-quantity-small form-control-inline" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0" step="1">
        <span class="js-cart-quantity-btn form-quantity-icon form-quantity-icon-up btn p-2" style="right: 15px; bottom: -1px;"  id="cart-quantity-btn-add"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H170.67V416H149.33V266.67H0V245.33H149.33V96h21.34V245.33H320Z"></path></svg> </span>
      </div>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputAtlantico = `
<div class="float-left form-quantity form-quantity-small mb-0">
  <span class="js-cart-quantity-btn form-quantity-icon btn p-2" style="left: 15px; bottom: -1px;" id="substract-btn"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H0V245.33H320Z"></path></svg> </span>
  <input class="form-control form-control-quantity form-control-quantity-small form-control-inline" type="number" name="quantity_aux" value="0" min="0" step="1">
  <span class="js-cart-quantity-btn form-quantity-icon form-quantity-icon-up btn p-2" style="right: 15px; bottom: -1px;" id="add-btn"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H170.67V416H149.33V266.67H0V245.33H149.33V96h21.34V245.33H320Z"></path></svg> </span>
</div>
`;

      // InnerHTML of theme Bahia
      // This is the HTML of the form to add to cart
      const stringFormInnerBahia = `
<input type="hidden" name="add_to_cart" value="122979185">
<div class="js-item-submit-container item-submit-container btn btn-primary btn-small p-0 position-relative">
  <input type="submit" class="js-addtocart js-prod-submit-form js-quickshop-icon-add btn btn-primary btn-small item-btn-quickshop item-btn-quickshop-medium cart" value="" alt="${addToCartTxt}">
  <svg class="svg-inline--fa fa-lg utilities-icon js-quickshop-bag" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.79,5.39l-3-4A1,1,0,0,0,18,1H6a1,1,0,0,0-.8.4l-3,4A1,1,0,0,0,2,6H2V20a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V6h0A1,1,0,0,0,21.79,5.39ZM6.5,3h11L19,5H5ZM7,10a1,1,0,0,1,2,0,3,3,0,0,0,6,0,1,1,0,0,1,2,0A5,5,0,0,1,7,10ZM20,20a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H20Z"></path></svg>
</div>
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition  btn-small item-btn-quickshop item-btn-quickshop-medium" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active"><svg class="svg-inline--fa fa-lg utilities-icon " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.79,5.39l-3-4A1,1,0,0,0,18,1H6a1,1,0,0,0-.8.4l-3,4A1,1,0,0,0,2,6H2V20a3,3,0,0,0,3,3H19a3,3,0,0,02,3-3V6h0A1,1,0,0,0,21.79,5.39ZM6.5,3h11L19,5H5ZM7,10a1,1,0,0,1,2,0,3,3,0,0,0,6,0,1,1,0,0,1,2,0A5,5,0,0,1,7,10ZM20,20a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H20Z"></path></svg>			</span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="h5 svg-inline--fa svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress"><svg class="svg-inline--fa fa-spin fa-lg svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></div>
  </div>
</div>
<input class="js-quantity-input hidden" type="number" name="quantity" id="input_quantity" value="0">
`;

      // This is the HTML of the headers of the table
      const stringHeadersBahia = `
<div class="col-12 d-flex item-container">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsBahia = `
<div class="col-12 item-container d-flex ">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
      <div class="col-3">
        <div class="form-group form-quantity small mb-2">
          <div class="row m-0 align-items-center ">
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-substract"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
            <div class="form-control-container col pl-0 pr-0"><input class="form-control text-center form-control-inline" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0" step="1"></div>
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-add"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
          </div>
        </div>
      </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputBahia = `
<div class="form-group form-quantity small mb-2" >
  <div class="row m-0 align-items-center ">
    <span class="js-cart-quantity-btn text-center btn" id="substract-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
    <div class="form-control-container col pl-0 pr-0"><input class="form-control text-center form-control-inline" type="number" name="quantity_aux" value="0" min="0" step="1"  ></div>
    <span class="js-cart-quantity-btn text-center btn" id="add-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
  </div>
</div>
`;

      // InnerHTML of theme Cubo
      // This is the HTML of the form to add to cart
      const stringFormInnerCubo = `
<input type="hidden" name="add_to_cart" value="122979185">                  
<svg class=" icon-inline icon-lg item-cart-absolute svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M551.991 64H129.28l-8.329-44.423C118.822 8.226 108.911 0 97.362 0H12C5.373 0 0 5.373 0 12v8c0 6.627 5.373 12 12 12h78.72l69.927 372.946C150.305 416.314 144 431.42 144 448c0 35.346 28.654 64 64 64s64-28.654 64-64a63.681 63.681 0 0 0-8.583-32h145.167a63.681 63.681 0 0 0-8.583 32c0 35.346 28.654 64 64 64 35.346 0 64-28.654 64-64 0-17.993-7.435-34.24-19.388-45.868C506.022 391.891 496.76 384 485.328 384H189.28l-12-64h331.381c11.368 0 21.177-7.976 23.496-19.105l43.331-208C578.592 77.991 567.215 64 551.991 64zM240 448c0 17.645-14.355 32-32 32s-32-14.355-32-32 14.355-32 32-32 32 14.355 32 32zm224 32c-17.645 0-32-14.355-32-32s14.355-32 32-32 32 14.355 32 32-14.355 32-32 32zm38.156-192H171.28l-36-192h406.876l-40 192z"></path></svg>
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-small py-3 pl-4 w-100 text-secondary cart" value="${buyTxt}">
<div class="js-addtocart js-addtocart-placeholder btn btn-transition disabled btn-small btn-transition-item py-3 pl-4 w-100 text-secondary" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active">${buyTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success smallest">${readyTxt}</span>
    <div class="js-addtocart-adding transition-container btn-transition-progress smallest">${addTxt}...</div>
  </div>
</div>
<input class="js-quantity-input hidden" type="number" name="quantity" id="input_quantity" value="0">
`;

      // This is the HTML of the headers of the table
      const stringHeadersCubo = `
<div class="col-12 d-flex item-container">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</
`;

      // This is the HTML of the variants of the products
      const stringVariantsCubo = `
<div class="item-container d-flex w-100">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name text-center"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
    <div class="col-3">
      <div class="d-flex justify-content-center m-0 align-items-center my-md-auto">
        <button class="js-cart-quantity-btn cart-item-btn btn" id="cart-quantity-btn-substract"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></button>
        <div class="form-control-container col"><input class="js-cart-quantity-input-item cart-item-input form-control" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0"step="1"  ></div>
        <button class="js-cart-quantity-btn cart-item-btn btn" id="cart-quantity-btn-add"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></button>
      </div>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputCubo = `
<div class="d-flex justify-content-center m-0 align-items-center my-md-auto">
  <span class="js-cart-quantity-btn cart-item-btn btn" id="substract-btn" id="substract-btn"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
  <div class="form-control-container col"><input type="number" class="js-cart-quantity-input-item cart-item-input form-control" name="quantity_aux" value="0" min="0" step="1" ></div>
  <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
</div>
`;

      // InnerHTML of theme Idea
      // This is the HTML of the form to add to cart
      const stringFormInnerIdea = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input  type="submit"  class="js-addtocart js-prod-submit-form btn btn-primary btn-small col-10 mb-2 cart"  value="${addToCartTxt}"/>
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-small btn-transition mb-2 col-10 disabled" style="display: none;">
    <div class="d-inline-block">
        <span class="js-addtocart-text">${addToCartTxt}</span>
        <span class="js-addtocart-success transition-container btn-transition-success-small">¡${readyTxt}!</span>
        <div class="js-addtocart-adding transition-container transition-soft btn-transition-progress-small"><div class="spinner-ellipsis-invert"></div></div>
    </div>
</div>
<input class="js-quantity-input hidden" type="number" name="quantity" id="input_quantity" value="0">
`;

      // This is the HTML of the headers of the table
      const stringHeadersIdea = `
<div class="col-12 d-flex item-container">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsIdea = `
<div class="col-12 item-container d-flex ">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
      <div class="col-3">
        <div class="form-group form-quantity small mb-2" data-component="product.quantity">
          <div class="row m-0 align-items-center " data-component="line-item.quantity">
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-substract"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
            <div class="form-control-container col pl-0 pr-0"><input  class="form-control text-center form-control-inline"  id="cart-quantity-input" type="number"  name="quantity_aux_variants"   value="0"  min="0"  step="1"  ></div>
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-add"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
          </div>
        </div>
      </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputIdea = `
<div class="form-group form-quantity small mb-2" data-component="product.quantity">
  <div class="row m-0 align-items-center " data-component="line-item.quantity">
    <span class="js-cart-quantity-btn text-center btn" id="substract-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                   </span>
    <div class="form-control-container col pl-0 pr-0"><input  class="form-control text-center form-control-inline"  type="number"  name="quantity_aux"  value="0"  min="0"  step="1"  ></div>
    <span class="js-cart-quantity-btn text-center btn" id="add-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
  </div>
</div>
`;

      // InnerHTML of theme Lifestyle
      // This is the HTML of the form to add to cart
      const stringFormInnerLifestyle = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" style="display:none !important;"/>
<div class="js-product-cta-container product-buy-container full-width">
  <input  type="submit"  class="js-prod-submit-form js-addtocart js-cart-direct-add btn btn-primary btn-inverse btn-smallest item-buy-btn full-width cart m-bottom-half"  value="${buyTxt}">
  <div class="js-addtocart js-addtocart-placeholder btn btn-primary full-width btn-transition m-bottom-half disabled btn-inverse btn-smallest item-buy-btn" style="display: none;">
    <div class="d-inline-block">
      <span class="js-addtocart-text">${buyTxt}</span>
      <span class="js-addtocart-success transition-container btn-transition-success">
        <span class="m-left p-left">¡${readyTxt}!</span>
        <span class="pull-right m-right"><svg class="svg-inline--fa fa-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z"></path></svg></span>
      </span>
      <div class="js-addtocart-adding transition-container btn-transition-progress">
        <span class="m-left ">${addTxt}</span>
        <span class="pull-right m-right"><svg class="svg-inline--fa fa-lg fa-spin m-left-half pull-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path></svg></span>
      </div>
    </div>
  </div>            
</div>
`;

      // This is the HTML of the headers of the table
      const stringHeadersLifestyle = `
<div class="d-flex col-auto full-width item-container" style="font-weight: bold;">
  <div class="span2 mb-3 mt-3 d-flex justify-content-center align-items-center" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${imageTxt}</span></div>
  <div class="span4" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${nameTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${priceTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${amountTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${stockTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${quantityTxt}</span></div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsLifestyle = `
<div class="col-auto full-width item-container d-flex" style="text-aling: center;">
  <div class="js-item-image-container item-image-container span2 overflow-hidden" style="height: 100px;" id="item-image"><div class="js-item-image-padding p-relative" style="padding-bottom: 100%; padding-left: 20%;"></div></div>
  <div class="span10 align-items-center" style="display: flex !important; margin: 0 !important;">
    <div class="js-item-name span3 item-name" style="margin: 0 !important;"></div>
    <div class="span2 px-2" style="margin: 0 !important;"></div>
    <div class="span4 px-2" style="margin: 0 !important;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
    <div class="span2 px-2" style="margin: 0 !important;"></div>
    <div class="span3 js-ajax-cart-qty-container pull-left m-top-half m-none-xs" style="margin: 0 !important;">
      <div class="cart-quantity-input-container d-inline-block pull-left">
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path></svg></span>
        <input  class="js-cart-quantity-input cart-quantity-input cart-quantity-input-small form-control form-control-secondary" id="cart-quantity-input" type="number"  name="quantity_aux_variants"  value="0"  min="0"  step="1" style="width: 100px !important;">
      </div>
      <div class="d-inline-block m-left-quarter m-none-xs">
        <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small" id="cart-quantity-btn-add"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
        <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small hidden-phone" id="cart-quantity-btn-substract"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
      </div>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputLifestyle = `
<div class="cart-quantity-input-container d-inline-block pull-left">
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path></svg></span>
  <input  class="js-cart-quantity-input cart-quantity-input cart-quantity-input-small form-control form-control-secondary" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 100px !important;">
</div>
<div class="d-inline-block m-left-quarter m-none-xs">
  <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small" id="add-btn"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
  <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small hidden-phone" id="substract-btn"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
</div>
`;

      // InnerHTML of theme Material
      // This is the HTML of the form to add to cart
      const stringFormInnerMaterial = `
<input type="hidden" name="add_to_cart" value="122979185">
<input  type="submit"  class="js-addtocart js-prod-submit-form btn btn-primary full-width font-small-xs cart"  value="${addToCartTxt}"  style="display: block;">
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition btn-block m-none disabled font-small-xs" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active">${addToCartTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="svg-check-circle-icon fa-lg svg-icon-invert  m-left-quarter" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress">
      <span class="m-left">${addTxt}...</span>
      <span class="pull-right m-right">
        <div class="spinner inverse">
          <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
            <div class="spinner-layer d-inline-block full-height full-width p-absolute">
              <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
            </div>
          </div>
        </div>
      </span>
    </div>
  </div>
</div>
<input  class="js-quantity-input hidden"  type="number"  name="quantity"  id="input_quantity"  value="0" style="display: none;">
`;

      // This is the HTML of the headers of the table
      const stringHeadersMaterial = `
<div class="d-flex item-container" style="flex: 0 0 100%; max-width: 100%;">
  <div class="d-flex" style="flex: 0 0 16.666667%; max-width: 16.666667%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;"><span class="item-name">${imageTxt}</span></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div style="flex: 0 0 25%; max-width: 25%;"><span class="item-name">${nameTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${priceTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${amountTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${stockTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsMaterial = `
<div class="item item-container d-flex" style="flex: 0 0 100%; max-width: 100%;">
  <div style="height: 100px; width: auto; position: relative; flex: 0 0 16.666667%; max-width: 16.666667%; overflow: hidden;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div class="d-flex"  style="width: 100%; align-items: center; justify-content: center;">
      <div class="js-item-name item-name" style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
      <div style="flex: 0 0 50%; max-width: 50%;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
    </div>
    <div style="flex: 0 0 25%; max-width: 25%;">
      <button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="cart-quantity-btn-substract"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
      <div class="cart-quantity-input-container d-inline-block pull-left">
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;">
          <div class="spinner">
            <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
              <div class="spinner-layer d-inline-block full-height full-width p-absolute">
                <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
                <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
                <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
              </div>
            </div>
          </div>
        </span>
        <input  class="cart-quantity-input"  id="cart-quantity-input" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 130px !important;">
      </div>
      <button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="cart-quantity-btn-add"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputMaterial = `
<button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="substract-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
<div class="cart-quantity-input-container d-inline-block pull-left">
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;">
    <div class="spinner">
      <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
        <div class="spinner-layer d-inline-block full-height full-width p-absolute">
          <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
          <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
          <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
        </div>
      </div>
    </div>
  </span>
  <input  class="cart-quantity-input" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 130px !important;">
</div>
<button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="add-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
`;

      // InnerHTML of theme Silent
      // This is the HTML of the form to add to cart
      const stringFormInnerSilent = `
<input type="hidden" name="add_to_cart" value="122979185">
<div class="item-submit-container btn btn-primary">
  <input type="submit" class="js-addtocart js-prod-submit-form js-quickshop-icon-add btn btn-primary item-btn-quickshop cart" value="" alt="${addToCartTxt}"/>
  <svg class="js-quickshop-cart-icon svg-inline--fa svg-icon-back" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M504.717 320H211.572l6.545 32h268.418c15.401 0 26.816 14.301 23.403 29.319l-5.517 24.276C523.112 414.668 536 433.828 536 456c0 31.202-25.519 56.444-56.824 55.994-29.823-.429-54.35-24.631-55.155-54.447-.44-16.287 6.085-31.049 16.803-41.548H231.176C241.553 426.165 248 440.326 248 456c0 31.813-26.528 57.431-58.67 55.938-28.54-1.325-51.751-24.385-53.251-52.917-1.158-22.034 10.436-41.455 28.051-51.586L93.883 64H24C10.745 64 0 53.255 0 40V24C0 10.745 10.745 0 24 0h102.529c11.401 0 21.228 8.021 23.513 19.19L159.208 64H551.99c15.401 0 26.816 14.301 23.403 29.319l-47.273 208C525.637 312.246 515.923 320 504.717 320zM408 168h-48v-40c0-8.837-7.163-16-16-16h-16c-8.837 0-16 7.163-16 16v40h-48c-8.837 0-16 7.163-16 16v16c0 8.837 7.163 16 16 16h48v40c0 8.837 7.163 16 16 16h16c8.837 0 16-7.163 16-16v-40h48c8.837 0 16-7.163 16-16v-16c0-8.837-7.163-16-16-16z"></path></svg>
</div>
<div class="js-addtocart js-addtocart-placeholder product-buy-btn btn btn-primary btn-transition item-btn-quickshop item-btn-quickshop-icon" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active"><svg class="svg-inline--fa" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M504.717 320H211.572l6.545 32h268.418c15.401 0 26.816 14.301 23.403 29.319l-5.517 24.276C523.112 414.668 536 433.828 536 456c0 31.202-25.519 56.444-56.824 55.994-29.823-.429-54.35-24.631-55.155-54.447-.44-16.287 6.085-31.049 16.803-41.548H231.176C241.553 426.165 248 440.326 248 456c0 31.813-26.528 57.431-58.67 55.938-28.54-1.325-51.751-24.385-53.251-52.917-1.158-22.034 10.436-41.455 28.051-51.586L93.883 64H24C10.745 64 0 53.255 0 40V24C0 10.745 10.745 0 24 0h102.529c11.401 0 21.228 8.021 23.513 19.19L159.208 64H551.99c15.401 0 26.816 14.301 23.403 29.319l-47.273 208C525.637 312.246 515.923 320 504.717 320zM408 168h-48v-40c0-8.837-7.163-16-16-16h-16c-8.837 0-16 7.163-16 16v40h-48c-8.837 0-16 7.163-16 16v16c0 8.837 7.163 16 16 16h48v40c0 8.837 7.163 16 16 16h16c8.837 0 16-7.163 16-16v-40h48c8.837 0 16-7.163 16-16v-16c0-8.837-7.163-16-16-16z"></path></svg></span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="svg-inline--fa" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress"><svg class="svg-inline--fa fa-spin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 39.056v16.659c0 10.804 7.281 20.159 17.686 23.066C383.204 100.434 440 171.518 440 256c0 101.689-82.295 184-184 184-101.689 0-184-82.295-184-184 0-84.47 56.786-155.564 134.312-177.219C216.719 75.874 224 66.517 224 55.712V39.064c0-15.709-14.834-27.153-30.046-23.234C86.603 43.482 7.394 141.206 8.003 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-115.633-79.14-212.779-186.211-240.236C302.678 11.889 288 23.456 288 39.056z"></path></svg></div>
  </div>
</div>
<input class="js-quantity-input" type="number" name="quantity" id="input_quantity" value="0"style="display: none;"/>
`;

      // This is the HTML of the headers of the table
      const stringHeadersSilent = `
<div class="d-flex item-container" style="flex: 0 0 100%; max-width: 100%;">
    <div class="d-flex" style="flex: 0 0 16.666667%; max-width: 16.666667%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;"><span class="item-name">${imageTxt}</span></div>
    <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div style="flex: 0 0 25%; max-width: 25%;"><span class="item-name">${nameTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${priceTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${amountTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${stockTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${quantityTxt}</span></div>
    </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsSilent = `
<div class="item-container d-flex" style="flex: 0 0 100%; max-width: 100%; text-transform: uppercase;">
  <div style="height: 100px; width: auto; position: relative; flex: 0 0 16.666667%; max-width: 16.666667%; overflow: hidden;" id="item-image">
    <div style="padding-bottom: 100%; position: relative;"></div>
  </div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div class="d-flex"  style="width: 100%; align-items: center; justify-content: center;">
      <div class="js-item-name item-name" style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
      <div style="flex: 0 0 50%; max-width: 50%;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
    </div>
      <div style="flex: 0 0 25%; max-width: 25%;">
        <button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="cart-quantity-btn-substract">
          <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>         
        </button>
        <div class="cart-quantity-input-container d-inline-block pull-left">
          <input class="js-cart-quantity-input cart-quantity-input" id="cart-quantity-input"type="number" name="quantity_aux_variants"  value="0" min="0" step="1"style="width: 160px !important;"/>
        </div>
        <button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="cart-quantity-btn-add">
          <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>             
        </button>
      </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputSilent = `
<button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="substract-btn">
  <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>                  
</button>
<div class="cart-quantity-input-container d-inline-block pull-left">
  <input class="js-cart-quantity-input cart-quantity-input" type="number" name="quantity_aux" value="0" min="0" step="1"style="width: 160px !important;">
</div>
<button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="add-btn">
  <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>     
</button>
`;

      // InnerHTML of theme Simple
      // This is the HTML of the form to add to cart
      const stringFormInnerSimple = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" id="input_quantity" style="display:none !important;"/>
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-primary full-width-xs btn-small btn-smallest font-small-extra p-right p-left p-right-half-xs p-left-half-xs cart m-auto" value="${addToCartTxt}"/>
<div class="js-addtocart js-addtocart-placeholder product-buy-btn btn btn-primary btn-transition js-addtocart-placeholder-inline full-width-xs btn-small btn-smallest font-small-extra p-right p-left p-right-half-xs p-left-half-xs m-auto" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active">${addToCartTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success">
      ¡${readyTxt}!<svg class="svg-inline--fa svg-icon-back m-left-quarter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M444.09 166.99l-27.39-28.37c-2.6-1.96-5.53-2.93-8.8-2.93-3.27 0-5.87.98-7.82 2.93L142.81 396.86l-94.88-94.88c-1.96-2.61-4.55-3.91-7.82-3.91-3.27 0-6.21 1.3-8.8 3.91l-27.4 27.38c-2.6 2.61-3.91 5.55-3.91 8.8s1.31 5.87 3.91 7.82l130.1 131.07c2.6 1.96 5.53 2.94 8.8 2.94 3.27 0 5.87-.98 7.82-2.94L444.08 183.6c2.6-2.61 3.91-5.55 3.91-8.8.01-3.24-1.3-5.86-3.9-7.81zM131.88 285.04c2.62 1.97 5.58 2.96 8.88 2.96s5.92-.99 7.89-2.96L353.34 80.35c2.62-2.64 3.95-5.6 3.95-8.88 0-3.28-1.33-5.92-3.95-7.89l-27.63-28.62c-2.62-1.97-5.58-2.96-8.88-2.96s-5.92.99-7.89 2.96L140.76 204.12l-60.41-60.41c-1.97-2.64-4.59-3.95-7.89-3.95s-6.26 1.31-8.88 3.95l-27.63 27.63c-2.62 2.64-3.95 5.6-3.95 8.88 0 3.29 1.33 5.92 3.95 7.89l95.93 96.93z"></path></svg>
    </span>
    <div class="js-addtocart-adding transition-container btn-transition-progress">
      ${addTxt}...<svg class="svg-inline--fa svg-icon-back  m-left-quarter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M551.991 64H144.28l-8.726-44.608C133.35 8.128 123.478 0 112 0H12C5.373 0 0 5.373 0 12v24c0 6.627 5.373 12 12 12h80.24l69.594 355.701C150.796 415.201 144 430.802 144 448c0 35.346 28.654 64 64 64s64-28.654 64-64a63.681 63.681 0 0 0-8.583-32h145.167a63.681 63.681 0 0 0-8.583 32c0 35.346 28.654 64 64 64s64-28.654 64-64c0-18.136-7.556-34.496-19.676-46.142l1.035-4.757c3.254-14.96-8.142-29.101-23.452-29.101H203.76l-9.39-48h312.405c11.29 0 21.054-7.869 23.452-18.902l45.216-208C578.695 78.139 567.299 64 551.991 64zM464 424c13.234 0 24 10.766 24 24s-10.766 24-24 24-24-10.766-24-24 10.766-24 24-24zm-256 0c13.234 0 24 10.766 24 24s-10.766 24-24 24-24-10.766-24-24 10.766-24 24-24zm279.438-152H184.98l-31.31-160h368.548l-34.78 160zM272 200v-16c0-6.627 5.373-12 12-12h32v-32c0-6.627 5.373-12 12-12h16c6.627 0 12 5.373 12 12v32h32c6.627 0 12 5.373 12 12v16c0 6.627-5.373 12-12 12h-32v32c0 6.627-5.373 12-12 12h-16c-6.627 0-12-5.373-12-12v-32h-32c-6.627 0-12-5.373-12-12z"></path></svg>
    </div>
  </div>
</div>
`;

      // This is the HTML of the headers of the table
      const stringHeadersSimple = `
<div class="d-flex col-auto full-width item-container" style="font-weight: bold;">
  <div class="span2 mb-3 mt-3 d-flex justify-content-center align-items-center" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${imageTxt}</span></div>
  <div class="span4" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${nameTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${priceTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${amountTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${stockTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${quantityTxt}</span></div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsSimple = `
<div class="col-auto full-width item-container d-flex ">
  <div class="js-item-image-container item-image-container span2 overflow-hidden" style="height: 100px;" id="item-image">
    <div class="js-item-image-padding p-relative" style="padding-bottom: 100%; padding-left: 20%;"></div>
  </div>
  <div class="span10 align-items-center" style="display: flex !important; margin: 0 !important;">
    <div class="js-item-name span3 item-name" style="margin: 0 !important;"></div>
    <div class="span1 px-2" style="margin: 0 !important;"></div>
    <div class="span4 px-2" style="margin: 0 !important;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
    <div class="span2 px-2" style="margin: 0 !important;"></div>
    <div class="span3" style="margin: 0 !important;">
      <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="cart-quantity-btn-substract">
        <div class="cart-quantity-svg-icon svg-icon-text" data-component="quantity.minus"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
      </span>
      <div class="cart-quantity-input-container d-inline-block pull-left">
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 5<path d="M483.515 28.485L431.35 80.65C386.475 35.767 324.485 8 256 8 123.228 8 14.824 112.338 8.31 243.493 7.971 250.311 13.475 256 20.301 256h28.045c6.353 0 11.613-4.952 11.973-11.294C66.161 141.649 151.453 60 256 60c54.163 0 103.157 21.923 138.614 57.386l-54.128 54.129c-7.56 7.56-2.206 20.485 8.485 20.485H492c6.627 0 12-5.373 12-12V36.971c0-10.691-12.926-16.045-20.485-8.486zM491.699 256h-28.045c-6.353 0-11.613 4.952-11.973 11.294C445.839 370.351 360.547 452 256 452c-54.163 0-103.157-21.923-138.614-57.386l54.128-54.129c7.56-7.56 2.206-20.485-8.485-20.485H20c-6.627 0-12 5.373-12 12v143.029c0 10.691 12.926 16.045 20.485 8.485L80.65 431.35C125.525 476.233 187.516 504 256 504c132.773 0 241.176-104.338 247.69-235.493.339-6.818-5.165-12.507-11.991-12.507z"></p</svg></span>
        <input class="cart-quantity-input"id="cart-quantity-input"type="number" name="quantity_aux_variants" value="0" min="0" step="1" style="width: 90px; height: 18px; border-top: 1px solid rgba(67, 67, 67, 0.8); border-bottom: 1px solid rgba(67, 67, 67, 0.8);"/>
      </div>
      <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="cart-quantity-btn-add">
        <div class="cart-quantity-svg-icon svg-icon-text"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
      </span>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputSimple = `
<span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="substract-btn">
  <div class="cart-quantity-svg-icon svg-icon-text" data-component="quantity.minus"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
</span>
<div class="cart-quantity-input-container d-inline-block pull-left">
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M483.515 28.485L431.35 80.65C386.475 35.767 324.485 8 256 8 123.228 8 14.824 112.338 8.31 243.493 7.971 250.311 13.475 256 20.301 256h28.045c6.353 0 11.613-4.952 11.973-11.294C66.161 141.649 151.453 60 256 60c54.163 0 103.157 21.923 138.614 57.386l-54.128 54.129c-7.56 7.56-2.206 20.485 8.485 20.485H492c6.627 0 12-5.373 12-12V36.971c0-10.691-12.926-16.045-20.485-8.486zM491.699 256h-28.045c-6.353 0-11.613 4.952-11.973 11.294C445.839 370.351 360.547 452 256 452c-54.163 0-103.157-21.923-138.614-57.386l54.128-54.129c7.56-7.56 2.206-20.485-8.485-20.485H20c-6.627 0-12 5.373-12 12v143.029c0 10.691 12.926 16.045 20.485 8.485L80.65 431.35C125.525 476.233 187.516 504 256 504c132.773 0 241.176-104.338 247.69-235.493.339-6.818-5.165-12.507-11.991-12.507z"></path></svg></span>
  <input class="cart-quantity-input"type="number" name="quantity_aux" value="0" min="0" step="1" style="width: 70px; height: 18px; border-top: 1px solid rgba(67, 67, 67, 0.8); border-bottom: 1px solid rgba(67, 67, 67, 0.8);"/>
</div>
<span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="add-btn">
  <div class="cart-quantity-svg-icon svg-icon-text"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
</span>
`;

      // InnerHTML of theme Trend
      // This is the HTML of the form to add to cart
      const stringFormInnerTrend = `
<input type="hidden" name="add_to_cart" value="122979185">
<input  type="submit"  class="js-addtocart js-prod-submit-form btn btn-primary full-width font-small-xs cart"  value="${addToCartTxt}"  style="display: block;">
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition btn-block m-none disabled font-small-xs" style="display: none;">
  <div class="d-inline-block"><span class="js-addtocart-text btn-transition-start transition-container active">${addToCartTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="svg-check-circle-icon fa-lg svg-icon-invert  m-left-quarter" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress"><span class="m-left">${addTxt}...</span>
      <span class="pull-right m-right">
        <div class="spinner inverse">
          <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
            <div class="spinner-layer d-inline-block full-height full-width p-absolute">
              <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
            </div>
          </div>
        </div>
      </span>
    </div>
  </div>
</div>
<input  class="js-quantity-input hidden"  type="number"  name="quantity"  id="input_quantity"  value="0" style="display: none;">
`;

      // This is the HTML of the headers of the table
      const stringHeadersTrend = `
<div class="d-flex item-container" style="flex: 0 0 100%; max-width: 100%;">
  <div class="d-flex" style="flex: 0 0 16.666667%; max-width: 16.666667%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;"><span class="item-name h2">${imageTxt}</span></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div style="flex: 0 0 25%; max-width: 25%;"><span class="item-name h2">${nameTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${priceTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${amountTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${stockTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsTrend = `
<div class="item item-container d-flex" style="flex: 0 0 100%; max-width: 100%;">
  <div style="height: 100px; width: auto; position: relative; flex: 0 0 16.666667%; max-width: 16.666667%; overflow: hidden;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div class="d-flex"  style="width: 100%; align-items: center; justify-content: center;">
      <div class="js-item-name item-name h2" style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
      <div style="flex: 0 0 50%; max-width: 50%;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
    </div>
    <div style="flex: 0 0 25%; max-width: 25%;">
      <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-left small" id="cart-quantity-btn-substract"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
      <div class="cart-quantity-input-container d-inline-block">        
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 39.056v16.659c0 10.804 7.281 20.159 17.686 23.066C383.204 100.434 440 171.518 440 256c0 101.689-82.295 184-184 184-101.689 0-184-82.295-184-184 0-84.47 56.786-155.564 134.312-177.219C216.719 75.874 224 66.517 224 55.712V39.064c0-15.709-14.834-27.153-30.046-23.234C86.603 43.482 7.394 141.206 8.003 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-115.633-79.14-212.779-186.211-240.236C302.678 11.889 288 23.456 288 39.056z"></path></svg></span>
        <input class="js-cart-quantity-input cart-quantity-input small" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0" step="1" style="width: 100px !important;">
      </div>
      <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-right small" id="cart-quantity-btn-add"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputTrend = `
<button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-left small" id="substract-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
<div class="cart-quantity-input-container d-inline-block">        
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 39.056v16.659c0 10.804 7.281 20.159 17.686 23.066C383.204 100.434 440 171.518 440 256c0 101.689-82.295 184-184 184-101.689 0-184-82.295-184-184 0-84.47 56.786-155.564 134.312-177.219C216.719 75.874 224 66.517 224 55.712V39.064c0-15.709-14.834-27.153-30.046-23.234C86.603 43.482 7.394 141.206 8.003 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-115.633-79.14-212.779-186.211-240.236C302.678 11.889 288 23.456 288 39.056z"></path></svg></span>
  <input  class="js-cart-quantity-input cart-quantity-input small" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 100px !important;">
</div>
<button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-right small" id="add-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
`;

      // InnerHTML of theme Rio
      // This is the HTML of the form to add to cart
      const stringFormInnerRio = `
<input type="hidden" name="add_to_cart" value="119938588">
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />                                              
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-primary btn-small w-100 mb-2 cart" value="${buyTxt}"  style="display: inline-block;">
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-block btn-transition btn-small mb-2 disabled" style="display: none;">
    <div class="d-inline-block">
        <span class="js-addtocart-text" data-prev-visibility="inline" style="display: inline;">${buyTxt}</span>
        <span class="js-addtocart-success transition-container">¡${readyTxt}!</span>
        <div class="js-addtocart-adding transition-container">${addTxt}...</div>
    </div>
</div>
`;

      // This is the HTML of the headers of the table
      const stringHeadersRio = `
<div class="col-12 d-flex item-container text-center">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsRio = `
<div class="col-12 item-container d-flex ">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name text-center"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
      <div class="col-3 d-flex align-items-center justify-content-center">
        <div class="form-group float-left form-quantity cart-item-quantity small mb-0">
          <div class="row m-0 align-items-center ">
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-substract"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
            <div class="form-control-container js-cart-quantity-container col px-1">
              <input type="number" class=" form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux_variants" value="0" min="0" step="1" id="cart-quantity-input" style="visibility: visible; pointer-events: all;">
            </div>
            <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-add"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
          </div>
        </div>
      </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputRio = `
<div class="form-group float-left form-quantity cart-item-quantity small mb-0">
  <div class="row m-0 align-items-center ">
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="substract-btn"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
    <div class="form-control-container js-cart-quantity-container col px-1">
      <input type="number" class=" form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux" value="0" min="0" step="1" style="visibility: visible; pointer-events: all;">
    </div>
    <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
  </div>
</div>
`;

      // InnerHTML of theme Lima
      // This is the HTML of the form to add to cart
      const stringFormInnerLima = `
<input type="hidden" name="add_to_cart" value="119938588" id="input_id">
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-secondary btn-medium w-100 py-2 cart" value="Comprar">
<div class="js-addtocart js-addtocart-placeholder btn btn-secondary btn-block btn-transition btn-medium mb-1 py-2 disabled" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text">Comprar</span>
    <span class="js-addtocart-success transition-container">¡Listo!</span>
    <div class="js-addtocart-adding transition-container transition-icon">
      <svg class="icon-inline btn-icon icon-spin font-body" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg>
    </div>
  </div>
</div>
`;

      // This is the HTML of the headers of the table
      const stringHeadersLima = `
<div class="col-12 d-flex item-container text-center">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

      // This is the HTML of the variants of the products
      const stringVariantsLima = `
<div class="js-item-product col-12 item-product col-grid">
  <div classs="item col-12">
    <div class="js-quickshop-container js-quickshop-has-variants position-relative row">
      <div class="item-image col-2" style="height: 100px; width: auto;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
      <div class="col-10 row justify-content-around align-items-center text-center">
        <div class="d-flex justify-content-around align-items-center w-100">
          <div class="js-item-name col-3 item-name text-center"></div>
          <div class="col-1"></div>
          <div class="col-5"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
          <div class="col-1"></div>
          <div class="col-3 form-group form-quantity cart-item-quantity small m-auto mb-0">
            <div class="row m-0 align-items-center ">
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-substract"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
            <div class="form-control-container js-cart-quantity-container col px-1">
              <input type="number" class="form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux_variants" value="0" min="0" step="1" id="cart-quantity-input">
            </div>
            <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-add"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
`;

      // This is the HTML of the quantity input
      const stringQuantityInputLima = `
<div class="row m-0 align-items-center">
  <span class="js-cart-quantity-btn form-quantity-icon btn" id="substract-btn"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
  <div class="form-control-container js-cart-quantity-container col px-1">
    <input type="number" class="form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux" value="0" min="0" step="1">
  </div>
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
  <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
</div>
`;

      /**
       * Configuration object for different themes.
       * @typedef {Object} ThemeConfig
       * @property {string} productContainer - Selector for the product container element.
       * @property {string} switchClass - Selector for the class used for switching.
       * @property {string} switchClassSearch - Selector for the search switch class.
       * @property {string} switchClassHome - Selector for the home switch class.
       * @property {string} inputPriceSelector - Selector for the input price element.
       * @property {string} cartQuantityInputSelector - Selector for the cart quantity input element.
       * @property {string} cardsSelector - Selector for the cards container.
       * @property {string} cardImgSelector - Selector for the card image element.
       * @property {string} cardDescriptionSelector - Selector for the card description element.
       * @property {string} stringSwitchStyles - String representing the switch styles.
       * @property {string} stringPriceClasses - String representing the price classes.
       * @property {string} stringBtnAddToCartStyles - String representing the button styles.
       * @property {string} stringQuantityInput - String representing the quantity input.
       * @property {string} stringVariants - String representing the variants.
       * @property {string} stringHeaders - String representing the headers.
       * @property {string} stringFormInner - String representing the inner form.
       */

      /**
       * Configuration object for different themes.
       * @type {Object.<string, ThemeConfig>}
       */
      const themesConfig = {
        Amazonas: {
          productContainer: '.item-description',
          switchClass: '.js-category-controls',
          switchClassSearch: '.page-header h1',
          switchClassHome: 'h3.h1',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.js-cart-quantity-input',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'max-width:350px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:300px',
          stringQuantityInput: stringQuantityInputAmazonas,
          stringVariants: stringVariantsAmazonas,
          stringHeaders: stringHeadersAmazonas,
          stringFormInner: stringFormInnerAmazonas,
        },

        Atlántico: {
          productContainer: '.item-description',
          switchClass: '.col.text-left.mt-md-3.mt-4',
          switchClassSearch: '.page-header h1',
          switchClassHome: '.row.align-items-center.justify-content-between .col-md-4',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.form-control-quantity',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'max-width:310px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:300px',
          stringQuantityInput: stringQuantityInputAtlantico,
          stringVariants: stringVariantsAtlantico,
          stringHeaders: stringHeadersAtlantico,
          stringFormInner: stringFormInnerAtlantico,
        },

        Bahia: {
          productContainer: '.item-description',
          switchClass: '.js-category-header',
          switchClassSearch: '.page-header.container',
          switchClassHome: '.tab-nav-container',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.form-control',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'max-width:400px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:400px',
          stringQuantityInput: stringQuantityInputBahia,
          stringVariants: stringVariantsBahia,
          stringHeaders: stringHeadersBahia,
          stringFormInner: stringFormInnerBahia,
        },

        Cubo: {
          productContainer: '.item-description',
          switchClass: '.js-category-controls',
          switchClassSearch: '.page-header',
          switchClassHome: '.section-featured-home .container .row .col-12',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.js-cart-quantity-input-item',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'max-width:350px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:300px',
          stringQuantityInput: stringQuantityInputCubo,
          stringVariants: stringVariantsCubo,
          stringHeaders: stringHeadersCubo,
          stringFormInner: stringFormInnerCubo,
        },

        Idea: {
          productContainer: '.item-description',
          switchClass: '.page-header.section-title',
          switchClassSearch: '.page-header',
          switchClassHome: '.featured-title h2',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.form-control',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'margin-top: 1rem',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:400px',
          stringQuantityInput: stringQuantityInputIdea,
          stringVariants: stringVariantsIdea,
          stringHeaders: stringHeadersIdea,
          stringFormInner: stringFormInnerIdea,
        },

        Lifestyle: {
          productContainer: '.js-product-container',
          switchClass: '.category-controls',
          switchClassSearch: '.title-container',
          switchClassHome: '.js-head .container',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.cart-quantity-input',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image-container',
          cardDescriptionSelector: '.item-info-container',
          stringSwitchStyles: 'max-width:350px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:350px',
          stringQuantityInput: stringQuantityInputLifestyle,
          stringVariants: stringVariantsLifestyle,
          stringHeaders: stringHeadersLifestyle,
          stringFormInner: stringFormInnerLifestyle,
          minimumCartSelector: 'p',
        },

        Lima: {
          productContainer: '.item-description',
          switchClass: '.js-category-controls',
          switchClassSearch: '.page-header',
          switchClassHome: '.section-featured-home .container .row .col-12',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.js-cart-quantity-input',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'max-width:350px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:350px',
          stringQuantityInput: stringQuantityInputLima,
          stringVariants: stringVariantsLima,
          stringHeaders: stringHeadersLima,
          stringFormInner: stringFormInnerLima,
          minimumCartSelector: '',
        },

        Material: {
          productContainer: '.item-description',
          switchClass: '.title-container',
          switchClassSearch: '.title-container',
          switchClassHome: '.home-categories',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.cart-quantity-input',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image-container',
          cardDescriptionSelector: '.item-info-container',
          stringSwitchStyles: 'margin: 1rem; max-width:400px;',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:400px',
          stringQuantityInput: stringQuantityInputMaterial,
          stringVariants: stringVariantsMaterial,
          stringHeaders: stringHeadersMaterial,
          stringFormInner: stringFormInnerMaterial,
          minimumCartSelector: 'h5',
        },

        Rio: {
          productContainer: '.item-description',
          switchClass: '.js-category-controls',
          switchClassSearch: '.page-header h1',
          switchClassHome: '.tab-nav-container',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.form-control',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image',
          cardDescriptionSelector: '.item-description',
          stringSwitchStyles: 'max-width:400px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:400px',
          stringQuantityInput: stringQuantityInputRio,
          stringVariants: stringVariantsRio,
          stringHeaders: stringHeadersRio,
          stringFormInner: stringFormInnerRio,
        },

        Silent: {
          productContainer: '.item-description',
          switchClass: '.title-container',
          switchClassSearch: '.title-container',
          switchClassHome: '.title-container',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.js-cart-quantity-input',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image-container',
          cardDescriptionSelector: '.item-info-container',
          stringSwitchStyles: 'margin-top:1rem;max-width:400px;',
          stringPriceClasses: 'js-price-display price p-left-quarter',
          stringBtnAddToCartStyles: 'max-width:400px;',
          stringQuantityInput: stringQuantityInputSilent,
          stringVariants: stringVariantsSilent,
          stringHeaders: stringHeadersSilent,
          stringFormInner: stringFormInnerSilent,
          minimumCartSelector: '.alert-info',
        },

        Trend: {
          productContainer: '.item-description',
          switchClass: '.category-controls',
          switchClassSearch: '.title-container',
          switchClassHome: '.title-container',
          inputPriceSelector: '.item-price',
          cartQuantityInputSelector: '.cart-quantity-input',
          cardsSelector: '.js-product-container',
          cardImgSelector: '.item-image-container',
          cardDescriptionSelector: '.item-info-container',
          stringSwitchStyles: 'margin: 1rem; max-width:400px;',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:400px',
          stringQuantityInput: stringQuantityInputTrend,
          stringVariants: stringVariantsTrend,
          stringHeaders: stringHeadersTrend,
          stringFormInner: stringFormInnerTrend,
        },

        default: {
          productContainer: '.js-product-container',
          switchClass: '.js-category-breadcrumbs',
          switchClassSearch: '.breadcrumb',
          switchClassHome: '.subtitle-container',
          inputPriceSelector: '#item-price',
          cartQuantityInputSelector: '.cart-quantity-input',
          cardsSelector: '.item-container',
          cardImgSelector: '.js-item-image-container',
          cardDescriptionSelector: '.item-info-container',
          stringSwitchStyles: 'max-width:350px',
          stringPriceClasses: 'js-price-display',
          stringBtnAddToCartStyles: 'max-width:350px',
          stringQuantityInput: stringQuantityInputSimple,
          stringVariants: stringVariantsSimple,
          stringHeaders: stringHeadersSimple,
          stringFormInner: stringFormInnerSimple,
          minimumCartSelector: 'h4',
        },
      };

      /**
       * Theme configuration object obtained based on the selected theme name.
       * @type {ThemeConfig}
       */
      const themeConfig = themesConfig[themeName] || themesConfig['default'];

      /**
       * Update btnStartBuy based on the themeName.
       * @param {string} themeName - The name of the current theme.
       */
      if (themeName === 'Amazonas' || themeName === 'Idea' || themeName === 'Atlántico' || themeName === 'Bahia' || themeName === 'Lima' || themeName === 'Rio' || themeName === 'Silent' || themeName === 'Cubo' || themeName === 'Lifestyle' || themeName === 'Material' || themeName === 'Trend') {
        const updateBtnStartBuy = (themeName) => {
          switch (themeName) {
            case 'Amazonas':
            case 'Idea':
              themeConfig.btnStartBuy = document.querySelector('#ajax-cart-submit-div').parentNode.children[1];
              break;

            case 'Atlántico':
            case 'Bahia':
            case 'Lima':
            case 'Rio':
            case 'Silent':
              themeConfig.btnStartBuy = document.querySelector('#ajax-cart-submit-div');
              break;

            case 'Cubo':
              themeConfig.btnStartBuy = document.querySelector('#ajax-cart-submit-div').parentNode.children[2];
              break;

            case 'Lifestyle':
              themeConfig.btnStartBuy = document.querySelector('#ajax-cart-minumum-div');
              break;

            case 'Material':
            case 'Trend':
              themeConfig.btnStartBuy = null;
              break;

            default:
              themeConfig.btnStartBuy = document.querySelector('.js-toggle-cart.js-fullscreen-modal-close.btn-link.m-bottom').parentNode;
              break;
          }
        };
        updateBtnStartBuy(themeName);
      }
      // Destructuring theme configuration properties
      const { productContainer, switchClass, switchClassSearch, switchClassHome, btnStartBuy, inputPriceSelector, cartQuantityInputSelector, cardsSelector, cardImgSelector, cardDescriptionSelector, stringSwitchStyles, stringPriceClasses, stringBtnAddToCartStyles, stringQuantityInput, stringVariants, stringHeaders, stringFormInner, minimumCartSelector } = themeConfig;

      /**
       * Adds styles to an element.
       * @param {HTMLElement} element - The element to which styles will be applied.
       * @param {string} styles - The CSS styles to be applied.
       */
      const addStyles = (element, styles) => {
        element?.setAttribute('style', styles);
      };

      /**
       * Creates a form to add items to the cart.
       * @returns {HTMLFormElement} The newly created form element.
       */
      const createForm = () => {
        // Creation of the form
        const form = document.createElement('form');
        form.classList.add('js-product-form');
        form.id = 'product-form-variant';
        form.innerHTML = stringFormInner;
        form.setAttribute('method', 'post');
        form.setAttribute('action', `/${productLinkTxt}/`);

        // Set the style of the form depending on the theme
        if (['Bahia', 'Silent', 'Idea', 'Material', 'Rio'].includes(themeName)) form.setAttribute('style', 'width: 100%; display: flex; justify-content: center;');
        else if (themeName === 'Cubo') form.classList.add('js-product-form', 'btn-small', 'd-block', 'p-0');

        return form;
      };

      /**
       * Creates an input element for the product variant.
       * @returns {HTMLInputElement} The created input element.
       */
      const createInputVariant = () => {
        const inputVariant = document.createElement('input');
        inputVariant.type = 'hidden';
        inputVariant.id = 'input_variant';

        return inputVariant;
      };

      /**
       * Creates a div element for displaying the product price.
       * @returns {HTMLDivElement} The created div element.
       */
      const createDivPrice = () => {
        const divPrice = document.createElement('div');
        divPrice.classList.add('item-price-container');
        divPrice.innerHTML = `<span class='${stringPriceClasses}' id='item-price'>${moneyTxt}0,00</span>`;

        // Set the style of the price depending on the theme
        if (['Cubo', 'Lima'].includes(themeName)) {
          divPrice.classList.add('mb-3');
        } else if (equals(themeName, 'Trend')) {
          divPrice.classList.add('h2');
          addStyles(divPrice, 'margin: 0 !important;');
          addStyles(divPrice?.children[0], 'font-size: 16px; line-height: 16px; word-break: break-word;');
        }

        return divPrice;
      };

      /**
       * Creates a div element for displaying an auxiliary product price.
       * @returns {HTMLDivElement} The created div element.
       */
      const createDivPriceAux = () => {
        const divPriceAux = document.createElement('div');
        divPriceAux.classList.add('item-price-container', 'mb-3');
        divPriceAux.innerHTML = divPrice.innerHTML; // Clone the content of divPrice
        divPriceAux.querySelector('.js-price-display').classList.add('item-price'); // Adjust class for consistency

        return divPriceAux;
      };

      /**
       * Creates a div element for displaying the product stock.
       * @returns {HTMLDivElement} The created div element.
       */
      const createDivStock = () => {
        const divStock = document.createElement('div');
        divStock.classList.add('item-stock-container');
        divStock.innerHTML = "<span class='js-stock-display'>&infin;</span>"; // Placeholder for infinite stock

        // Set the style of the stock depending on the theme
        if (['Cubo', 'Lima'].includes(themeName)) {
          divStock.classList.add('mb-3');
        } else if (equals(themeName, 'Trend')) {
          divStock.classList.add('h2');
          addStyles(divStock, 'margin: 0 !important;');
          addStyles(divStock.children[0], 'font-size: 16px; line-height: 16px; word-break: break-word;');
        }

        return divStock;
      };

      /**
       * Creates a div element for displaying headers.
       * @returns {HTMLDivElement} The created div element.
       */
      const createDivHeaders = () => {
        const divHeaders = document.createElement('div');
        divHeaders.classList.add('item', 'span10', 'col-auto');
        divHeaders.id = 'div-headers';
        divHeaders.dataset.isChanged = 'true';
        divHeaders.innerHTML = stringHeaders;

        // Set the style of the headers depending on the theme
        switch (themeName) {
          case 'Amazonas':
            addStyles(divHeaders, 'border-bottom: 1px solid rgba(0, 0, 0, 0.2); flex: 0 0 100%; max-width: 100%;');
            break;

          case 'Atlántico':
          case 'Bahia':
          case 'Cubo':
          case 'Lima':
          case 'Rio':
            divHeaders.classList.add('d-flex', 'item', 'mb-3', 'col-12');
            addStyles(divHeaders, 'display: flex !important;');
            break;

          case 'Idea':
            divHeaders.classList.add('mb-3', 'col-12', 'd-flex');
            break;

          case 'Lifestyle':
            divHeaders.classList.add('span12');
            addStyles(divHeaders, 'display: flex !important; text-align: center;');
            break;

          case 'Material':
            addStyles(divHeaders, 'display: flex !important; width: 100%; margin-bottom: 1rem; text-align: center;');
            break;

          case 'Silent':
            addStyles(divHeaders, 'width: calc(100% - 20px); display: flex !important; margin-bottom: 1rem; margin: 10px 20px 10px;');
            break;

          case 'Trend':
            divHeaders.classList.add('d-flex');
            addStyles(divHeaders, 'width: 100%; margin-bottom: 1rem; text-align: center;');
            break;

          default:
            addStyles(divHeaders, 'display: flex !important;');
            break;
        }

        return divHeaders;
      };

      /**
       * Creates a button element to add items to the cart.
       * @returns {HTMLDivElement} The created button element.
       */
      const createBtnAddToCart = () => {
        const btnAddToCart = document.createElement('div');
        btnAddToCart.id = 'ajax-cart-add-div';
        btnAddToCart.innerHTML = `<button class="btn btn-primary btn-block" style="${stringBtnAddToCartStyles}" id="btn-add-to-cart">${addToCartTxt}</button>`;
        addStyles(btnAddToCart, 'width: 100% !important; display: flex !important; justify-content: center; align-items: center;');

        return btnAddToCart;
      };

      /**
       * Creates a button element to clear the cart.
       * @returns {HTMLDivElement} The created button element.
       */
      const createBtnClearCart = () => {
        const divBtnClearCart = document.createElement('div');
        divBtnClearCart.id = 'ajax-cart-clear-div';
        divBtnClearCart.innerHTML = `<span class="btn btn-primary btn-block" id="clear_cart">${clearCartTxt}</span>`;
        divBtnClearCart.classList.add('js-ajax-cart-submit');

        // Set the style of the button to empty the cart depending on the theme
        switch (themeName) {
          case 'Amazonas':
          case 'Rio':
            divBtnClearCart.classList.add('row', 'mb-3');
            break;

          case 'Atlántico':
            divBtnClearCart.classList.add('mt-3');
            divBtnClearCart.children[0].classList.add('d-block');
            break;

          case 'Bahia':
            divBtnClearCart.classList.add('my-3');
            break;

          case 'Cubo':
            divBtnClearCart.classList.add('js-ajax-cart-submit', 'col-6', 'my-2');
            divBtnClearCart.children[0].classList.add('btn-medium', 'w-100', 'px-1', 'd-block');
            break;

          case 'Idea':
          case 'Lima':
            divBtnClearCart.classList.add('js-ajax-cart-submit', 'row', 'mb-3');
            break;

          case 'Lifestyle':
            divBtnClearCart.classList.add('m-bottom');
            divBtnClearCart.children[0].classList.add('btn-inverse', 'full-width');
            break;

          case 'Material':
            addStyles(divBtnClearCart, 'display: flex !important; margin-top: 1rem; margin-bottom: 1rem;');
            break;

          case 'Silent':
          case 'Trend':
            addStyles(divBtnClearCart, 'margin-top: 1rem; margin-bottom: 1rem; display: flex !important;');
            break;

          default:
            divBtnClearCart.classList.add('pull-right', 'full-width-xs', 'm-top-quarter');
            break;
        }

        return divBtnClearCart;
      };

      /**
       * Creates a div element for the button to switch to the quick buy view.
       * @returns {HTMLDivElement} The created div element.
       */
      const createSwitchDiv = () => {
        const isGuestUser = LS.customer;
        // If the user is logged out and the client sends false then the wholesale list is shown
        if (defaultVariables.require_login === false || (defaultVariables.require_login === true && isGuestUser !== null)) {
          const switchDiv = document.createElement('div');
          switchDiv.id = 'switch-view-div';
          switchDiv.innerHTML = `<button class="btn btn-primary btn-block" id="switch-button" style="${stringSwitchStyles}">${switchTxt}</button>`;
          addStyles(switchDiv, 'width: 100% !important; display: flex !important; justify-content: center; align-items: center; margin-top: 10px; margin-bottom: 10px;');
          if (equals(themeName, 'Idea')) addStyles(switchDiv?.children[0], 'width: max-content;');
          return switchDiv;
        }
      };

      /**
       * Creates a link element for the button to switch to the quick buy view.
       * @returns {HTMLAnchorElement} The created link element.
       */
      const createSwitchLink = () => {
        const switchLink = document.createElement('a');
        switchLink.id = 'switch-view-div';
        switchLink.setAttribute('href', `/${productLinkTxt}/?mpage=50`);
        switchLink.innerHTML = switchDiv.innerHTML;

        // Set the style of the link depending on the theme
        if (equals(themeName, 'Atlántico')) addStyles(switchLink, 'width: 100% !important; display: flex !important; justify-content: start; align-items: center; text-decoration: none; margin-top: 10px;');
        else addStyles(switchLink, 'width: 100% !important; display: flex !important; justify-content: center; align-items: center; text-decoration: none; margin-top: 10px; margin-bottom: 10px;');
        if (equals(themeName, 'Idea')) addStyles(switchLink?.children[0], 'width: max-content;');

        return switchLink;
      };

      /**
       * Creates a div element for the quantity of the product.
       * @returns {HTMLDivElement} The created div element.
       */
      const createQuantityDiv = () => {
        const divQuantity = document.createElement('div');
        divQuantity.classList.add('span3', 'd-flex', 'align-items-center', 'justify-content-center');
        divQuantity.id = 'div-quantity';
        divQuantity.innerHTML = stringQuantityInput;

        // Set the style of the div for the quantity of the product depending on the theme
        switch (themeName) {
          case 'Amazonas':
          case 'Atlántico':
          case 'Cubo':
          case 'Idea':
            divQuantity.classList.add('col-3');
            break;

          case 'Bahia':
          case 'Rio':
            divQuantity.classList.add('col-3');
            addStyles(divQuantity, 'margin: 0 !important; float: right !important;');
            break;

          case 'Lifestyle':
            divQuantity.classList.add('js-ajax-cart-qty-container', 'pull-left', 'm-top-half', 'm-none-xs');
            break;

          case 'Lima':
            divQuantity.classList.add('col-3', 'form-group', 'form-quantity', 'cart-item-quantity', 'small', 'm-auto', 'mb-0');
            break;

          case 'Material':
            divQuantity.classList.add('pull-left', 'm-top-quarter');
            addStyles(divQuantity, 'flex: 0 0 16.666667%; max-width: 16.666667%;');
            break;

          case 'Trend':
            divQuantity.classList.add('pull-left', 'm-top-quarter');
            addStyles(divQuantity, 'flex: 0 0 25%; max-width: 25%;');
            break;

          default:
            addStyles(divQuantity, 'margin: 0 !important; float: right !important;');
            break;
        }

        return divQuantity;
      };

      /**
       * Creates and inserts the alert for minimum cart amount if the theme is Cubo.
       * @returns {HTMLDivElement | null} The created alert element or null if theme is not Cubo.
       */
      const createAlertMinimunCart = () => {
        if (equals(themeName, 'Cubo')) {
          const alertMinimunCart = document.createElement('div');
          alertMinimunCart.id = 'ajax-cart-minumum-div';
          addStyles(alertMinimunCart, 'display: none;');
          alertMinimunCart.innerHTML = `<div class="alert alert-warning">${alertMunimumTxt}</div>`;

          let auxAlertMinimunCart = document.querySelector('.js-ajax-cart-submit').parentNode;
          auxAlertMinimunCart.parentNode.insertBefore(alertMinimunCart, auxAlertMinimunCart);

          return alertMinimunCart;
        }

        return null;
      };

      /**
       * Sets the display property to 'flex' for the button to clear the cart if found.
       */
      const setDisplayFlexClearCart = () => {
        let divBtnClearCart = document.querySelector('.ajax-cart-clear-div');
        if (divBtnClearCart) {
          let stylesDivBtnClearCart = divBtnClearCart.getAttribute('style');

          stylesDivBtnClearCart = stylesDivBtnClearCart.replace('display: none;', 'display: flex !important;');
          addStyles(divBtnClearCart, stylesDivBtnClearCart);
        }
      };

      // Create form element
      const form = createForm();

      // Create input variant element
      const inputVariant = createInputVariant();

      // Create div price element
      const divPrice = createDivPrice();

      // Create div price aux element
      const divPriceAux = createDivPriceAux();

      // Create div stock element
      const divStock = createDivStock();

      // Create div headers element
      const divHeaders = createDivHeaders();

      // Create button to add to cart
      const btnAddToCart = createBtnAddToCart();

      // Create button to clear the cart
      const divBtnClearCart = createBtnClearCart();

      // Creation of the div for the button to switch to the quick buy view
      const switchDiv = createSwitchDiv();

      // Creation of the link for the button to switch to the quick buy view
      const switchLink = createSwitchLink();

      // Creation of the div for the quantity of the product
      const divQuantity = createQuantityDiv();

      // Create and insert the alert for minimum cart amount
      const alertMinimunCart = createAlertMinimunCart();

      /**
       * Sends data to the server via AJAX.
       * @param {string} url - The URL to send the AJAX request to.
       * @param {Object} data - The data to send in the request.
       * @param {string} successMsg - The message to log on successful AJAX request.
       * @param {string} errorMsg - The message to log on failed AJAX request.
       * @async
       */
      const ajaxFunction = async (url, data, successMsg, errorMsg) => {
        $.ajax({
          url: url,
          method: 'POST',
          data: { ...data },
          async: false,
          dataType: 'json',
          success: () => console.info(successMsg),
          error: () => console.error(errorMsg),
        });
      };

      /**
       * Retrieves the quantity of items in the cart and updates the global variable `cartQuantity`.
       * @async
       */
      const saveQuantityCart = async () => {
        let data = 0;

        $('.js-cart-item').map((_, element) => {
          let aux = element.querySelector('.js-cart-quantity-input');
          data += parseInt(aux.value);
        });

        defaultVariables.cartQuantity = data;
      };

      /**
       * Formats the price of a product based on the locale and currency.
       * @param {number} price - The price of the product.
       * @param {number} [quantity=1] - The quantity of the product.
       * @returns {string} - The formatted price string.
       */
      const formatPrice = (price, quantity = 1) => {
        const totalPrice = price * parseFloat(quantity);
        const languageConfig = {
          es: { locale: 'es-AR', currency: 'ARS' },
          pt: { locale: 'pt-BR', currency: 'BRL' },
          default: { locale: 'en-US', currency: 'USD' },
        };

        const { locale, currency } = languageConfig[language] || languageConfig['default'];

        return totalPrice.toLocaleString(locale, { style: 'currency', currency });
      };

      /**
       * Retrieves the minimum cart amount, minimum units, and type of minimum from the server.
       * @async
       */
      const getMinimunCart = async () => {
        try {
          const response = await $.ajax({
            url: `https://api.sellerslatam.com/affiliates/minimum_cart?id=${LS.store.id}`,
            method: 'GET',
            dataType: 'json',
          });

          defaultVariables.minimumCart = response.data.minimum_cart;
          defaultVariables.minimumUnits = response.data.minimum_units;
          defaultVariables.minimumType = response.data.minimum_type ? 'units' : 'price';
          defaultVariables.require_login = response.data.require_login;
        } catch (error) {
          console.error(error);
        }
      };

      /**
       * Determines the container class for the switch button based on the current path.
       * @returns {string} - The class name for the container of the switch button.
       */
      const setContainerSwitch = () => {
        if (equals(defaultVariables.pathName, '/search/')) {
          return switchClassSearch;
        }

        return equals(defaultVariables.pathName, '/') ? switchClassHome : switchClass;
      };

      /**
       * Handles the display of the minimum cart alert based on the provided elements.
       * @param {HTMLElement[]} elements - The elements to which the alert will be appended.
       */
      const handleMinimumCartAlert = (elements) => {
        const containerSwitchSelector = setContainerSwitch();
        const containerSwitch = document.querySelector(containerSwitchSelector);

        if (elements.length === 1) {
          let newWarning = createMinimumCartAlert(elements[0]);
          if (containerSwitch) {
            containerSwitch.appendChild(newWarning);
          }
        }
      };

      /**
       * Creates a minimum cart alert element based on the provided element.
       * @param {HTMLElement} element - The element to be cloned and appended to the alert.
       * @returns {HTMLElement} - The created minimum cart alert element.
       */
      const createMinimumCartAlert = (element) => {
        const newWarning = document.createElement('div');
        if (['Amazonas', 'Atlántico', 'Bahia', 'Cubo', 'Idea', 'Rio', 'Simple'].includes(themeName)) {
          newWarning.appendChild(element.cloneNode(true));
          addStyles(newWarning, 'width: 100% !important; display: flex !important; justify-content: center; align-items: center; text-decoration: none; margin-top: 10px; font-size: 14px;');
        } else {
          newWarning.appendChild(element.cloneNode(true));
          addStyles(newWarning?.children[0], 'max-width:400px;');
        }
        return newWarning;
      };

      /**
       * Disables or enables the purchase button based on the cart minimum violation and updates the button's link.
       * @param {HTMLElement[]} aux - The auxiliary elements related to the purchase button.
       * @param {HTMLElement} btnStartBuy - The main purchase button element.
       * @param {HTMLElement} btnStartBuy2 - The secondary purchase button element.
       * @param {boolean} isMinimumCartViolated - Flag indicating if the minimum cart amount is violated.
       * @param {boolean} isMinimumUnitsViolated - Flag indicating if the minimum units requirement is violated.
       */
      const disableOrEnablePurchaseButton = (aux, btnStartBuy, btnStartBuy2, isMinimumCartViolated, isMinimumUnitsViolated) => {
        aux.forEach((element) => {
          if (isMinimumCartViolated || isMinimumUnitsViolated) {
            displayMinimumCartAlert(element, minimumCartSelector, auxShippingTxt, auxAmoutTxt, moneyTxt);
          } else {
            addStyles(element, 'display: none;');
          }
        });

        btnStartBuy.disabled = isMinimumCartViolated || isMinimumUnitsViolated;

        if (btnStartBuy2) {
          btnStartBuy2.setAttribute('href', isMinimumCartViolated || isMinimumUnitsViolated ? `/${buyLinkTxt}/` : '');
        }
      };

      /**
       * Displays the minimum cart alert message and updates its content based on the violation.
       * @param {HTMLElement} element - The element containing the alert message.
       * @param {string} minimumCartSelector - The selector for the minimum cart container.
       * @param {string} auxShippingTxt - The auxiliary shipping text.
       * @param {string} auxAmoutTxt - The auxiliary amount text.
       * @param {string} moneyTxt - The currency symbol.
       */
      const displayMinimumCartAlert = (element, minimumCartSelector, auxShippingTxt, auxAmoutTxt, moneyTxt) => {
        addStyles(element, 'display: flex; justify-content: center; align-items: center; width: 100%;');

        let txtContainer = null;
        let auxInnerHTML = null;
        let innerHTML = [];

        if (['Lifestyle', 'Material', 'Silent', 'Simple'].includes(themeName)) {
          txtContainer = element.querySelector(minimumCartSelector);
        } else {
          txtContainer = element;
        }

        if (txtContainer) {
          auxInnerHTML = equals(defaultVariables.minimumType, 'units') ? txtContainer.innerHTML.replace(`${auxShippingTxt}`, '') : txtContainer.innerHTML;
          innerHTML = auxInnerHTML.trim().replace(`${auxAmoutTxt}`, '').split(' ');

          innerHTML.forEach((element, index) => {
            if (element.includes(`${moneyTxt}`)) {
              innerHTML[index] = equals(defaultVariables.minimumType, 'units') ? `${defaultVariables.minimumUnits} unidades` : formatPrice(defaultVariables.minimumCart);
            }
          });

          txtContainer.innerHTML = innerHTML.join(' ');
        }
      };

      /**
       * Resets the purchase button state and hides the warning messages.
       * @param {HTMLElement[]} aux - The auxiliary elements related to the purchase button.
       * @param {HTMLElement} btnStartBuy - The main purchase button element.
       * @param {HTMLElement} btnStartBuy2 - The secondary purchase button element.
       */
      const resetPurchaseButtonAndHideWarning = (aux, btnStartBuy, btnStartBuy2) => {
        btnStartBuy.disabled = false;

        if (btnStartBuy2) {
          btnStartBuy2.setAttribute('href', '');
        }

        aux.forEach((element) => {
          addStyles(element, 'display: none;');
        });
      };

      /**
       * Function to disable the button to start the purchase
       * @async
       */
      const functionToDisableBtnStartBuy = async () => {
        await saveQuantityCart();
        const btnStartBuy = document.querySelector('#ajax-cart-submit-div')?.children[0];
        const subTotal = (parseInt(document.querySelector('.js-cart-subtotal')?.dataset?.priceraw) / 100).toFixed(2) || 0;
        const btnStartBuy2 = document.querySelector('a.btn.btn-primary.btn-medium.w-100.px-1.d-block');
        const aux = document.querySelectorAll('#ajax-cart-minumum-div');

        if (!btnStartBuy) {
          return;
        }

        const isGuestUser = LS.customer;
        const isMinimumCartViolated = subTotal < defaultVariables.minimumCart && defaultVariables.minimumType === 'price';
        const isMinimumUnitsViolated = defaultVariables.cartQuantity < defaultVariables.minimumUnits && defaultVariables.minimumType === 'units';

        if (!isGuestUser || isGuestUser) {
          handleMinimumCartAlert(aux);
          disableOrEnablePurchaseButton(aux, btnStartBuy, btnStartBuy2, isMinimumCartViolated, isMinimumUnitsViolated);
        } else {
          resetPurchaseButtonAndHideWarning(aux, btnStartBuy, btnStartBuy2);
        }
      };

      /**
       * Disables the button to start the purchase process and attaches event listeners to relevant elements.
       * This function waits for the minimum cart information to be retrieved before attaching event listeners.
       * @async
       */
      const disableBtnStartBuy = async () => {
        // Get the minimum amount of the cart
        await getMinimunCart();
        await saveQuantityCart();
        /**
         * Adds a click event listener to the given element if not already added
         * @param {HTMLElement} element - The element to add the click event to
         */
        const addClickEvent = (element) => {
          if (!element?.addClick && element) {
            element.addEventListener('click', disableBtnStartBuy);
            element.addClick = true;
          }
        };

        /**
         * Adds a change event listener to the given element if not already added
         * @param {HTMLElement} element - The element to add the change event to
         */
        const addChangeEvent = (element) => {
          if (!element?.addChange && element) {
            element.addEventListener('change', disableBtnStartBuy);
            element.addChange = true;
          }
        };

        /**
         * Attaches click and change event listeners to specified buttons and quantity input elements
         */
        const attachEvents = () => {
          const buttons = ['.ajax-cart-btn-delete', '.js-prod-submit-form', '.cart-item-delete', '.btn.btn-link.btn-link-underline.ml-4', '.ajax-cart-quantity-btn:not(.not-cart-quantity-btn)', '.js-cart-quantity-btn:not(.not-cart-quantity-btn)', '.cart-item-delete .btn'];

          // Attach click event listeners to buttons
          buttons.forEach((selector) => {
            document.querySelectorAll(selector).forEach(addClickEvent);
          });

          // Attach change event listeners to quantity input elements
          document.querySelectorAll('.js-cart-quantity-input').forEach(addChangeEvent);
        };

        // Attach event listeners
        attachEvents();

        // Delay execution of functionToDisableBtnStartBuy and re-attach event listeners
        setTimeout(() => {
          functionToDisableBtnStartBuy();
          attachEvents();
        }, 2000);
      };

      // Obtén la URL actual
      var url = window.location.href;

      // Divide la URL por "/"
      var urlParts = url.split('/');

      // Encuentra la posición de "productos" en las partes de la URL
      var indexProducts = urlParts.indexOf('productos');

      // Verifica si "productos" está en la URL y si hay algo después de "productos"
      if (indexProducts !== -1 && indexProducts < urlParts.length - 1) {
        // Hay algo después de "productos"
        var somethingAfterProducts = urlParts[indexProducts + 1];

        // Verifica si somethingAfterProducts comienza con "?"
        if (somethingAfterProducts.startsWith('?')) {
        } else {
          // Estás en un producto específico
          getMinimunCart();
          disableBtnStartBuy();
        }
      }

      /**
       * Updates the count of unsaved products in the local storage.
       * @param {boolean} increment - Whether to increment or decrement the count.
       */
      const updateProductsNotSaved = (increment = true) => {
        defaultVariables.productsNotSaved += increment ? +1 : -1;
        localStorage.setItem('productsNotSaved', defaultVariables.productsNotSaved);
      };

      /**
       * Updates the quantity and price of an item in the UI.
       * @param {HTMLInputElement} item - The input element representing the quantity of the item.
       * @param {HTMLInputElement} itemQuantity - The input element where the quantity is displayed.
       * @param {HTMLElement} itemPrice - The element where the price is displayed.
       * @param {number} price - The price of the item.
       */
      const updateQuantityAndPrice = (item, itemQuantity, itemPrice, price) => {
        // Set the new quantity to the input and to the span
        itemQuantity.value = item.value;
        itemQuantity.setAttribute('value', item.value);
        // Set the new price to the span
        itemPrice.innerHTML = formatPrice(price, item.value);
      };

      const calculateNewValue = (value, operation, max) => {
        if (equals(operation, 'up') || equals(operation, 'increment')) return value < max ? `${value + 1}` : max;

        return value > 0 ? `${value - 1}` : 0;
      };

      /**
       * Changes the quantity of an item based on the specified operation.
       * @param {HTMLInputElement} item - The input element representing the quantity of the item.
       * @param {HTMLInputElement} itemQuantity - The input element where the quantity is displayed.
       * @param {HTMLElement} itemPrice - The element where the price is displayed.
       * @param {number} price - The price of the item.
       * @param {string} operation - The operation to perform ('increment' or 'decrement').
       */
      const changeQuantity = (item, itemQuantity, itemPrice, price, operation, stock) => {
        stock = stock != '∞' ? parseInt(stock) : Infinity;

        if (price && item.value >= 0) {
          // Perform the specified operation on the quantity
          item.value = calculateNewValue(parseInt(item.value), operation, stock);
          // Update productsNotSaved and localStorage accordingly
          updateProductsNotSaved(equals(operation, 'increment'));
          // Update quantity, value, and price in the UI
          updateQuantityAndPrice(item, itemQuantity, itemPrice, price);
        }
      };

      /**
       * Changes the quantity of an item in the cart.
       * @param {HTMLInputElement} item - The input element representing the quantity of the item.
       * @param {string} direction - The direction of the change ('up' to increment, 'down' to decrement).
       */
      const changeQuantityAux = (item, direction) => {
        item.value = calculateNewValue(parseInt(item.value), direction, Infinity);
      };

      /**
       * Clears the entire shopping cart.
       * @param {number} lengthArr - The length of the array containing the items in the cart.
       * @async
       */
      const clearCart = async (lengthArr) => {
        $('.js-cart-item').map(async (index, element) => {
          const itemId = element.dataset.itemId;

          // If an element exists then an ajax message is sent
          if (index + 1 < lengthArr) {
            const quantity = { [itemId]: 0 };

            try {
              // Ajax request to clear products from the shopping cart
              await ajaxFunction('/cart/update/', { quantity }, 'success clear cart', 'error clear cart');

              element.remove();
            } catch (error) {
              console.error('Error clearing cart:', error);
            }
          } else {
            // If an element does not exist, execute a TiendaNube function to remove the item
            LS?.removeItem(itemId, true);
          }
        });

        // Execute disableBtnStartBuy after clearing the cart
        disableBtnStartBuy();
      };

      /**
       * Resets the quantity of items in the cart.
       * @param {Array} idsArr - An array containing the IDs of the items in the cart.
       * @async
       */
      const resetItemsCart = async (idsArr) => {
        await Promise.all(
          idsArr.map(async (element, index) => {
            const itemId = element.itemCartId;

            try {
              if (index + 1 < idsArr.length) {
                const quantity = { [itemId]: 0 };

                // Ajax request to clear products from the shopping cart
                await ajaxFunction('/cart/update/', { quantity }, 'success clear cart', 'error clear cart');
              } else {
                // If an element does not exist, execute a TiendaNube function
                LS?.removeItem(itemId, true);
              }
            } catch (error) {
              console.error('Error clearing cart:', error);
            }
          }),
        );

        // Execute disableBtnStartBuy after resetting the items in the cart
        disableBtnStartBuy();
      };

      /**
       * Adds products to the shopping cart.
       * @param {number} maxAmount - The maximum number of items to add to the cart.
       * @async
       */
      const addToCart = async (maxAmount) => {
        let addedAmount = 1;
        $('.js-product-form').map(async (_, element) => {
          // Auxiliary variables
          const itemIdInput = element.querySelector('input[name=add_to_cart]');
          const itemQuantityInput = element.querySelector('input[name=quantity]');
          const itemVariants = element.querySelectorAll('#input_variant');
          const addToCartButton = element.querySelector('.js-addtocart');

          if (itemIdInput && itemQuantityInput && itemQuantityInput.value && itemQuantityInput.value !== '0') {
            if (addedAmount < maxAmount) {
              try {
                const data = { add_to_cart: itemIdInput.value, quantity: itemQuantityInput.value, add_to_cart_enhanced: 1 };

                // If a variant name exists, add it to the data object
                itemVariants.forEach((variant) => (data[variant?.name] = variant?.value));

                // Ajax request to add products to the shopping cart
                const ajaxUrl = equals(themeName, 'Cubo') ? `/${buyLinkTxt}/` : `/${auxAmoutTxt}/`;
                await ajaxFunction(ajaxUrl, data, 'success add to cart', 'error add to cart');

                addedAmount++;
              } catch (error) {
                console.error('Error adding item to cart:', error);
              }
            } else {
              addToCartButton?.click();
            }
          }
        });

        // Execute disableBtnStartBuy after adding items to the cart
        await disableBtnStartBuy();
        setDisplayFlexClearCart();
      };

      /**
       * Resets the value of an input element.
       * @param {HTMLElement} container - The container element containing the input element.
       * @param {string} inputSelector - The CSS selector of the input element.
       * @param {string} value - The value to set for the input element.
       */
      const resetInputValue = (container, inputSelector, value) => {
        const inputElement = inputSelector ? container.querySelector(inputSelector) : container;
        if (inputElement) {
          inputElement.value = value;
          inputElement.setAttribute('value', value);
        }
      };

      /**
       * Resets the quantity of the products.
       * @async
       */
      const resetQuantity = async () => {
        $(productContainer).map((_, element) => {
          resetInputValue(element, 'input[name=quantity_aux]', 0);
          resetInputValue(element, 'input[name=quantity]', 0);

          // Set the price to 0
          const inputPrice = element.querySelector('.item-price');
          if (inputPrice) inputPrice.innerHTML = '$0,00';
        });

        $('input[name=quantity_aux_variants]').map((_, element) => {
          resetInputValue(element, null, 0);
        });
      };

      /**
       * Sets styles to the elements when the mouse is over.
       * @param {HTMLElement} element - The target element.
       */
      const mouseOver = (element) => {
        addStyles(element, 'border-bottom: none; box-shadow: 0 1px 6px rgba(0, 0, 0, 0.2);');
      };

      /**
       * Sets styles to the elements when the mouse is out.
       * @param {HTMLElement} element - The target element.
       */
      const mouseOut = (element) => {
        addStyles(element, 'border-bottom: 1px solid rgba(0, 0, 0, 0.2); box-shadow: none;');
      };

      /**
       * Saves data from the cart items.
       * @async
       */
      const saveData = async () => {
        const data = [];

        $('.js-cart-item').map((_, element) => {
          const itemCartId = element.dataset.itemId;
          let aux, itemId, itemName, itemVariants;

          // Select the elements according to the theme
          switch (themeName) {
            case 'Amazonas':
            case 'Cubo':
            case 'Rio':
              aux = element.querySelector('.cart-item-name');
              itemName = aux.children[0].innerHTML.trim();
              itemVariants = aux.children[1].innerHTML.trim();
              break;

            case 'Atlántico':
              aux = element.querySelector('.cart-item-info').children[0];
              itemName = aux.children[0].innerHTML.trim();
              itemVariants = aux.children[1].innerHTML.trim();
              break;

            case 'Bahia':
              aux = element.querySelector('h6');
              itemName = aux.children[0].innerHTML.trim();
              itemVariants = aux.children[1].innerHTML.trim();
              break;

            case 'Idea':
              aux = element.querySelector('.cart-item-name a');
              itemName = aux.children[0].innerHTML.trim();
              itemVariants = aux.children[1].innerHTML.trim();
              break;

            case 'Lifestyle':
            case 'Trend':
              aux = element.querySelector('.ajax-cart-item-link');
              itemName = aux.innerHTML.trim().split('<small>')[0].trim();
              itemVariants = aux.children[0].innerHTML.trim();
              break;

            case 'Lima':
              aux = element.querySelector('.col.align-items-center.pl-0.py-3 .font-small.mb-2.mb-md-1');
              itemName = aux.children[0].innerHTML.trim();
              itemVariants = aux.children[1].innerHTML.trim();
              break;

            case 'Material':
              aux = element.querySelector('.ajax-cart-item-link');
              itemName = aux.children[0].innerHTML.trim().split('<small>')[0].trim();
              itemVariants = aux.children[0].children[0].innerHTML.trim();
              break;

            case 'Silent':
              aux = element.querySelector('.cart-item-name').children[0];
              itemName = aux.innerHTML.trim().split('<small>')[0].trim();
              itemVariants = aux.children[0].innerHTML.trim();
              break;

            default:
              aux = element.querySelector('.ajax-cart-item-link.cart-item-name').children[0];
              itemName = aux.children[0].innerHTML.trim();
              itemVariants = aux.children[1].innerHTML.trim();
              break;
          }

          // Push the data to the array
          data.push({ itemCartId, itemId, itemName, itemVariants });
        });

        // Save the data
        defaultVariables.cartsData = data;
      };

      /**
       * Extracts the name of the product from a string.
       * @param {string} str - The input string containing the product name.
       * @param {boolean} isVariantsCard - Indicates whether the string is from a variants card.
       * @returns {string} The extracted product name.
       */
      const getName = (str, isVariantsCard = false) => {
        if (str.includes('<br>')) str = str.split('<br>')[0];

        return isVariantsCard ? str.toLowerCase().trim() : str.split('(')[0].trim().toLowerCase();
      };

      /**
       * Formats the name of a product
       * @param {string} str - The original name of the product
       * @param {number} dataVariants - The price data for the product variants
       * @returns {string} The formatted name of the product
       */
      const formatName = (str, dataVariants) => {
        let newName = str.trim().split('<br>')[0];

        // // Check if the name has quantity
        // if (/^(?:\d|[a-z]|\d|[.!¡%+\- (,)])+(?:\|)(?:\d|[a-z]|\d|[.!¡%+\- (,)])+$/im.test(newName)) {
        //   const aux = formatPrice(dataVariants, 1 / parseInt(str.trim().split('|').reverse()[0].trim().split(' ')[0]));

        //   newName += `<br/><small>(${aux} x unidad)</small>`;
        // }

        if (['Silent', 'Trend'].includes(themeName)) return newName;
        return `<span id="item-name">${newName}</span>`;
      };

      /**
       * Replaces '.' and ' ' characters in the name.
       * @param {string} name - The input name string.
       * @returns {string} The modified name string.
       */
      const replaceName = (name) => name.replace(/\u002e/g, '').replace(/ /g, '-');

      /**
       * Formats the name of the product with variants.
       * @param {string} innerHTML - The inner HTML of the product name element.
       * @param {string} option - The selected option for the product variant.
       * @param {string} variation - The name of the variant.
       * @param {HTMLFormElement} form - The form element.
       * @returns {string} The updated inner HTML for the product name.
       */
      const formatNameVariants = (innerHTML, option, variation, form) => {
        // Check if the name has options
        if (innerHTML.includes('<small>($')) {
          const aux = innerHTML.split('<br>');

          // Set the option
          if (aux[0].includes(')')) aux[0] = aux[0].replace(')', `, ${option})`);
          else aux[0] += ` (${option})`;

          // Set the new innerHTML
          innerHTML = aux.join('<br>');
        } else {
          // Set the innerHTML depending on the theme
          if (['Simple', 'Cubo', 'Lifestyle', 'Lima'].includes(themeName)) innerHTML = innerHTML.replace('</span>', '');

          if (innerHTML.includes(')')) innerHTML = innerHTML.replace(')', `, ${option})`);
          else innerHTML += ` (${option})`;

          if (['Simple', 'Cubo', 'Lifestyle', 'Lima'].includes(themeName)) innerHTML += '</span>';
        }

        // Set the input variant
        inputVariant.value = option;
        inputVariant.name = variation;
        form.appendChild(inputVariant.cloneNode(true));

        // Return the new innerHTML
        return innerHTML;
      };
      if (LS.store.id === 4668972 || LS.store.id === 106881 || LS.store.id === 1892515 || LS.store.id === 1223426 || LS.store.id === 106881 || (LS.store.id === 4886159 && LS.customer) || LS.store.id === 5741803) {
        // Seleccionar elementos
        const forms = document.querySelector('.js-product-form');
        if (forms) {
          forms.style.width = 'auto';
        }

        let newButton = document.createElement('input');
        newButton.setAttribute('type', 'button');
        newButton.setAttribute('value', 'Agregar al carrito');
        let cartParent = document.querySelector('[data-store="product-buy-button"]').parentElement;
        if (cartParent) {
          cartParent.appendChild(newButton);
        }

        let containers = document.getElementsByClassName('form-row mb-2');
        if (containers.length > 0) {
          let container = containers[0];
          container.style.marginTop = '-120px';
          container.style.marginBottom = '10px';
          container.style.flexDirection = 'column-reverse';
          container.style.alignContent = 'center';
          container.style.alignItems = 'center';
        }

        let latestProductsAvailable = document.querySelector('.js-latest-products-available');
        if (latestProductsAvailable) {
          latestProductsAvailable.style.display = 'none';
        }

        let jsQuantity = document.querySelector('.js-quantity');
        if (jsQuantity) {
          jsQuantity.style.display = 'none';
        }

        let jsProductStockElements = document.getElementsByClassName('font-small py-2 text-center');
        if (jsProductStockElements.length > 0) {
          jsProductStockElements[0].style.display = 'none'; // Ocultar el primer elemento
        }

        let productVariants = document.getElementsByClassName('js-product-variants');
        if (productVariants.length > 0) {
          productVariants[0].style.display = 'none';
        }

        let fontSmallTextCenter = document.getElementsByClassName('font-small pt-2 pb-3 text-center');
        if (fontSmallTextCenter.length > 0) {
          fontSmallTextCenter[0].style.display = 'none';
        }

        let lastProduct = document.querySelector('.js-last-product');
        if (lastProduct) {
          lastProduct.style.display = 'none';
        }

        let productStock = document.querySelector('.js-product-stock');
        if (productStock) {
          productStock.style.display = 'none';
        }

        let productDescription = document.querySelector('.product-description');
        if (productDescription) {
          productDescription.style.marginBottom = '100px';
        }

        let cartButton = document.querySelector('[data-store="product-buy-button"]');
        if (cartButton) {
          let buttonContainer = document.createElement('div');
          buttonContainer.classList.add('button-container');
          cartButton.parentNode.insertBefore(buttonContainer, cartButton);
          buttonContainer.appendChild(cartButton);
          buttonContainer.style.display = 'none';

          let buttonOriginalStyles = window.getComputedStyle(cartButton);

          newButton.style.backgroundColor = buttonOriginalStyles.backgroundColor;
          newButton.style.color = buttonOriginalStyles.color;
          newButton.style.border = buttonOriginalStyles.border;
          newButton.style.padding = buttonOriginalStyles.padding;
          newButton.style.fontSize = '16px';
          newButton.style.fontFamily = buttonOriginalStyles.fontFamily;
          newButton.style.fontWeight = buttonOriginalStyles.fontWeight;
          newButton.style.textTransform = buttonOriginalStyles.textTransform;
          newButton.style.cursor = 'pointer';
          newButton.style.marginTop = '10px';
          newButton.style.borderRadius = '10px';

          newButton.classList.add('button-addCart');
        }

        // Crear el loader y el overlay al principio
        let loaderOverlay = document.createElement('div');
        loaderOverlay.style.position = 'fixed';
        loaderOverlay.style.top = '0';
        loaderOverlay.style.left = '0';
        loaderOverlay.style.width = '100%';
        loaderOverlay.style.height = '100%';
        loaderOverlay.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'; // Fondo semitransparente
        loaderOverlay.style.display = 'flex';
        loaderOverlay.style.justifyContent = 'center';
        loaderOverlay.style.alignItems = 'center';
        loaderOverlay.style.zIndex = '9999'; // Asegurar que esté al frente
        loaderOverlay.style.display = 'none'; // Iniciar oculto

        let loaderText = document.createElement('div');
        loaderText.innerText = 'Cargando productos...';
        loaderText.style.color = 'white';
        loaderText.style.fontSize = '24px';
        loaderText.style.fontWeight = 'bold';
        loaderText.style.backgroundColor = 'black';
        loaderText.style.padding = '20px';
        loaderText.style.borderRadius = '10px';
        loaderOverlay.appendChild(loaderText);

        document.body.appendChild(loaderOverlay); // Añadir el overlay al body

        // Mostrar el loader
        function showLoader() {
          loaderOverlay.style.display = 'flex';
          document.body.style.pointerEvents = 'none'; // Deshabilitar interacción con la página
        }

        // Ocultar el loader
        function hideLoader() {
          loaderOverlay.style.display = 'none';
          document.body.style.pointerEvents = 'auto'; // Habilitar interacción nuevamente
        }

        // Normalizar variaciones
        function normalizeVariations(variants) {
          return variants.map((variant) => {
            let color = variant.option0;
            let size = variant.option1 || 'Tamaño único';

            if (variant.option1 === null) {
              color = 'Unico color';
              size = variant.option0 || 'Tamaño único';
            }

            return { color, size, stock: variant.stock };
          });
        }

        // Obtener las etiquetas de variaciones
        let sizeLabel = document.querySelector('label[for="variation_1"]');
        let colorLabel = document.querySelector('label[for="variation_2"]');

        let sizeText = sizeLabel ? sizeLabel.innerText.trim().toLowerCase() : '';
        let colorText = colorLabel ? colorLabel.innerText.trim().toLowerCase() : '';

        // Determinar si el tamaño está en columnas o filas
        let sizeIsColumn = sizeText.includes('talle') || sizeText.includes('size');
        let colorIsRow = colorText.includes('color') || colorText.includes('color');

        // Variantes y stock
        let variants = normalizeVariations(LS.variants);
        let colorList = [];
        let sizeList = [];
        let combinations = [];
        let stocks = [];

        // Ajuste en la validación de variantes
        for (let i = 0; i < variants.length; i++) {
          let variant = variants[i];
          let color = variant.color;
          let size = variant.size;

          colorList.push(color);
          sizeList.push(size);
          combinations.push(color + '&' + size);
          stocks.push(variant.stock);
        }

        // Crear una lista única de colores y tallas
        colorList = [...new Set(colorList)];
        sizeList = [...new Set(sizeList)];

        let tableHTML = "<div class='responsive-table'><table style='border-collapse: collapse; border: 1px solid black; margin-bottom: 150px; text-align: center; width: 100%; box-sizing: border-box;'>";

        // Cabecera de la tabla
        let headerRow = "<tr><td style='border: 1px solid white; padding: 5px; background-color: black; color: white; font-weight: bold;'>Color/Talle</td>";
        for (let i = 0; i < colorList.length; i++) {
          headerRow += "<td style='border: 1px solid white; padding: 5px; background-color: black; color: white; font-weight: bold;'>" + colorList[i] + '</td>';
        }
        headerRow += '</tr>';
        tableHTML += headerRow;

        // Filas de tallas
        for (let i = 0; i < sizeList.length; i++) {
          let size = sizeList[i];
          let row = "<tr><td style='border: 1px solid black; padding: 5px; font-weight: bold; text-align: center;'>" + size + '</td>';

          for (let j = 0; j < colorList.length; j++) {
            let color = colorList[j];
            let cell = "<td style='border: 1px solid black; padding: 5px; text-align: center;'>";

            let index = combinations.indexOf(color + '&' + size);
            if (index > -1 && stocks[index] > 0) {
              cell += "<input type='number' min='0' max='" + stocks[index] + "' value='0' data-color='" + color + "' data-size='" + size + "' data-field='1' class='cart-quantity' style='text-align: center;'>";
              cell += "<br><span style='font-size: 12px; color: black; text-align: center;'>De: " + stocks[index] + '</span>';
            } else {
              cell += 'X';
            }

            cell += '</td>';
            row += cell;
          }

          row += '</tr>';
          tableHTML += row;
        }

        tableHTML += '</table></div>';

        if (productVariants.length > 0) {
          let variantsElement = productVariants[0];
          variantsElement.insertAdjacentHTML('afterend', tableHTML);
        }

        let numberInputs = document.querySelectorAll('.cart-quantity');
        numberInputs.forEach(function (input) {
          input.addEventListener('input', function () {
            let maxValue = parseInt(this.getAttribute('max'));
            let enteredValue = parseInt(this.value);
            if (enteredValue > maxValue) {
              this.value = maxValue;
            }
          });
        });

        // Función para añadir productos al carrito
        function addCart(color, size, quantity) {
          let cart = JSON.parse(localStorage.getItem('cart')) || [];
          let existingItem = cart.find((item) => item.color === color && item.size === size);
          if (existingItem) {
            existingItem.quantity += quantity;
          } else {
            cart.push({ color, size, quantity });
          }
          localStorage.setItem('cart', JSON.stringify(cart));

          // Actualizar el stock en la tabla
          let index = combinations.indexOf(color + '&' + size);
          if (index > -1) {
            stocks[index] -= quantity;
            if (stocks[index] < 0) {
              stocks[index] = 0;
            }
            updateStockTable();
          }

          let sizeInput = document.getElementById('variation_2');
          let colorInput = document.getElementById('variation_1');
          if (sizeInput === null) {
            colorInput = 'Unico color';
            sizeInput = document.getElementById('variation_1');
          }
          if (sizeInput && colorInput) {
            sizeInput.value = size;
            colorInput.value = color;

            let quantityInput = document.createElement('input');
            quantityInput.setAttribute('type', 'hidden');
            quantityInput.setAttribute('name', 'quantity');
            quantityInput.setAttribute('value', quantity);
            forms.appendChild(quantityInput);

            cartButton.click();

            console.log(quantityInput);
            
            forms.removeChild(quantityInput);
          }
        }

        // Función para actualizar la tabla de stock
        function updateStockTable() {
          let numberInputs = document.querySelectorAll('.cart-quantity');
          numberInputs.forEach(function (input) {
            let color = input.getAttribute('data-color');
            let size = input.getAttribute('data-size');
            let index = combinations.indexOf(color + '&' + size);
            if (index > -1) {
              input.setAttribute('max', stocks[index]);
              let stockSpan = input.nextElementSibling;
              if (stockSpan) {
                stockSpan.innerText = 'De: ' + stocks[index];
              }
            }
          });
        }

        // Manejo del botón de agregar al carrito
        function addToCartHandler() {
          showLoader(); // Mostrar loader cuando comienza el proceso

          let inputs = document.querySelectorAll('.cart-quantity');
          let items = [];

          inputs.forEach(function (input) {
            let quantity = parseInt(input.value);
            if (quantity > 0) {
              let color = input.getAttribute('data-color');
              let size = input.getAttribute('data-size');

              let index = combinations.indexOf(color + '&' + size);
              if (index > -1) {
                let stock = stocks[index];
                if (quantity > stock) {
                  input.value = stock;
                  quantity = stock;
                }
              }
              items.push({ color: color, size: size, quantity: quantity });
            }
          });

          // Agrupar los productos por color y tamaño
          let groupedItems = {};
          items.forEach(function (item) {
            let key = item.color + '&' + item.size;
            if (groupedItems[key]) {
              groupedItems[key].quantity += item.quantity;
            } else {
              groupedItems[key] = { color: item.color, size: item.size, quantity: item.quantity };
            }
          });

          let finalItems = Object.values(groupedItems);

          inputs.forEach((input) => (input.value = 0)); 
          
          finalItems.forEach(function (item) {
            addCart(item.color, item.size, item.quantity);
          });

          setTimeout(hideLoader, 1000); // Ocultar el loader después de 1 segundo
        }

        newButton.addEventListener('click', addToCartHandler);

        // Estilos adicionales responsivos
        const style = document.createElement('style');
        style.textContent = `
        .responsive-table {
          width: 100%;
        }
        
        .responsive-table table {
          width: 100%;
          border-collapse: collapse;
        }
        
        .responsive-table td, .responsive-table th {
          padding: 8px;
          text-align: center;
        }
        
        .button-addCart {
          margin-top: 10px;
        }
        
        @media (max-width: 768px) {
          .responsive-table td, .responsive-table th {
            font-size: 12px;
            padding: 4px;
          }
          
          .button-addCart {
            font-size: 14px;
            padding: 8px;
          }
        }
        `;

        document.head.appendChild(style);

        // Cargar el carrito desde localStorage al cargar la página
        document.addEventListener('DOMContentLoaded', function () {
          let cart = JSON.parse(localStorage.getItem('cart')) || [];
          cart.forEach(function (item) {
            addCart(item.color, item.size, item.quantity);
          });

          // Actualizar la tabla con los valores del carrito
          let numberInputs = document.querySelectorAll('.cart-quantity');
          numberInputs.forEach(function (input) {
            let color = input.getAttribute('data-color');
            let size = input.getAttribute('data-size');
            let cartItem = cart.find((item) => item.color === color && item.size === size);
            if (cartItem) {
              input.value = cartItem.quantity;
            }
          });
        });
      }

      /**
       * Changes the view of the products.
       * @async
       */
      const changeView = async () => {
        // Toggle the viewList in localStorage
        localStorage.setItem('viewList', !JSON.parse(localStorage.getItem('viewList')));

        // Disable the buy button and update the view
        disableBtnStartBuy();

        // Check if the view is list
        if (JSON.parse(localStorage.getItem('viewList'))) {
          // Create variable
          const link = window.location;
          let search = [];

          // Parse and save the search parameters
          if (link.search) search = link.search.replace('?', '').split('&');

          // Check and update the mpage parameter
          if (!link.href.includes('mpage=50') && !link.href.includes('mpage=')) {
            if (search[0] || search.length) search.push('mpage=50');
            else search[0] = 'mpage=50';
          } else if (!link.href.includes('mpage=50') && link.href.includes('mpage='))
            search.forEach((item, index) => {
              if (item.includes('mpage=') && item !== 'mpage=50') search[index] = 'mpage=50';
            });

          // Set the new link
          const newLink = `${link.origin}${link.pathname}?${search.join('&')}`;

          // Check if the new link is different from the current link and update the link
          if (newLink !== window.location.href) window.location.replace(newLink);

          // Set that the view has changed
          localStorage.setItem('isChanged', true);

          // Check if the button exists and execute the Loading function
          const btnChangeView = document.getElementById('switch-button');
          if (btnChangeView) {
            async function Loading() {
              btnChangeView.disabled = true;
              btnChangeView.innerHTML = 'Cargando <i class="spinner spinner-small span4 offset-4"/>';
            }
            await Loading();
          }

          // Auxiliary variables
          let children = [],
            auxProduct,
            products = null,
            grids = null;
          const productTable = document.querySelector('.js-product-table');

          const moveChildrenToContainer = (container, children) => {
            children.forEach((child) => container.appendChild(child));
          };

          let auxProductTable = null;

          // Get the products and save in a variable the products depending on the theme
          switch (themeName) {
            case 'Lifestyle':
              products = document.querySelector('.js-product-table');
              $('.js-item-product').map((_, element) => children.push(element));
              grids = document.querySelectorAll('.grid-row');
              grids.forEach((grid) => products.removeChild(grid));
              moveChildrenToContainer(products, children);
              break;

            case 'Material':
              products = defaultVariables.isSearch ? document.querySelector('.js-masonry-grid .js-masonry-grid') : document.querySelector('.js-masonry-grid');
              $('.js-item-product').map((_, element) => children.push(element));
              grids = document.querySelectorAll('.product-row');
              grids.forEach((grid) => products.removeChild(grid));
              moveChildrenToContainer(products, children);
              break;

            case 'Simple':
              $('.item-container').map((_, element) => children.push(element));
              productTable.classList.add('col-auto', 'full-width');
              productTable.parentNode.replaceChild(defaultVariables.defaultCatalog.cloneNode(true), productTable);
              grids = document.querySelectorAll('.grid-row');
              auxProductTable = document.querySelector('.js-product-table');
              products = defaultVariables.isSearch ? auxProductTable : document.querySelector('.container-product-grid');
              grids.forEach((grid) => auxProductTable.removeChild(grid));
              moveChildrenToContainer(auxProductTable, children);
              break;

            case 'Trend':
              $('.js-item-product').map((_, element) => children.push(element));
              products = document.querySelector('.js-masonry-grid');
              grids = document.querySelectorAll('.product-row');
              grids.forEach((grid) => products.removeChild(grid));
              moveChildrenToContainer(products, children);
              break;

            default:
              products = document.querySelector('.js-product-table');
              children = [...products.children];
              break;
          }

          // Iterates through each item in the product table
          children.forEach((child, index) => {
            // Auxiliary variables
            let nextChild = children[index + 1],
              product_id = null,
              auxName = null,
              auxImg = null,
              auxDatasetVariants = null;

            // Save the product id depending of the theme
            if (equals(themeName, 'Simple')) product_id = child.children[0].getAttribute('data-product-id');
            else product_id = child.getAttribute('data-product-id');

            // If product id exists and the theme is not "Simple" or "Silent"
            if (product_id && !equals(themeName, 'Silent') && !equals(themeName, 'Simple')) {
              // Remove the class "col-6" and add the class "col-12"
              child.classList.remove('col-6', 'col-md-4', 'col-lg-3', 'col-md-3', 'col-md-12', 'col-xs-12', 'col-sm-6', 'col-md-4', 'col-lg-4', 'col-md-6');
              child.classList.add('col-12');

              // If the theme is "Amazonas" add the style "border-bottom = 1px solid rgba(0, 0, 0, 0.2)"
              if (equals(themeName, 'Amazonas')) child.style.borderBottom = '1px solid rgba(0, 0, 0, 0.2)';
            } else if (product_id && equals(themeName, 'Simple')) {
              child.classList.add('col-12');
              child.setAttribute('style', 'max-width: 100% !important; width: 100% !important;');
            }

            // If product id exists and the child does not contain the class "no-price"
            if (product_id && !child?.classList?.contains('no-price')) {
              let datasetVariants;

              // Save the variants depending of the theme
              if (child?.children[0]?.children[0]?.dataset?.variants) datasetVariants = JSON.parse(child.children[0].children[0].dataset.variants);
              else datasetVariants = JSON.parse(child.children[0].dataset.variants);

              // If a product exists, it has variants
              if (datasetVariants.length > 1 || datasetVariants[0].option0 || datasetVariants[0].option1 || datasetVariants[0].option2) {
                // Create a copy of the product and delete it
                auxProduct = child.cloneNode(true);

                // Save the name, image, and variants depending of the theme
                switch (themeName) {
                  case 'Amazonas':
                  case 'Cubo':
                  case 'Idea':
                  case 'Lima':
                    auxName = auxProduct.querySelector('.item-name').children[0].innerHTML;
                    auxImg = auxProduct.querySelector('.item-image').children[0].children[0];
                    auxDatasetVariants = auxProduct.children[0];
                    break;

                  case 'Atlántico':
                    auxName = auxProduct.querySelector('.item-name').children[0].innerHTML;
                    auxImg = auxProduct.querySelector('.item-image').children[0].children[0];
                    auxDatasetVariants = auxProduct.children[0].children[0];
                    break;

                  case 'Bahia':
                  case 'Lifestyle':
                  case 'Rio':
                    auxName = auxProduct.querySelector('.item-name').children[0].innerHTML;
                    auxImg = auxProduct.querySelector('.js-item-image');
                    auxDatasetVariants = auxProduct.children[0];
                    break;

                  case 'Material':
                    auxName = auxProduct.querySelector('.js-item-name').children[0].innerHTML;
                    auxImg = auxProduct.querySelector('.item-image-link').children[0];
                    auxDatasetVariants = auxProduct.children[0];
                    break;

                  case 'Silent':
                  case 'Trend':
                    auxName = auxProduct.querySelector('.item-name').innerHTML;
                    auxImg = auxProduct.querySelector('.js-item-image');
                    auxDatasetVariants = auxProduct.children[0];
                    break;

                  default:
                    auxName = auxProduct.querySelector('.js-item-name.item-name').children[0].innerHTML;
                    auxImg = auxProduct.querySelector('.item-image').parentNode;
                    auxDatasetVariants = auxProduct.children[0].children[0];
                    break;
                }

                // Create a card to see the variants
                const nameProduct = getName(auxName, true);
                const productVariants = document.createElement('div');
                if (productVariants) {
                  productVariants.classList.add('item');
                  productVariants.innerHTML = stringVariants;

                  // Set style depending on the theme
                  switch (themeName) {
                    case 'Amazonas':
                      addStyles(productVariants, 'display: flex; flex: 0 0 100%; max-width: 100%; border-bottom: 1px solid rgba(0, 0, 0, 0.2); margin-bottom: 1rem;');
                      break;

                    case 'Atlántico':
                      addStyles(productVariants, 'display: flex; flex: 0 0 100%; max-width: 100%; padding-right: 15px; padding-left: 15px;');
                      break;

                    case 'Bahia':
                    case 'Idea':
                    case 'Lima':
                    case 'Rio':
                      productVariants.classList.add('d-flex', 'mb-3', 'col-12');
                      break;

                    case 'Cubo':
                      addStyles(productVariants, 'display: flex; flex: 0 0 100%; max-width: 100%; margin-bottom: 1rem; border-bottom: 3px solid #00B2FF; background: #fafafa;');
                      break;

                    case 'Lifestyle':
                    case 'Silent':
                      addStyles(productVariants, 'display: flex; margin-bottom: 1rem; flex: 0 0 100%; max-width: 100%; width: calc(100% - 20px); margin: 10px 20px 10px;');
                      break;

                    case 'Material':
                      productVariants.classList.add('item-container');
                      addStyles(productVariants, 'display: flex; margin-bottom: 1rem; flex: 0 0 100%; max-width: 100%; width: 100%;');
                      break;

                    case 'Trend':
                      productVariants.classList.remove('item');
                      productVariants.classList.add('item-container');
                      addStyles(productVariants, 'display: flex; margin-bottom: 1rem; flex: 0 0 100%; max-width: 100%; width: 100%;');
                      break;

                    default:
                      productVariants.classList.add('d-flex', 'item', 'col-auto', 'full-width');
                      break;
                  }

                  // Set the name and style image of the product
                  productVariants.querySelector('#item-image').children[0].appendChild(auxImg.cloneNode(true));
                  addStyles(productVariants?.querySelector('.js-item-image'), 'height: 100px; width: auto; max-width: 100%; object-fit: contain;');

                  // Set the name of the product depending on the theme
                  switch (themeName) {
                    case 'Amazonas':
                      productVariants.querySelector('.js-item-name').innerHTML = nameProduct;
                      productVariants.addEventListener('mouseover', () => mouseOver(productVariants));
                      productVariants.addEventListener('mouseout', () => mouseOut(productVariants));
                      break;

                    case 'Cubo':
                    case 'Idea':
                    case 'Lima':
                      productVariants.querySelector('.js-item-name').innerHTML = nameProduct;
                      break;

                    default:
                      productVariants.querySelector('.item-name').innerHTML = nameProduct;
                      break;
                  }

                  // Set event to see the variants
                  productVariants.querySelector('#see-variants').addEventListener('click', () => {
                    if (productVariants.querySelector('#see-variants').innerHTML.includes('Ver variantes')) productVariants.querySelector('#see-variants').innerHTML = 'Ocultar variantes';
                    else productVariants.querySelector('#see-variants').innerHTML = 'Ver variantes';
                  });

                  // Get the btn to add to cart, the input quantity, and the btn to change the quantity
                  let btnDown = productVariants.querySelector('#cart-quantity-btn-substract'),
                    btnUp = productVariants.querySelector('#cart-quantity-btn-add'),
                    inputQuantity = productVariants.querySelector('#cart-quantity-input');

                  // Set the event to change the quantity
                  if (btnDown)
                    btnDown.addEventListener('click', () => {
                      changeQuantityAux(inputQuantity, 'down');
                    });
                  if (btnUp)
                    btnUp.addEventListener('click', () => {
                      changeQuantityAux(inputQuantity, 'up');
                    });

                  // Set the id of the btn to add to cart, the input quantity, and the btn to change the quantity
                  let idName = replaceName(nameProduct);
                  if (btnDown) btnDown.id = `cart-quantity-btn-substract-${idName}`;
                  if (inputQuantity) inputQuantity.id = `cart-quantity-input-${idName}`;
                  if (btnUp) btnUp.id = `cart-quantity-btn-add-${idName}`;

                  // Replace the product with the variants
                  if (equals(themeName, 'Material') || equals(themeName, 'Trend')) products.replaceChild(productVariants, child);
                  else document.querySelector('.js-product-table').replaceChild(productVariants, child);

                  // Iterates for each variant
                  datasetVariants.forEach((data) => {
                    // The data of each variant is added and a clone is created
                    auxDatasetVariants.dataset.variants = `[${JSON.stringify(data)}]`;
                    let newProduct = auxProduct.cloneNode(true);

                    // Set the style of the new product depending of the theme
                    if (equals(themeName, 'Amazonas')) addStyles(newProduct, 'display: none; border-bottom: 1px solid rgba(0, 0, 0, 0.2); margin-bottom: 1rem; padding: 0; padding-bottom: 15px; border-radius: 20px;');
                    else addStyles(newProduct, 'display: none;');

                    // Set event to see the variants
                    productVariants.querySelector('#see-variants').addEventListener('click', () => {
                      let stylesNewProduct = newProduct.getAttribute('style');

                      // Show or hide the product
                      if (stylesNewProduct.includes('display: none;')) addStyles(newProduct, `${stylesNewProduct.replace('display: none;', 'display: initial;')}`);
                      else addStyles(newProduct, `${stylesNewProduct.replace('display: initial;', 'display: none;')}`);
                    });

                    // The information of each product is saved
                    let dataObj = data;

                    // The form to add the products to the cart is replaced by that of each variant
                    let divActionAux = null;
                    if (equals(themeName, 'Cubo')) divActionAux = newProduct.querySelector('.js-item-buy-open').parentNode;
                    else if (equals(themeName, 'Idea') || equals(themeName, 'Lifestyle')) divActionAux = newProduct.querySelector('.js-item-variants');
                    else if (equals(themeName, 'Lima')) divActionAux = newProduct.querySelector('.js-quickshop-modal-open').parentNode;
                    else divActionAux = newProduct.querySelector('.item-actions');

                    // If the theme is Lifestyle, the button to add to cart is deleted
                    if (equals(themeName, 'Lifestyle')) {
                      let divToDelete = newProduct.querySelector('.js-item-buy-open');
                      if (divToDelete) divToDelete.parentNode.removeChild(divToDelete);
                    }

                    // Replace the form to add to cart
                    if (equals(themeName, 'Trend')) divActionAux?.replaceChild(form.cloneNode(true), divActionAux.querySelector('.js-quickshop-modal-open'));
                    else divActionAux?.replaceChild(form.cloneNode(true), divActionAux.children[0]);

                    // Add event to the btn to add to cart
                    divActionAux?.querySelector('.js-addtocart.js-prod-submit-form').addEventListener('click', () => {
                      defaultVariables.productsNotSaved -= 1;
                    });

                    // If the price exists, it is added to the product card
                    if (dataObj.price_short && newProduct?.querySelector('.item-price')) newProduct.querySelector('.item-price').innerHTML = dataObj.price_short;

                    // If the product id exists, it is added to the product card
                    if (dataObj.product_id && newProduct?.querySelector('#input_id')) newProduct.querySelector('#input_id').value = dataObj.product_id;

                    // If there is a variant name, it is added to the product card
                    if ((dataObj.option0 || dataObj.option1 || dataObj.option2) && newProduct?.querySelector('#product-form-variant')) {
                      const form = newProduct.querySelector('#product-form-variant');
                      form.querySelector('input[name=add_to_cart]').value = data.product_id;
                      let innerHTML = newProduct.querySelector('.item-name').innerHTML;

                      // Add the variants to the product card
                      if (dataObj.option0) innerHTML = formatNameVariants(innerHTML, dataObj.option0, 'variation[0]', form);
                      if (dataObj.option1) innerHTML = formatNameVariants(innerHTML, dataObj.option1, 'variation[1]', form);
                      if (dataObj.option2) innerHTML = formatNameVariants(innerHTML, dataObj.option2, 'variation[2]', form);

                      // Set the name of the product
                      newProduct.querySelector('.item-name').innerHTML = innerHTML;
                    }

                    // If the theme is Cubo, the price is replaced
                    if (equals(themeName, 'Cubo') || equals(themeName, 'Lima')) {
                      let formAux = newProduct.querySelector('.js-product-form');
                      formAux.parentNode.replaceChild(divPriceAux.cloneNode(true), formAux);
                    }

                    // If there is a following product, the product is added before,
                    // if not, it is added to the end of the list.
                    if (equals(themeName, 'Material') || equals(themeName, 'Trend')) {
                      if (nextChild) products.insertBefore(newProduct, nextChild);
                      else products.appendChild(newProduct);
                    } else if (nextChild) document.querySelector('.js-product-table').insertBefore(newProduct, nextChild);
                    else document.querySelector('.js-product-table').appendChild(newProduct);
                  });
                }
              } else if (equals(datasetVariants.length, 1) && !equals(themeName, 'Atlántico') && !equals(themeName, 'Bahia')) {
                // If theme isn't Atlántico or Bahia, create a new input to add the quantity
                const inputQuantity = document.createElement('input');
                inputQuantity.classList.add('js-quantity-input', 'hidden');
                inputQuantity.setAttribute('type', 'number');
                inputQuantity.setAttribute('name', 'quantity');
                inputQuantity.setAttribute('id', 'input_quantity');
                inputQuantity.setAttribute('value', 0);
                inputQuantity.setAttribute('min', 0);
                addStyles(inputQuantity, 'display:none !important');

                // Add the input to the form
                const form = child.querySelector('.js-product-form');
                if (form) form.appendChild(inputQuantity);
              }
            }
          });

          // Variables auxiliars
          let cards = [...document.querySelectorAll(cardsSelector)];

          // Iterates for each card of the products
          cards.forEach((card) => {
            // For each child add class
            let cardImg = card.querySelector(cardImgSelector),
              cardDescription = card.querySelector(cardDescriptionSelector);

            // Variables auxiliars
            let dataVariants,
              stock,
              dataObj = {},
              newLinkImg,
              divAction = null,
              variantsAux = null,
              nameProduct = '',
              cardParent = null,
              stylesCardParent = '',
              divActionAux = card.querySelector('.js-item-variants.hidden'),
              a = card.querySelectorAll('a');

            // Set the target of the links to _blank
            a.forEach((a) => a.setAttribute('target', '_blank'));

            if (cardDescription?.parentNode?.dataset?.variants || card?.dataset?.variants) {
              // If it is a product with variants select the variant string
              if (equals(themeName, 'Amazonas') || equals(themeName, 'Simple')) variantsAux = cardDescription.parentNode.dataset.variants;
              else variantsAux = card.dataset.variants;

              // Transform the string to a JSON object
              dataObj = JSON.parse(variantsAux)[0];
              newLinkImg = 'https:' + dataObj.image_url;
            }

            // If the theme is Lifestyle, the divActionAux is removed
            if (equals(themeName, 'Lifestyle')) divActionAux = card.querySelector('.js-item-variants.item-buy-variants.item-buy-variants-even');
            if (divActionAux) divActionAux.parentNode.removeChild(divActionAux);

            // Remove the class span3, the class col-auto and full-width
            card.classList.remove('span3');
            card.children[0].children[0].classList.add('col-auto', 'full-width');

            let stylesElement = null,
              linkToDelete = null,
              inputToDelete = null,
              linkToDeleteLima = null;

            // Set styles to the card depending on the theme
            switch (themeName) {
              case 'Amazonas':
                cardParent = card.parentNode;
                stylesElement = cardParent.parentNode.getAttribute('style');
                addStyles(card, 'height: 100%; display: flex;');

                if (!equals(cardParent.classList[0], 'js-item-product')) {
                  addStyles(cardParent, 'height: 100%; border: none;');
                  addStyles(cardParent?.parentNode, `${stylesElement.replace(' padding-bottom: 15px;', '')} `);
                }
                break;

              case 'Atlántico':
                card.classList.add('d-flex');
                break;

              case 'Bahia':
              case 'Idea':
              case 'Rio':
                card.classList.add('d-flex', 'justify-conten-center', 'flex-wrap');
                card.parentNode.classList.add('col-12');
                break;

              case 'Cubo':
                if (divAction) divAction.classList.remove('position-absolute');
                linkToDelete = card.querySelector('.js-item-link');
                if (linkToDelete) linkToDelete.parentNode.removeChild(linkToDelete);
                card.classList.add('row');
                card.parentNode.classList.add('col-12');
                if (equals(themeName, 'Cubo')) addStyles(card, 'background: #fafafa; border-bottom: 3px solid #00B2FF;');
                break;

              case 'Lifestyle':
                addStyles(card, 'display: flex; justify-content: center; flex-wrap: wrap; position: relative;');
                cardParent = card.parentNode;
                stylesCardParent = cardParent.getAttribute('style');
                inputToDelete = card.querySelector('.js-product-quantity:not(#input_quantity)');
                if (inputToDelete) inputToDelete.parentNode.removeChild(inputToDelete);
                addStyles(cardParent, `${stylesCardParent} flex: 0 0 100%; max-width: 100%; width: 100% !important; margin: 0 !important; padding: 0 !important;`);
                break;

              case 'Lima':
                linkToDeleteLima = card.querySelector('.js-item-link');
                if (linkToDeleteLima) linkToDeleteLima.parentNode.removeChild(linkToDeleteLima);
                card.classList.add('row', 'justify-content-center');
                card.parentNode.classList.add('col-12');
                break;

              case 'Material':
                addStyles(card, 'display: flex; justify-content: center; flex-wrap: wrap;');
                addStyles(card?.parentNode, 'flex: 0 0 100%; max-width: 100%; width: 100% !important;');
                cardParent = card.parentNode.parentNode;
                stylesCardParent = cardParent.getAttribute('style');
                addStyles(cardParent, `${stylesCardParent?.includes('display: none;') ? 'display: none;' : ''} flex: 0 0 100%; max-width: 100%; width: 100% !important;`);
                break;

              case 'Silent':
                if (defaultVariables.isSearch) addStyles(card, 'display: flex; justify-content: center; position: relative;');
                else addStyles(card, 'display: flex; justify-content: center; flex-wrap: wrap; position: relative;');
                cardParent = card.parentNode;
                stylesCardParent = cardParent.getAttribute('style');
                addStyles(cardParent, `${stylesCardParent} flex: 0 0 100%; max-width: 100%; width: calc(100% - 20px) !important;`);
                break;

              case 'Trend':
                addStyles(card, 'flex: 0 0 100%; max-width: 100%; width: 100% !important; display: flex; justify-content: center; flex-wrap: wrap;');
                addStyles(card?.parentNode, 'flex: 0 0 100%; max-width: 100%; width: 100% !important;');
                addStyles(cardImg?.parentNode, 'flex: 0 0 100%; max-width: 100%; width: 100% !important; display: flex; justify-content: center; flex-wrap: wrap;');
                cardParent = card.parentNode.parentNode;
                stylesCardParent = cardParent.getAttribute('style');
                addStyles(cardParent, `${stylesCardParent.includes('display: none;') ? 'display: none;' : ''} flex: 0 0 100%; max-width: 100%; width: 100% !important;`);
                break;

              default:
                addStyles(card?.children[0]?.children[0], 'display: flex; justify-content: center !important; flex-wrap: wrap !important;');
                break;
            }

            if (cardImg) {
              // If the image exists, the "col-2" class is added and the sizes are set for the image
              cardImg.classList.add('span2');
              addStyles(cardImg, 'height: 100px; width: auto; ');
              let img = cardImg.querySelector('img');
              addStyles(img, 'height: 100px; width: auto; max-width: 100%; object-fit: contain;');

              if (img.src && img.src !== newLinkImg && newLinkImg) {
                // If the image is not the same as the new one, the image is replaced
                img.src = newLinkImg;
                img.setAttribute('src', newLinkImg);

                if (img.dataset.srcset) {
                  let datasetSrcset = img.dataset.srcset;
                  datasetSrcset.replace(/[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi, newLinkImg);
                  img.srcset = datasetSrcset;
                  img.setAttribute('srcset', newLinkImg);
                }
              }

              // Variable auxiliar
              let divBuy;

              let inputBtn = null,
                linkAux = null;

              // Save the "item-actions" div depending on the theme
              switch (themeName) {
                case 'Amazonas':
                  cardImg.classList.add('col-2');
                  divBuy = card.querySelector('.item-buy');
                  if (divBuy) divBuy.parentNode.removeChild(divBuy);
                  divAction = card.querySelector('.item-actions.row.justify-content-center');
                  addStyles(divAction, 'visibility: initial; opacity: 1; position: absolute; bottom: -12px; left: 50%; transform: translateX(-50%); width: 200px; height: max-content; background: none; box-shadow: none;');
                  break;

                case 'Atlántico':
                  cardImg.classList.add('col-2');
                  divAction = cardImg.children[0].children[3];
                  break;

                case 'Bahia':
                case 'Rio':
                  addStyles(cardImg, 'height: 100px; width: auto; background-color: transparent;');
                  cardImg.classList.add('col-2');
                  divAction = cardImg.querySelector('.item-actions');
                  divAction?.classList.add('d-flex', 'justify-content-center', 'w-100');
                  break;

                case 'Cubo':
                case 'Lima':
                  cardImg.classList.add('col-2');
                  divAction = cardDescription.querySelector('.item-actions');
                  break;

                case 'Idea':
                  cardImg.classList.add('col-2');
                  divAction = cardImg.querySelector('.js-product-form');
                  inputBtn = divAction.querySelector('.js-addtocart');
                  inputBtn?.setAttribute('style', 'margin: 0; max-width: 230px;');
                  linkAux = divAction.querySelector('a');
                  linkAux?.setAttribute('style', 'width: 100%; max-width: 230px;');
                  divAction.classList.add('d-flex', 'justify-content-center', 'w-100');
                  divBuy = cardImg.querySelector('.item-buy');
                  divBuy.parentNode.removeChild(divBuy);
                  break;

                case 'Lifestyle':
                  addStyles(cardImg, 'flex: 0 0 16.666667%; max-width: 16.666667%; height: 100px; width: auto;');
                  divAction = card.querySelector('.js-product-form');
                  break;

                case 'Material':
                  addStyles(cardImg, 'flex: 0 0 16.666667%; max-width: 16.666667%; height: 100px; width: auto;');
                  divAction = cardDescription.querySelector('.item-actions');
                  break;

                case 'Silent':
                  addStyles(cardImg, 'flex: 0 0 16.666667%; max-width: 16.666667%; height: 100px; width: auto;');
                  divAction = cardImg.querySelector('.item-actions');
                  break;

                case 'Trend':
                  addStyles(cardImg, 'flex: 0 0 16.666667%; max-width: 16.666667%; height: 100px; width: auto;');
                  addStyles(cardImg?.parentNode, 'flex: 0 0 100%; max-width: 100%; width: 100% !important; display: flex; justify-content: center; flex-wrap: wrap;');
                  divAction = cardDescription.querySelector('.item-actions');
                  break;

                default:
                  if (equals(themeName, 'Simple')) addStyles(cardImg, 'height: 100px;');
                  img.classList.remove('item-image', 'img-absolute', 'img-absolute-centered');
                  if (cardImg.querySelector('.js-item-variants.item-buy-variants.hidden')) cardImg.removeChild(cardImg.querySelector('.js-item-variants.item-buy-variants.hidden'));
                  break;
              }
            }

            // If there is a description of each product
            if (cardDescription) {
              let price = cardDescription.querySelector('.js-price-display'),
                divAux,
                priceContainer,
                itemQuantityAux;

              // Store price and stock if they exist
              if (dataObj.price_short) dataVariants = parseFloat(dataObj.price_short.replace(`${moneyTxt}`, '').replaceAll('.', '').replace(',', '.'));

              // Store stock if it exists, is 0, or is null
              if (dataObj.stock || equals(dataObj.stock, 0) || equals(dataObj.stock, null)) stock = dataObj.stock;

              // Add styles to the description of each product
              switch (themeName) {
                case 'Amazonas':
                case 'Atlántico':
                  divAux = cardDescription.querySelector('.js-item-name').children[0];
                  nameProduct = divAux.innerHTML;
                  priceContainer = cardDescription.children[0];
                  itemQuantityAux = cardDescription.parentNode.querySelector('.js-quantity-input');
                  addStyles(divAux.parentNode, 'display: block; text-transform: capitalize;');
                  cardDescription.classList.add('col-10');
                  addStyles(cardDescription, 'text-aling: center; display: flex; justify-content: space-around; align-items: center; border: none !important;');
                  addStyles(cardDescription.children[0], 'height: 100%; width: 100%; display: flex; justify-content: space-around; align-items: center;');
                  break;

                case 'Bahia':
                  divAux = cardDescription.querySelector('.js-item-name').children[0];
                  nameProduct = divAux.innerHTML;
                  priceContainer = cardDescription.children[0];
                  itemQuantityAux = cardDescription.parentNode.querySelector('.js-quantity-input');
                  addStyles(divAux?.parentNode, 'display: block; text-transform: capitalize;');
                  cardDescription.classList.add('col-10');
                  addStyles(cardDescription?.children[0], 'height: 100%; width: 100%; display: flex; justify-content: space-around; align-items: center;');
                  addStyles(cardDescription, 'height: 100%; width: 100%; display: flex; justify-content: space-around; align-items: center;');
                  break;

                case 'Cubo':
                case 'Lima':
                  divAux = cardDescription.querySelector('.js-item-name').children[0];
                  nameProduct = divAux.innerHTML;
                  priceContainer = cardDescription.children[0];
                  itemQuantityAux = cardDescription.parentNode.querySelector('.js-quantity-input');
                  addStyles(divAux.parentNode, 'display: block; text-transform: capitalize;');
                  cardDescription.classList.add('col-10');
                  cardDescription.classList.add('row', 'justify-content-around', 'align-items-center', 'text-center');
                  cardDescription.classList.remove('item-description-large');
                  addStyles(cardDescription, 'border: none;');
                  cardDescription.children[0].classList.add('d-flex', 'justify-content-around', 'align-items-center', 'col-9');
                  cardDescription.children[0].classList.remove('position-absolute', 'item-description-container');
                  break;

                case 'Idea':
                  divAux = cardDescription.querySelector('.js-item-name').children[0];
                  nameProduct = divAux.innerHTML;
                  addStyles(divAux?.parentNode, 'display: block; text-transform: capitalize;');
                  cardDescription.classList.add('col-10');
                  cardDescription.classList.add('d-flex', 'justify-content-around', 'align-items-center', 'text-center');
                  priceContainer = cardDescription.children[0];
                  itemQuantityAux = divAction.querySelector('.js-quantity-input');
                  addStyles(cardDescription?.children[0], 'height: 100%; width: 100%; display: flex; justify-content: space-around; align-items: center;');
                  break;

                case 'Lifestyle':
                  divAux = cardDescription.querySelector('.item-name');
                  nameProduct = divAux.innerHTML;
                  priceContainer = cardDescription.children[0];
                  itemQuantityAux = card.querySelector('.js-quantity-input');
                  addStyles(cardDescription, 'display: flex; justify-content: center; align-items: center; text-align: center; flex: 0 0 83.333333%; max-width: 83.333333%; padding: 0 !important;');
                  addStyles(cardDescription.children[0], 'display: flex; justify-content: center; align-items: center; text-align: center; width: 100%; padding: 0 !important;');
                  break;

                case 'Material':
                  addStyles(divAction, 'margin-bottom: 1rem;');
                  divAux = cardDescription.querySelector('.js-item-name').children[0];
                  nameProduct = divAux.innerHTML;
                  itemQuantityAux = card.parentNode.querySelector('#input_quantity');
                  priceContainer = cardDescription;
                  addStyles(cardDescription, 'display: flex; justify-content: center; align-items: center; text-align: center; flex: 0 0 83.333333%; max-width: 83.333333%; padding: 0 !important;');
                  break;

                case 'Rio':
                  divAux = cardDescription.querySelector('.js-item-name').children[0];
                  nameProduct = divAux.innerHTML;
                  priceContainer = cardDescription.children[0];
                  itemQuantityAux = cardDescription.querySelector('#input_quantity');
                  addStyles(divAux?.parentNode, 'display: block; text-transform: capitalize;');
                  cardDescription.classList.add('col-10');
                  addStyles(cardDescription?.children[0], 'height: 100%; width: 100%; display: flex; justify-content: space-around; align-items: center; flex-wrap: wrap; text-align: center;');
                  addStyles(cardDescription, 'height: 100%; width: 100%; display: flex; justify-content: space-around; align-items: center;');
                  cardDescription.children[0].setAttribute('href', '');
                  break;

                case 'Silent':
                  divAux = cardDescription.querySelector('.js-item-name');
                  nameProduct = divAux.innerHTML;
                  priceContainer = cardDescription;
                  itemQuantityAux = card.querySelector('.js-quantity-input');
                  addStyles(cardDescription, 'display: flex; justify-content: center; align-items: center; text-align: center; flex: 0 0 83.333333%; max-width: 83.333333%; padding: 0 !important;');
                  break;

                case 'Trend':
                  divAux = cardDescription.querySelector('.js-item-name');
                  nameProduct = divAux.innerHTML;
                  addStyles(cardDescription?.children[0], 'flex: 0 0 66.66666667%; max-width: 66.66666667%;');
                  addStyles(cardDescription?.children[0]?.children[0], 'flex: 0 0 100%; max-width: 100%; width: 100% !important; display: flex; justify-content: space-around; align-items: center;');
                  priceContainer = cardDescription?.children[0]?.children[0];
                  itemQuantityAux = card.querySelector('.js-quantity-input');
                  addStyles(divAction, 'margin-bottom: 1rem;');
                  addStyles(cardDescription, 'display: flex; justify-content: center; align-items: center; text-align: center; flex: 0 0 83.333333%; max-width: 83.333333%; padding: 0 !important;');
                  break;

                default:
                  nameProduct = cardDescription.querySelector('.js-item-name').children[0].innerHTML;
                  cardDescription.querySelector('.js-item-name').innerHTML = '';
                  divAux = document.createElement('div');
                  divAux.classList.add('mb-3');
                  cardDescription.querySelector('.js-item-name').appendChild(divAux);
                  priceContainer = cardDescription.querySelector('.js-item-name');
                  itemQuantityAux = cardDescription.parentNode.querySelector('.js-quantity-input');
                  if (price) {
                    price.style.fontWeight = 'initial';
                    price.classList.remove('h6');
                    let div = document.createElement('div');
                    div.classList.add('mb-3');
                    div.appendChild(price);
                    priceContainer.insertBefore(div, cardDescription.querySelector('.item-stock-container'));
                  }
                  cardDescription.classList.add('span10', 'd-flex', 'align-items-center', 'text-center');
                  cardDescription.children[0].classList.add('span9', 'align-items-center');
                  cardDescription.children[0].style.height = '100%';
                  addStyles(cardDescription?.children[0], 'display:flex !important');
                  if (cardDescription.children[1]) {
                    cardDescription.children[1].classList.add('d-flex', 'full-width');
                    addStyles(cardDescription?.children[1], 'justify-content: center !important');
                  }
                  break;
              }

              // Format name product
              divAux.innerHTML = formatName(nameProduct, dataVariants);

              // Format price
              if (['Bahia', 'Idea', 'Rio'].includes(themeName)) divPrice.children[0].innerHTML = formatPrice(dataVariants);

              let notPriceString = `${moneyTxt}0,00`;
              if (dataVariants) {
                // Price is added to the card description depending on the theme
                switch (themeName) {
                  case 'Bahia':
                  case 'Idea':
                  case 'Rio':
                    cardDescription.querySelector('.item-price-container').children[1].innerHTML = notPriceString;
                    priceContainer.insertBefore(divPrice.cloneNode(true), priceContainer.querySelector('.item-price-container'));
                    break;

                  case 'Material':
                    priceContainer.querySelector('.js-price-display').innerHTML = notPriceString;
                    divPrice.children[0].innerHTML = formatPrice(dataVariants);
                    priceContainer.insertBefore(divPrice.cloneNode(true), cardDescription.querySelector('.row.m-none'));
                    break;

                  case 'Simple':
                    divPrice.children[0].innerHTML = notPriceString;
                    cardDescription.children[0].appendChild(divPrice.cloneNode(true));
                    break;

                  default:
                    priceContainer.querySelector('.js-price-display').innerHTML = notPriceString;
                    divPrice.children[0].innerHTML = formatPrice(dataVariants);
                    priceContainer.insertBefore(divPrice.cloneNode(true), priceContainer.querySelector('.item-price-container'));
                    break;
                }
              } else {
                // 'No disponible' is added to the card description depending on the theme
                divPrice.children[0].innerHTML = notAvalibleTxt;
                let targetNode = null;

                if (equals(themeName, 'Simple')) {
                  cardDescription.children[0].appendChild(divPrice.cloneNode(true));
                  price.innerHTML = `${notAvalibleTxt}`;
                } else {
                  if (equals(themeName, 'Silent')) targetNode = priceContainer.querySelector('.item-price-container');
                  else if (equals(themeName, 'Material')) targetNode = priceContainer.querySelector('.row.m-none');

                  if (targetNode) priceContainer.replaceChild(divPrice.cloneNode(true), targetNode);
                  else priceContainer.appendChild(divPrice.cloneNode(true));

                  priceContainer.appendChild(divPrice.cloneNode(true));
                }
              }

              // Set the stock
              if (stock) divStock.children[0].innerHTML = stock;
              else divStock.children[0].innerHTML = equals(stock, 0) ? `${notStockTxt}` : '&infin;';

              // Stock is added to the card description depending on the theme
              let divStockAux = null;
              if (['Amazonas', 'Atlántico', 'Bahia', 'Cubo', 'Idea', 'Rio', 'Simple'].includes(themeName)) divStockAux = cardDescription.children[0];
              else if (['Trend', 'Lima'].includes(themeName)) divStockAux = cardDescription.children[0].children[0];
              else divStockAux = cardDescription;

              if (equals(themeName, 'Rio')) divStockAux.insertBefore(divStock.cloneNode(true), divStockAux.children[3]);
              else divStockAux.appendChild(divStock.cloneNode(true));

              // Styles, properties and classes are added to html elements
              const div = divQuantity.cloneNode(true);
              const input = div.querySelector(cartQuantityInputSelector);
              if (stock || equals(stock, 0)) {
                input.max = stock;
                input.setAttribute('max', stock);
              }

              // Set the id of the input
              let idName = replaceName(getName(nameProduct));

              // Function to handle quantity change
              const handleQuantityChange = (incrementDecrement) => {
                changeQuantity(input, itemQuantityAux, cardDescription.querySelector(inputPriceSelector), dataVariants, incrementDecrement, cardDescription.querySelector('.js-stock-display').innerHTML);
              };

              // Function to add event listeners
              const addButtonEventListener = (button, idSuffix, handler) => {
                const buttonVariant = document.getElementById(`cart-quantity-btn-${idSuffix}-${idName}`);
                button.addEventListener('click', handler);
                if (buttonVariant) buttonVariant.addEventListener('click', handler);
              };

              const buttonSubtract = div.querySelector('#substract-btn');
              const buttonAdd = div.querySelector('#add-btn');

              // Adding event listeners for quantity change
              addButtonEventListener(buttonSubtract, 'substract', () => handleQuantityChange('decrement'));
              addButtonEventListener(buttonAdd, 'add', () => handleQuantityChange('increment'));

              // Disabling elements if there is no stock
              if (!dataVariants || equals(stock, 0)) {
                input.disabled = true;
                buttonSubtract.disabled = true;
                buttonAdd.disabled = true;

                // Setting attributes for disabled state
                [input, buttonSubtract, buttonAdd].forEach((element) => {
                  element.setAttribute('disabled', true);
                });
              }

              // Function to add event listener to input
              const addInputEventListener = (input, handler) => {
                input.addEventListener('change', handler);
              };

              const inputVariant = document.getElementById(`cart-quantity-input-${idName}`);

              addInputEventListener(input, handleQuantityChange);
              if (inputVariant) addInputEventListener(inputVariant, handleQuantityChange);

              // The created and modified elements are added
              if (cardDescription.children[1] && equals(themeName, 'Simple')) cardDescription.insertBefore(div, cardDescription.children[1]);
              else cardDescription.appendChild(div);

              // The button to add to the cart is added
              if (divAction) {
                if (equals(themeName, 'Amazonas')) cardDescription.appendChild(divAction);
                else if (['Bahia', 'Silent', 'Rio'].includes(themeName)) {
                  card.appendChild(divAction);
                  divAction.querySelector('.js-item-submit-container')?.classList.add('position-relative');
                } else if (['Cubo', 'Idea', 'Lifestyle', 'Material', 'Trend'].includes(themeName)) card.appendChild(divAction);
                else if (equals(themeName, 'Lima')) {
                  cardDescription.removeChild(divAction);
                  cardDescription.parentNode.appendChild(divAction);
                } else cardDescription.children[0].appendChild(divAction);
              }

              // Set the default value in the themes
              if (itemQuantityAux && !['Bahia', 'Rio', 'Simple'].includes(themeName)) {
                itemQuantityAux?.setAttribute('value', 0);
                cardDescription.querySelector('.item-price').innerHTML = notPriceString;
              }

              // If the stock are 0 set the price in 0
              if (equals(stock, 0)) cardDescription.querySelector('.item-price').innerHTML = notPriceString;

              // Get the description depending on the theme
              let cardDescriptionChildren;
              if (['Material', 'Silent'].includes(themeName)) cardDescriptionChildren = [...cardDescription.children];
              else if (equals(themeName, 'Trend')) cardDescriptionChildren = [...cardDescription.children[0].children[0].children];
              else cardDescriptionChildren = [...cardDescription.children[0].children];

              // Add the classes to the elements of the description
              const applyStylesToElement = (element, styles) => {
                const currentStyles = element.getAttribute('style') || '';
                element.setAttribute('style', `${currentStyles} ${styles}`);
              };

              cardDescriptionChildren.forEach((children) => {
                let element = children;

                if (element.classList[0] !== 'item-actions') {
                  let isName = false;
                  if (equals(element.classList[0], 'js-item-name') || equals(element.classList[0], 'title') || equals(element.id, 'div-quantity')) {
                    isName = true;
                  }

                  if (isName) {
                    if (['Amazonas', 'Atlántico', 'Bahia', 'Cubo', 'Lima'].includes(themeName)) {
                      element.classList.add('col-4');
                    } else if (themeName === 'Rio') {
                      element.classList.add('col-3');
                    } else if (['Material', 'Silent', 'Trend'].includes(themeName)) {
                      applyStylesToElement(element, 'flex: 0 0 25%; max-width: 25%;');
                    } else {
                      element.classList.add('span4');
                    }
                  } else if (['Amazonas', 'Atlántico', 'Bahia', 'Cubo', 'Lima'].includes(themeName)) {
                    element.classList.add('col-3');
                  } else if (['Material', 'Silent', 'Trend'].includes(themeName)) {
                    applyStylesToElement(element, 'flex: 0 0 16.666667%; max-width: 16.666667%;');
                  } else {
                    element.classList.add('span3');
                  }
                }
              });
            }
          });

          // HTML elements are added to the products container
          products.appendChild(btnAddToCart);
          let childrenProductsAux = products.children;

          if (childrenProductsAux[0].classList.contains('span12') && (equals(themeName, 'Simple') || equals(themeName, 'Lifestyle'))) {
            let divAuxSeparator = document.createElement('div');
            divAuxSeparator.classList.add('span12');
            products.insertBefore(divAuxSeparator, childrenProductsAux[0]);

            divHeaders.classList.add('span12');
            divHeaders.classList.remove('span10');
          }

          // Function to determine the position to insert the headers
          const getHeadersInsertPosition = () => {
            if (equals(themeName, 'Lifestyle') || (equals(themeName, 'Trend') && defaultVariables.isSearch)) {
              return 0;
            } else if ((equals(themeName, 'Amazonas') || equals(themeName, 'Rio')) && !childrenProductsAux[0].classList.contains('last-page')) {
              return 0;
            } else {
              return 1;
            }
          };

          // The headers are added to the products container
          const headersInsertPosition = getHeadersInsertPosition();
          products.insertBefore(divHeaders, childrenProductsAux[headersInsertPosition]);

          // Function to add all quantities of products to the cart
          $(document).on('click', '#btn-add-to-cart', async () => {
            let idsArr = [],
              amount = 0;
            defaultVariables.productsNotSaved = 0;

            // Function to set disable and innerHTML of btn add to cart
            const setBtnAddToCart = async (disabled, innerHTML) => {
              const btn = document.getElementById('btn-add-to-cart');
              btn.disabled = disabled;
              btn.innerHTML = innerHTML;
            };

            // Disable btn clear cart and save data
            await setBtnAddToCart(true, '<span class="text-center">Cargando...</span>');
            await saveData();

            setDisplayFlexClearCart();

            setTimeout(async () => {
              const addToIdsArr = (itemId, itemQuantity, itemVariant) => {
                let variantString = '(';
                itemVariant.forEach((element, index) => {
                  variantString += element.value;
                  if (index !== itemVariant.length - 1) variantString += ', ';
                  else variantString += ')';
                });

                if (itemId && itemQuantity && itemQuantity.value && itemQuantity.value !== '0') {
                  const productAux = defaultVariables.cartsData.find((product) => (product.itemVariants ? equals(product.itemId, itemId.value) && product.itemVariants.includes(variantString) : equals(product.itemId, itemId.value)));

                  if (productAux) idsArr.push(productAux);
                }
              };

              $('.js-product-form').map((_, element) => {
                const itemId = element.querySelector('input[name=add_to_cart]');
                const itemQuantity = element.querySelector('input[name=quantity]');
                const itemVariant = element.querySelectorAll('#input_variant');
                addToIdsArr(itemId, itemQuantity, itemVariant);
              });

              // Get the amount of products to the cart
              $('input[name=quantity]').each((_, element) => {
                if (element?.value && element.value !== '0') amount++;
              });

              // Reset items of the cart and add the products to the cart
              if (idsArr.length) await resetItemsCart(idsArr);
              await addToCart(amount);

              // Enable btn clear cart
              setBtnAddToCart(false, `${addToCartTxt}`);
            }, 500);
          });

          if (btnChangeView)
            setTimeout(() => {
              btnChangeView.disabled = false;
              btnChangeView.innerHTML = `${switchTxt2}`;
            }, 1000);
        } else if (JSON.parse(localStorage.getItem('isChanged'))) {
          // Change the view to quick buy
          if (document.getElementById('switch-button')) document.getElementById('switch-button').innerHTML = `${switchTxt}`;

          // Function to get the table based on the theme
          function getTableForTheme(theme) {
            if (equals(theme, 'Lifestyle')) return document.querySelector('.js-product-table');
            else if (equals(theme, 'Material')) return defaultVariables.isSearch ? document.querySelector('.js-masonry-grid') : document.querySelector('.product-row');
            else if (equals(theme, 'Trend')) return document.querySelector('.js-masonry-grid');
            else return document.querySelector('.js-product-table');
          }

          // Set the default catalog and remove the headers
          let table = getTableForTheme(themeName);

          // Replace the table of products with the default catalog
          table.parentNode.replaceChild(defaultVariables.defaultCatalog.cloneNode(true), table);

          // Function to remove an element by ID
          function removeElement(elementId) {
            const element = document.querySelector(elementId);
            if (element) element.parentNode.removeChild(element);
          }

          // Remove the headers
          removeElement('#div-headers');

          // Remove the button to add all products to the cart
          const products = defaultVariables.isSearch || equals(themeName, 'Lifestyle') ? document.querySelector('.js-product-table') : document.querySelector('.container-product-grid');
          if (products?.querySelector('#ajax-cart-add-div') && products?.querySelector('#div-headers')) {
            removeElement('#ajax-cart-add-div');
            removeElement('#div-headers');
          }

          // Format the name of the products
          const formatProductName = (element) => {
            if (element.dataset.variants) {
              const priceAux = JSON.parse(element.dataset.variants)[0].price_short?.replace(`${moneyTxt}`, '')?.replaceAll('.', '')?.replace(',', '.'),
                nameAux = themeName !== 'Lifestyle' ? element.querySelector('.js-item-name.item-name') : element.querySelector('.item-name');
              nameAux.innerHTML = formatName(nameAux.innerHTML, priceAux);
            }
          };

          // Format the name of the products
          $('.js-product-container').each((_, element) => formatProductName(element));
          $('.js-item-product').each((_, element) => formatProductName(element));

          await functionToDisableBtnStartBuy();

          // Set the isChanged variable to false
          localStorage.setItem('isChanged', false);
        }
      };

      /**
       * Determines the position based on the theme and the existence of a start buy button.
       * @returns {number} The position value.
       */
      const setPosition = () => {
        if (equals(themeName, 'Material')) {
          return 2;
        } else if (equals(themeName, 'Trend')) {
          return 1;
        } else if (equals(themeName, 'Simple')) {
          return 3;
        }

        return btnStartBuy ? 1 : 0;
      };

      /**
       * Function executed before unloading the page.
       * @param {Event} event - The beforeunload event.
       */
      const beforeUnload = (event) => {
        const productsNotSavedAux = defaultVariables.productsNotSaved;

        // If there are products to add to the cart, show a message
        if (productsNotSavedAux > 0) {
          event.returnValue = `${alertBeforeTxt}`;
        }
      };

      // Set the function before unload
      document.body.onbeforeunload = beforeUnload;
      document.body.addEventListener('beforeunload', beforeUnload);

      // HTML elements are added
      let btnClearCartContainer = null;

      /**
       * Inserts the clear cart button into the specified container at the given position.
       * @param {HTMLElement} container - The container element.
       * @param {number} position - The position to insert the button.
       */
      const insertBtnClearCart = (container, position) => {
        if (divBtnClearCart) container?.insertBefore(divBtnClearCart, container?.children[position]);
      };

      // Add button clear cart depending on the theme
      if (equals(themeName, 'Atlántico')) {
        const container = document.querySelector('.js-visible-on-cart-filled.js-has-new-shipping').children[0];
        insertBtnClearCart(container, 0);
      } else {
        btnClearCartContainer = document.querySelector('#ajax-cart-submit-div')?.parentNode;
        const position = setPosition();
        insertBtnClearCart(btnClearCartContainer, position);
      }

      // Set the display property to 'flex' for the button to clear the cart
      setDisplayFlexClearCart();

      /**
       * Sets the properties of a button.
       * @param {string} buttonId - The ID of the button element.
       * @param {boolean} disabled - Whether the button should be disabled.
       * @param {string} innerHTML - The inner HTML content of the button.
       */
      const setButtonProperties = (buttonId, disabled, innerHTML) => {
        const btn = document.getElementById(buttonId);
        if (btn) {
          btn.disabled = disabled;
          btn.innerHTML = innerHTML;
        }
      };

      /**
       * Function to clean the items in the shopping cart.
       * @function
       * @listens click
       * @param {Event} event - The click event.
       * @returns {Promise<void>} The promise object representing the asynchronous operation.
       * @async
       */
      $(document).on('click', '#clear_cart', async (event) => {
        /**
         * The length of the items array in the cart.
         * @type {number}
         */
        const lengthArr = $('.js-cart-item').length;
        /**
         * The number of products not saved.
         * @type {number}
         */
        defaultVariables.productsNotSaved = 0;

        // Disable btn clear cart
        setButtonProperties('clear_cart', true, `<span class="text-center">${loadingTxt}...</span>`);

        /**
         * Asynchronously clean the cart and update the UI.
         * @async
         */
        setTimeout(async () => {
          if (JSON.parse(localStorage.getItem('viewList'))) await resetQuantity();
          await clearCart(lengthArr);

          // Enable btn clear cart
          setButtonProperties('clear_cart', false, `${clearCartTxt}`);
        }, 500);
      });

      /**
       * Function executed when the user clicks on the button to load more products.
       * @function
       * @listens click
       * @param {Event} event - The click event.
       */
      $(document).on('click', '.js-load-more-btn', (event) => {
        // Set the view of the catalog
        localStorage.setItem('viewList', !JSON.parse(localStorage.getItem('viewList')));

        // Change the view of the catalog
        changeView();
      });

      /**
       * Function that is executed when the DOM is ready.
       * It sets up various functionalities based on the page and user status.
       * @function
       * @listens DOMContentLoaded
       */
      $(document).ready(() => {
        /**
         * Flag indicating if the current page is a search page.
         * @type {boolean}
         */
        defaultVariables.isSearch = false;

        /**
         * The path name of the current URL.
         * @type {string}
         */
        defaultVariables.pathName = window.location.pathname;

        // Set the customer if exists
        let customer = LS?.customer || null;

        if (equals(defaultVariables.pathName, '/') && customer) {
          // If the path name is home
          // Add event to switch view and add the switch to the DOM
          disableBtnStartBuy();
          switchLink.addEventListener('click', () => localStorage.setItem('viewList', true));
          document.querySelector(switchClassHome).appendChild(switchLink);
        } else if (customer || (!customer && (document.querySelector('.js-product-table') || document.querySelector('.product-row') || document.querySelector('.js-masonry-grid')))) {
          // If the customer exists and the DOM has elements with the class js-product-table, product-row, or js-masonry-grid
          $('.js-product-container').each((_, element) => {
            if (element.dataset.variants) {
              // Set the name and price of the product
              const priceAux = JSON.parse(element.dataset.variants)[0].price_short?.replace('$', '')?.replaceAll('.', '')?.replace(',', '.'),
                nameAux = themeName !== 'Lifestyle' ? element.querySelector('.js-item-name.item-name') : element.querySelector('.item-name');
              nameAux.innerHTML = formatName(nameAux.innerHTML, priceAux);

              if (equals(themeName, 'Amazonas') || equals(themeName, 'Atlántico')) {
                // Set the display of the name of the product
                const divName = element.querySelector('.js-item-name');
                divName.setAttribute('style', 'display: block;');
              }
            }
          });

          // Function to change the view of the catalog
          if (equals(defaultVariables.pathName, '/search/')) defaultVariables.isSearch = true;

          // Add event to switch view and add the switch to the DOM
          $(document).on('click', '#switch-button', changeView);
          let containerSwitch = defaultVariables.isSearch ? document.querySelector(switchClassSearch) : document.querySelector(switchClass);
          containerSwitch?.appendChild(switchDiv);

          // Set the view of the catalog
          localStorage.setItem('viewList', !JSON.parse(localStorage.getItem('viewList')));

          // Change the view of the catalog
          changeView();
        }

        // If the customer exists, disable the button start buy
        if (!customer) disableBtnStartBuy();
      });
    } catch (error) {
      console.error(error);
    }
  });
})();
