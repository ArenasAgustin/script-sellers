// Variables to save the txt
let imageTxt, nameTxt, priceTxt, amountTxt, stockTxt, quantityTxt, addToCartTxt, seeVariatsTxt, buyTxt, readyTxt, addTxt

// Set the txt depending of the language
switch (language){
    case 'es':
        imageTxt = 'Imagen';
        nameTxt = 'Nombre';
        priceTxt = 'Precio';
        amountTxt = 'Monto total';
        stockTxt = 'Stock';
        quantityTxt = 'Cantidad';
        addToCartTxt = 'Agregar al carrito';
        seeVariatsTxt ='Ver variantes'
        buyTxt = 'Comprar'
        readyTxt = 'Listo'
        addTxt = 'Agregando'
        break;

    case 'pt':
        imageTxt = 'Imagem';
        nameTxt = 'Nome';
        priceTxt = 'Preço';
        amountTxt = 'Valor total';
        stockTxt = 'Estoque';
        quantityTxt = 'Quantia';
        addToCartTxt = 'Adicionar ao carrinho';
        seeVariatsTxt ='Ver variantes'
        buyTxt = 'Comprar'
        readyTxt = 'Pronto'
        addTxt = 'Adicionando'
        break;

    default:
        imageTxt = 'Image';
        nameTxt = 'Name';
        priceTxt = 'Price';
        amountTxt = 'Total amount';
        stockTxt = 'Stock';
        quantityTxt = 'Quantity';
        addToCartTxt = 'Add to cart';
        seeVariatsTxt ='See variants'
        buyTxt = 'Buy'
        readyTxt = 'Ready'
        addTxt = 'Adding'
        break;
}

// InnerHTML of theme Amazonas
// This is the HTML of the form to add to cart
const stringFormInnerAmazonas = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-primary btn-small w-100 mb-2 cart" value="${buyTxt}" />
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-block btn-transition btn-small w-100 mb-2 disabled" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text">${buyTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success-small"><svg class="icon-inline svg-icon-invert btn-transition-success-icon" viewBox="0 0 512 512"><path d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 48c110.532 0 200 89.451 200 200 0 110.532-89.451 200-200 200-110.532 0-200-89.451-200-200 0-110.532 89.451-200 200-200m140.204 130.267l-22.536-22.718c-4.667-4.705-12.265-4.736-16.97-.068L215.346 303.697l-59.792-60.277c-4.667-4.705-12.265-4.736-16.97-.069l-22.719 22.536c-4.705 4.667-4.736 12.265-.068 16.971l90.781 91.516c4.667 4.705 12.265 4.736 16.97.068l172.589-171.204c4.704-4.668 4.734-12.266.067-16.971z"></path></svg></span>
    <div class="js-addtocart-adding transition-container transition-soft btn-transition-progress">
      <div class="spinner-ellipsis invert">
        <div class="point"></div><div class="point"></div><div class="point"></div><div class="point"></div>
      </div>
    </div>
  </div>
</div>
`;

// This is the HTML of the headers of the table
const stringHeadersAmazonas = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsAmazonas = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image">
    <div style="padding-bottom: 100%;" class="p-relative"></div>
  </div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name text-capitalize"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
    <div class="col-3">
      <div class="form-group float-left form-quantity cart-item-quantity small mb-0">
        <div class="row m-0 align-items-center ">
          <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-substract"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
          <div class="form-control-container col">
            <input class=" form-control js-cart-quantity-input text-center h6 form-control-inline" id="cart-quantity-input"type="number" name="quantity_aux_variants" value="0" min="0"step="1"  >
          </div>
          <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 24.103v8.169a11.995 11.995 0 0 0 9.698 11.768C396.638 63.425 472 150.461 472 256c0 118.663-96.055 216-216 216-118.663 0-216-96.055-216-216 0-104.534 74.546-192.509 174.297-211.978A11.993 11.993 0 0 0 224 32.253v-8.147c0-7.523-6.845-13.193-14.237-11.798C94.472 34.048 7.364 135.575 8.004 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.789 504 256c0-121.187-86.924-222.067-201.824-243.704C294.807 10.908 288 16.604 288 24.103z"></path></svg></span>
          <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-add"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
        </div>
      </div>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputAmazonas = `
<div class="form-group float-left form-quantity cart-item-quantity small mb-0">
  <div class="row m-0 align-items-center ">
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="substract-btn" id="substract-btn"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
    <div class="form-control-container col"><input type="number" class="form-control js-cart-quantity-input text-center h6 form-control-inline" name="quantity_aux" value="0" min="0" step="1" ></div>
    <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 24.103v8.169a11.995 11.995 0 0 0 9.698 11.768C396.638 63.425 472 150.461 472 256c0 118.663-96.055 216-216 216-118.663 0-216-96.055-216-216 0-104.534 74.546-192.509 174.297-211.978A11.993 11.993 0 0 0 224 32.253v-8.147c0-7.523-6.845-13.193-14.237-11.798C94.472 34.048 7.364 135.575 8.004 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.789 504 256c0-121.187-86.924-222.067-201.824-243.704C294.807 10.908 288 16.604 288 24.103z"></path></svg></span>
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline icon-w-12 svg-icon-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
  </div>
</div>
`;

// InnerHTML of theme Atlántico
// This is the HTML of the form to add to cart
const stringFormInnerAtlantico = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />
<div class="js-quickshop-add-container item-submit-container btn btn-primary">
  <input type="submit" class="js-addtocart js-prod-submit-form js-quickshop-icon-add btn btn-primary item-btn-quickshop cart" value="" alt="${addToCartTxt}" style="display: block"/>
  <svg class="js-quickshop-bag" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478 512"><path d="M443.62,170.67H340.48V102.4C340.48,45.85,294.3,0,237.34,0S134.2,45.85,134.2,102.4v68.27H31.06A34.25,34.25,0,0,0-3.32,204.8V477.87A34.25,34.25,0,0,0,31.06,512H443.62A34.25,34.25,0,0,0,478,477.87V204.8A34.25,34.25,0,0,0,443.62,170.67Zm-275-68.27c0-37.7,30.79-68.27,68.76-68.27S306.1,64.7,306.1,102.4v68.27H168.58Zm275,375.47H31.06V204.8H443.62V477.87Z"></path></svg>
</div>
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition disabled item-submit-container item-btn-quickshop" style="display: none">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active" >
      <svg class="icon-inline icon-lg svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478 512" ><path d="M443.62,170.67H340.48V102.4C340.48,45.85,294.3,0,237.34,0S134.2,45.85,134.2,102.4v68.27H31.06A34.25,34.25,0,0,0-3.32,204.8V477.87A34.25,34.25,0,0,0,31.06,512H443.62A34.25,34.25,0,0,0,478,477.87V204.8A34.25,34.25,0,0,0,443.62,170.67Zm-275-68.27c0-37.7,30.79-68.27,68.76-68.27S306.1,64.7,306.1,102.4v68.27H168.58Zm275,375.47H31.06V204.8H443.62V477.87Z"></path></svg>
    </span>
    <span class="js-addtocart-success transition-container btn-transition-success">
      <svg class="icon-inline icon-lg svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M444.96 159l-12.16-11c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L131.77 428 31.42 329c-2.03-2.67-4.72-4-8.11-4s-6.08 1.33-8.11 4L3.04 340C1.01 342.67 0 345.67 0 349s1.01 6 3.04 8l120.62 119c2.69 2.67 5.57 4 8.62 4s5.92-1.33 8.62-4l304.07-300c2.03-2 3.04-4.67 3.04-8s-1.02-6.33-3.05-9zM127.17 284.03c2.65 2.65 5.48 3.97 8.47 3.97s5.82-1.32 8.47-3.97L365.01 63.8c1.99-2 2.99-4.65 2.99-7.96s-1-6.29-2.99-8.94l-11.96-10.93c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97L135.14 236.34l-72.25-72.03c-1.99-2.65-4.64-3.97-7.97-3.97s-5.98 1.32-7.97 3.97l-11.96 10.93C33 177.89 32 180.87 32 184.18s1 5.96 2.99 7.95l92.18 91.9z"></path></svg>
    </span>
    <div class="js-addtocart-adding transition-container btn-transition-progress mt-0 pr-3"><i class="spinner spinner-small col-4 offset-4"></i></div>
  </div>
</div>
`;

// This is the HTML of the headers of the table
const stringHeadersAtlantico = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsAtlantico = `
<div class="col-12 d-flex item-product-card">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image">
    <div style="padding-bottom: 100%;" class="p-relative"></div>
  </div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
    <div class="col-3">
      <div class="float-left form-quantity form-quantity-small mb-0">
        <span class="js-cart-quantity-btn form-quantity-icon btn p-2" style="left: 15px; bottom: -1px;" id="cart-quantity-btn-substract"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H0V245.33H320Z"></path></svg> </span>
        <input class="form-control form-control-quantity form-control-quantity-small form-control-inline" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0" step="1">
        <span class="js-cart-quantity-btn form-quantity-icon form-quantity-icon-up btn p-2" style="right: 15px; bottom: -1px;"  id="cart-quantity-btn-add"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H170.67V416H149.33V266.67H0V245.33H149.33V96h21.34V245.33H320Z"></path></svg> </span>
      </div>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputAtlantico = `
<div class="float-left form-quantity form-quantity-small mb-0">
  <span class="js-cart-quantity-btn form-quantity-icon btn p-2" style="left: 15px; bottom: -1px;" id="substract-btn"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H0V245.33H320Z"></path></svg> </span>
  <input class="form-control form-control-quantity form-control-quantity-small form-control-inline" type="number" name="quantity_aux" value="0" min="0" step="1">
  <span class="js-cart-quantity-btn form-quantity-icon form-quantity-icon-up btn p-2" style="right: 15px; bottom: -1px;" id="add-btn"><svg class="icon-inline icon-w-12 icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M320,266.67H170.67V416H149.33V266.67H0V245.33H149.33V96h21.34V245.33H320Z"></path></svg> </span>
</div>
`;

// InnerHTML of theme Bahia
// This is the HTML of the form to add to cart
const stringFormInnerBahia = `
<input type="hidden" name="add_to_cart" value="122979185">
<div class="js-item-submit-container item-submit-container btn btn-primary btn-small p-0 position-relative">
  <input type="submit" class="js-addtocart js-prod-submit-form js-quickshop-icon-add btn btn-primary btn-small item-btn-quickshop item-btn-quickshop-medium cart" value="" alt="${addToCartTxt}">
  <svg class="svg-inline--fa fa-lg utilities-icon js-quickshop-bag" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.79,5.39l-3-4A1,1,0,0,0,18,1H6a1,1,0,0,0-.8.4l-3,4A1,1,0,0,0,2,6H2V20a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V6h0A1,1,0,0,0,21.79,5.39ZM6.5,3h11L19,5H5ZM7,10a1,1,0,0,1,2,0,3,3,0,0,0,6,0,1,1,0,0,1,2,0A5,5,0,0,1,7,10ZM20,20a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H20Z"></path></svg>
</div>
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition  btn-small item-btn-quickshop item-btn-quickshop-medium" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active"><svg class="svg-inline--fa fa-lg utilities-icon " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.79,5.39l-3-4A1,1,0,0,0,18,1H6a1,1,0,0,0-.8.4l-3,4A1,1,0,0,0,2,6H2V20a3,3,0,0,0,3,3H19a3,3,0,0,02,3-3V6h0A1,1,0,0,0,21.79,5.39ZM6.5,3h11L19,5H5ZM7,10a1,1,0,0,1,2,0,3,3,0,0,0,6,0,1,1,0,0,1,2,0A5,5,0,0,1,7,10ZM20,20a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V7H20Z"></path></svg>			</span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="h5 svg-inline--fa svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress"><svg class="svg-inline--fa fa-spin fa-lg svg-icon-invert" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></div>
  </div>
</div>
<input class="js-quantity-input hidden" type="number" name="quantity" id="input_quantity" value="0">
`;

// This is the HTML of the headers of the table
const stringHeadersBahia = `
<div class="col-12 d-flex item-container">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsBahia = `
<div class="col-12 item-container d-flex ">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
      <div class="col-3">
        <div class="form-group form-quantity small mb-2">
          <div class="row m-0 align-items-center ">
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-substract"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
            <div class="form-control-container col pl-0 pr-0"><input class="form-control text-center form-control-inline" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0" step="1"></div>
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-add"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
          </div>
        </div>
      </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputBahia = `
<div class="form-group form-quantity small mb-2" >
  <div class="row m-0 align-items-center ">
    <span class="js-cart-quantity-btn text-center btn" id="substract-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
    <div class="form-control-container col pl-0 pr-0"><input class="form-control text-center form-control-inline" type="number" name="quantity_aux" value="0" min="0" step="1"  ></div>
    <span class="js-cart-quantity-btn text-center btn" id="add-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
  </div>
</div>
`;

// InnerHTML of theme Cubo
// This is the HTML of the form to add to cart
const stringFormInnerCubo = `
<input type="hidden" name="add_to_cart" value="122979185">                  
<svg class=" icon-inline icon-lg item-cart-absolute svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M551.991 64H129.28l-8.329-44.423C118.822 8.226 108.911 0 97.362 0H12C5.373 0 0 5.373 0 12v8c0 6.627 5.373 12 12 12h78.72l69.927 372.946C150.305 416.314 144 431.42 144 448c0 35.346 28.654 64 64 64s64-28.654 64-64a63.681 63.681 0 0 0-8.583-32h145.167a63.681 63.681 0 0 0-8.583 32c0 35.346 28.654 64 64 64 35.346 0 64-28.654 64-64 0-17.993-7.435-34.24-19.388-45.868C506.022 391.891 496.76 384 485.328 384H189.28l-12-64h331.381c11.368 0 21.177-7.976 23.496-19.105l43.331-208C578.592 77.991 567.215 64 551.991 64zM240 448c0 17.645-14.355 32-32 32s-32-14.355-32-32 14.355-32 32-32 32 14.355 32 32zm224 32c-17.645 0-32-14.355-32-32s14.355-32 32-32 32 14.355 32 32-14.355 32-32 32zm38.156-192H171.28l-36-192h406.876l-40 192z"></path></svg>
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-small py-3 pl-4 w-100 text-secondary cart" value="${buyTxt}">
<div class="js-addtocart js-addtocart-placeholder btn btn-transition disabled btn-small btn-transition-item py-3 pl-4 w-100 text-secondary" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active">${buyTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success smallest">${readyTxt}</span>
    <div class="js-addtocart-adding transition-container btn-transition-progress smallest">${addTxt}...</div>
  </div>
</div>
<input class="js-quantity-input hidden" type="number" name="quantity" id="input_quantity" value="0">
`;

// This is the HTML of the headers of the table
const stringHeadersCubo = `
<div class="col-12 d-flex item-container">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsCubo = `
<div class="item-container d-flex w-100">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name text-center"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
    <div class="col-3">
      <div class="d-flex justify-content-center m-0 align-items-center my-md-auto">
        <button class="js-cart-quantity-btn cart-item-btn btn" id="cart-quantity-btn-substract"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></button>
        <div class="form-control-container col"><input class="js-cart-quantity-input-item cart-item-input form-control" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0"step="1"  ></div>
        <button class="js-cart-quantity-btn cart-item-btn btn" id="cart-quantity-btn-add"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></button>
      </div>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputCubo = `
<div class="d-flex justify-content-center m-0 align-items-center my-md-auto">
  <span class="js-cart-quantity-btn cart-item-btn btn" id="substract-btn" id="substract-btn"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h352c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
  <div class="form-control-container col"><input type="number" class="js-cart-quantity-input-item cart-item-input form-control" name="quantity_aux" value="0" min="0" step="1" ></div>
  <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline svg-icon-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M368 224H224V80c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v144H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h144v144c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V288h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg></span>
</div>
`;

// InnerHTML of theme Idea
// This is the HTML of the form to add to cart
const stringFormInnerIdea = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input  type="submit"  class="js-addtocart js-prod-submit-form btn btn-primary btn-small col-10 mb-2 cart"  value="${addToCartTxt}"/>
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-small btn-transition mb-2 col-10 disabled" style="display: none;">
    <div class="d-inline-block">
        <span class="js-addtocart-text">${addToCartTxt}</span>
        <span class="js-addtocart-success transition-container btn-transition-success-small">¡${readyTxt}!</span>
        <div class="js-addtocart-adding transition-container transition-soft btn-transition-progress-small"><div class="spinner-ellipsis-invert"></div></div>
    </div>
</div>
<input class="js-quantity-input hidden" type="number" name="quantity" id="input_quantity" value="0">
`;

// This is the HTML of the headers of the table
const stringHeadersIdea = `
<div class="col-12 d-flex item-container">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsIdea = `
<div class="col-12 item-container d-flex ">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
      <div class="col-3">
        <div class="form-group form-quantity small mb-2" data-component="product.quantity">
          <div class="row m-0 align-items-center " data-component="line-item.quantity">
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-substract"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
            <div class="form-control-container col pl-0 pr-0"><input  class="form-control text-center form-control-inline"  id="cart-quantity-input" type="number"  name="quantity_aux_variants"   value="0"  min="0"  step="1"  ></div>
            <span class="js-cart-quantity-btn text-center btn" id="cart-quantity-btn-add"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
          </div>
        </div>
      </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputIdea = `
<div class="form-group form-quantity small mb-2" data-component="product.quantity">
  <div class="row m-0 align-items-center " data-component="line-item.quantity">
    <span class="js-cart-quantity-btn text-center btn" id="substract-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h368c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                   </span>
    <div class="form-control-container col pl-0 pr-0"><input  class="form-control text-center form-control-inline"  type="number"  name="quantity_aux"  value="0"  min="0"  step="1"  ></div>
    <span class="js-cart-quantity-btn text-center btn" id="add-btn"><svg class="svg-inline--fa fa-w-12 fa-lg svg-icon-text " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"></path></svg>                  </span>
  </div>
</div>
`;

// InnerHTML of theme Lifestyle
// This is the HTML of the form to add to cart
const stringFormInnerLifestyle = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" style="display:none !important;"/>
<div class="js-product-cta-container product-buy-container full-width">
  <input  type="submit"  class="js-prod-submit-form js-addtocart js-cart-direct-add btn btn-primary btn-inverse btn-smallest item-buy-btn full-width cart m-bottom-half"  value="${buyTxt}">
  <div class="js-addtocart js-addtocart-placeholder btn btn-primary full-width btn-transition m-bottom-half disabled btn-inverse btn-smallest item-buy-btn" style="display: none;">
    <div class="d-inline-block">
      <span class="js-addtocart-text">${buyTxt}</span>
      <span class="js-addtocart-success transition-container btn-transition-success">
        <span class="m-left p-left">¡${readyTxt}!</span>
        <span class="pull-right m-right"><svg class="svg-inline--fa fa-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z"></path></svg></span>
      </span>
      <div class="js-addtocart-adding transition-container btn-transition-progress">
        <span class="m-left ">${addTxt}</span>
        <span class="pull-right m-right"><svg class="svg-inline--fa fa-lg fa-spin m-left-half pull-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path></svg></span>
      </div>
    </div>
  </div>            
</div>
`;

// This is the HTML of the headers of the table
const stringHeadersLifestyle = `
<div class="d-flex col-auto full-width item-container" style="font-weight: bold;">
  <div class="span2 mb-3 mt-3 d-flex justify-content-center align-items-center" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${imageTxt}</span></div>
  <div class="span4" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${nameTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${priceTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${amountTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${stockTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${quantityTxt}</span></div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsLifestyle = `
<div class="col-auto full-width item-container d-flex" style="text-aling: center;">
  <div class="js-item-image-container item-image-container span2 overflow-hidden" style="height: 100px;" id="item-image"><div class="js-item-image-padding p-relative" style="padding-bottom: 100%; padding-left: 20%;"></div></div>
  <div class="span10 align-items-center" style="display: flex !important; margin: 0 !important;">
    <div class="js-item-name span3 item-name" style="margin: 0 !important;"></div>
    <div class="span2 px-2" style="margin: 0 !important;"></div>
    <div class="span4 px-2" style="margin: 0 !important;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
    <div class="span2 px-2" style="margin: 0 !important;"></div>
    <div class="span3 js-ajax-cart-qty-container pull-left m-top-half m-none-xs" style="margin: 0 !important;">
      <div class="cart-quantity-input-container d-inline-block pull-left">
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path></svg></span>
        <input  class="js-cart-quantity-input cart-quantity-input cart-quantity-input-small form-control form-control-secondary" id="cart-quantity-input" type="number"  name="quantity_aux_variants"  value="0"  min="0"  step="1" style="width: 100px !important;">
      </div>
      <div class="d-inline-block m-left-quarter m-none-xs">
        <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small" id="cart-quantity-btn-add"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
        <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small hidden-phone" id="cart-quantity-btn-substract"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
      </div>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputLifestyle = `
<div class="cart-quantity-input-container d-inline-block pull-left">
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path></svg></span>
  <input  class="js-cart-quantity-input cart-quantity-input cart-quantity-input-small form-control form-control-secondary" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 100px !important;">
</div>
<div class="d-inline-block m-left-quarter m-none-xs">
  <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small" id="add-btn"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
  <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-small hidden-phone" id="substract-btn"><div class="cart-quantity-svg-icon svg-text-fill"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
</div>
`;

// InnerHTML of theme Material
// This is the HTML of the form to add to cart
const stringFormInnerMaterial = `
<input type="hidden" name="add_to_cart" value="122979185">
<input  type="submit"  class="js-addtocart js-prod-submit-form btn btn-primary full-width font-small-xs cart"  value="${addToCartTxt}"  style="display: block;">
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition btn-block m-none disabled font-small-xs" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active">${addToCartTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="svg-check-circle-icon fa-lg svg-icon-invert  m-left-quarter" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress">
      <span class="m-left">${addTxt}...</span>
      <span class="pull-right m-right">
        <div class="spinner inverse">
          <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
            <div class="spinner-layer d-inline-block full-height full-width p-absolute">
              <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
            </div>
          </div>
        </div>
      </span>
    </div>
  </div>
</div>
<input  class="js-quantity-input hidden"  type="number"  name="quantity"  id="input_quantity"  value="0" style="display: none;">
`;

// This is the HTML of the headers of the table
const stringHeadersMaterial = `
<div class="d-flex item-container" style="flex: 0 0 100%; max-width: 100%;">
  <div class="d-flex" style="flex: 0 0 16.666667%; max-width: 16.666667%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;"><span class="item-name">${imageTxt}</span></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div style="flex: 0 0 25%; max-width: 25%;"><span class="item-name">${nameTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${priceTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${amountTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${stockTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsMaterial = `
<div class="item item-container d-flex" style="flex: 0 0 100%; max-width: 100%;">
  <div style="height: 100px; width: auto; position: relative; flex: 0 0 16.666667%; max-width: 16.666667%; overflow: hidden;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div class="d-flex"  style="width: 100%; align-items: center; justify-content: center;">
      <div class="js-item-name item-name" style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
      <div style="flex: 0 0 50%; max-width: 50%;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
    </div>
    <div style="flex: 0 0 25%; max-width: 25%;">
      <button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="cart-quantity-btn-substract"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
      <div class="cart-quantity-input-container d-inline-block pull-left">
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;">
          <div class="spinner">
            <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
              <div class="spinner-layer d-inline-block full-height full-width p-absolute">
                <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
                <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
                <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
              </div>
            </div>
          </div>
        </span>
        <input  class="cart-quantity-input"  id="cart-quantity-input" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 130px !important;">
      </div>
      <button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="cart-quantity-btn-add"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputMaterial = `
<button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="substract-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
<div class="cart-quantity-input-container d-inline-block pull-left">
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;">
    <div class="spinner">
      <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
        <div class="spinner-layer d-inline-block full-height full-width p-absolute">
          <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
          <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
          <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
        </div>
      </div>
    </div>
  </span>
  <input  class="cart-quantity-input" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 130px !important;">
</div>
<button class="js-cart-quantity-btn cart-quantity-btn btn btn-default" id="add-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
`;

// InnerHTML of theme Silent
// This is the HTML of the form to add to cart
const stringFormInnerSilent = `
<input type="hidden" name="add_to_cart" value="122979185">
<div class="item-submit-container btn btn-primary">
  <input type="submit" class="js-addtocart js-prod-submit-form js-quickshop-icon-add btn btn-primary item-btn-quickshop cart" value="" alt="${addToCartTxt}"/>
  <svg class="js-quickshop-cart-icon svg-inline--fa svg-icon-back" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M504.717 320H211.572l6.545 32h268.418c15.401 0 26.816 14.301 23.403 29.319l-5.517 24.276C523.112 414.668 536 433.828 536 456c0 31.202-25.519 56.444-56.824 55.994-29.823-.429-54.35-24.631-55.155-54.447-.44-16.287 6.085-31.049 16.803-41.548H231.176C241.553 426.165 248 440.326 248 456c0 31.813-26.528 57.431-58.67 55.938-28.54-1.325-51.751-24.385-53.251-52.917-1.158-22.034 10.436-41.455 28.051-51.586L93.883 64H24C10.745 64 0 53.255 0 40V24C0 10.745 10.745 0 24 0h102.529c11.401 0 21.228 8.021 23.513 19.19L159.208 64H551.99c15.401 0 26.816 14.301 23.403 29.319l-47.273 208C525.637 312.246 515.923 320 504.717 320zM408 168h-48v-40c0-8.837-7.163-16-16-16h-16c-8.837 0-16 7.163-16 16v40h-48c-8.837 0-16 7.163-16 16v16c0 8.837 7.163 16 16 16h48v40c0 8.837 7.163 16 16 16h16c8.837 0 16-7.163 16-16v-40h48c8.837 0 16-7.163 16-16v-16c0-8.837-7.163-16-16-16z"></path></svg>
</div>
<div class="js-addtocart js-addtocart-placeholder product-buy-btn btn btn-primary btn-transition item-btn-quickshop item-btn-quickshop-icon" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active"><svg class="svg-inline--fa" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M504.717 320H211.572l6.545 32h268.418c15.401 0 26.816 14.301 23.403 29.319l-5.517 24.276C523.112 414.668 536 433.828 536 456c0 31.202-25.519 56.444-56.824 55.994-29.823-.429-54.35-24.631-55.155-54.447-.44-16.287 6.085-31.049 16.803-41.548H231.176C241.553 426.165 248 440.326 248 456c0 31.813-26.528 57.431-58.67 55.938-28.54-1.325-51.751-24.385-53.251-52.917-1.158-22.034 10.436-41.455 28.051-51.586L93.883 64H24C10.745 64 0 53.255 0 40V24C0 10.745 10.745 0 24 0h102.529c11.401 0 21.228 8.021 23.513 19.19L159.208 64H551.99c15.401 0 26.816 14.301 23.403 29.319l-47.273 208C525.637 312.246 515.923 320 504.717 320zM408 168h-48v-40c0-8.837-7.163-16-16-16h-16c-8.837 0-16 7.163-16 16v40h-48c-8.837 0-16 7.163-16 16v16c0 8.837 7.163 16 16 16h48v40c0 8.837 7.163 16 16 16h16c8.837 0 16-7.163 16-16v-40h48c8.837 0 16-7.163 16-16v-16c0-8.837-7.163-16-16-16z"></path></svg></span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="svg-inline--fa" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress"><svg class="svg-inline--fa fa-spin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 39.056v16.659c0 10.804 7.281 20.159 17.686 23.066C383.204 100.434 440 171.518 440 256c0 101.689-82.295 184-184 184-101.689 0-184-82.295-184-184 0-84.47 56.786-155.564 134.312-177.219C216.719 75.874 224 66.517 224 55.712V39.064c0-15.709-14.834-27.153-30.046-23.234C86.603 43.482 7.394 141.206 8.003 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-115.633-79.14-212.779-186.211-240.236C302.678 11.889 288 23.456 288 39.056z"></path></svg></div>
  </div>
</div>
<input class="js-quantity-input" type="number" name="quantity" id="input_quantity" value="0"style="display: none;"/>
`;

// This is the HTML of the headers of the table
const stringHeadersSilent = `
<div class="d-flex item-container" style="flex: 0 0 100%; max-width: 100%;">
    <div class="d-flex" style="flex: 0 0 16.666667%; max-width: 16.666667%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;"><span class="item-name">${imageTxt}</span></div>
    <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div style="flex: 0 0 25%; max-width: 25%;"><span class="item-name">${nameTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${priceTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${amountTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${stockTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name">${quantityTxt}</span></div>
    </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsSilent = `
<div class="item-container d-flex" style="flex: 0 0 100%; max-width: 100%; text-transform: uppercase;">
  <div style="height: 100px; width: auto; position: relative; flex: 0 0 16.666667%; max-width: 16.666667%; overflow: hidden;" id="item-image">
    <div style="padding-bottom: 100%; position: relative;"></div>
  </div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div class="d-flex"  style="width: 100%; align-items: center; justify-content: center;">
      <div class="js-item-name item-name" style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
      <div style="flex: 0 0 50%; max-width: 50%;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
    </div>
      <div style="flex: 0 0 25%; max-width: 25%;">
        <button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="cart-quantity-btn-substract">
          <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>         
        </button>
        <div class="cart-quantity-input-container d-inline-block pull-left">
          <input class="js-cart-quantity-input cart-quantity-input" id="cart-quantity-input"type="number" name="quantity_aux_variants"  value="0" min="0" step="1"style="width: 160px !important;"/>
        </div>
        <button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="cart-quantity-btn-add">
          <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>             
        </button>
      </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputSilent = `
<button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="substract-btn">
  <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>                  
</button>
<div class="cart-quantity-input-container d-inline-block pull-left">
  <input class="js-cart-quantity-input cart-quantity-input" type="number" name="quantity_aux" value="0" min="0" step="1"style="width: 160px !important;">
</div>
<button class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn" id="add-btn">
  <div class="cart-quantity-svg-icon svg-icon-text"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>     
</button>
`;

// InnerHTML of theme Simple
// This is the HTML of the form to add to cart
const stringFormInnerSimple = `
<input type="hidden" name="add_to_cart" value="100373312" id="input_id"/>
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" id="input_quantity" style="display:none !important;"/>
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-primary full-width-xs btn-small btn-smallest font-small-extra p-right p-left p-right-half-xs p-left-half-xs cart m-auto" value="${addToCartTxt}"/>
<div class="js-addtocart js-addtocart-placeholder product-buy-btn btn btn-primary btn-transition js-addtocart-placeholder-inline full-width-xs btn-small btn-smallest font-small-extra p-right p-left p-right-half-xs p-left-half-xs m-auto" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text btn-transition-start transition-container active">${addToCartTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success">
      ¡${readyTxt}!<svg class="svg-inline--fa svg-icon-back m-left-quarter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M444.09 166.99l-27.39-28.37c-2.6-1.96-5.53-2.93-8.8-2.93-3.27 0-5.87.98-7.82 2.93L142.81 396.86l-94.88-94.88c-1.96-2.61-4.55-3.91-7.82-3.91-3.27 0-6.21 1.3-8.8 3.91l-27.4 27.38c-2.6 2.61-3.91 5.55-3.91 8.8s1.31 5.87 3.91 7.82l130.1 131.07c2.6 1.96 5.53 2.94 8.8 2.94 3.27 0 5.87-.98 7.82-2.94L444.08 183.6c2.6-2.61 3.91-5.55 3.91-8.8.01-3.24-1.3-5.86-3.9-7.81zM131.88 285.04c2.62 1.97 5.58 2.96 8.88 2.96s5.92-.99 7.89-2.96L353.34 80.35c2.62-2.64 3.95-5.6 3.95-8.88 0-3.28-1.33-5.92-3.95-7.89l-27.63-28.62c-2.62-1.97-5.58-2.96-8.88-2.96s-5.92.99-7.89 2.96L140.76 204.12l-60.41-60.41c-1.97-2.64-4.59-3.95-7.89-3.95s-6.26 1.31-8.88 3.95l-27.63 27.63c-2.62 2.64-3.95 5.6-3.95 8.88 0 3.29 1.33 5.92 3.95 7.89l95.93 96.93z"></path></svg>
    </span>
    <div class="js-addtocart-adding transition-container btn-transition-progress">
      ${addTxt}...<svg class="svg-inline--fa svg-icon-back  m-left-quarter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M551.991 64H144.28l-8.726-44.608C133.35 8.128 123.478 0 112 0H12C5.373 0 0 5.373 0 12v24c0 6.627 5.373 12 12 12h80.24l69.594 355.701C150.796 415.201 144 430.802 144 448c0 35.346 28.654 64 64 64s64-28.654 64-64a63.681 63.681 0 0 0-8.583-32h145.167a63.681 63.681 0 0 0-8.583 32c0 35.346 28.654 64 64 64s64-28.654 64-64c0-18.136-7.556-34.496-19.676-46.142l1.035-4.757c3.254-14.96-8.142-29.101-23.452-29.101H203.76l-9.39-48h312.405c11.29 0 21.054-7.869 23.452-18.902l45.216-208C578.695 78.139 567.299 64 551.991 64zM464 424c13.234 0 24 10.766 24 24s-10.766 24-24 24-24-10.766-24-24 10.766-24 24-24zm-256 0c13.234 0 24 10.766 24 24s-10.766 24-24 24-24-10.766-24-24 10.766-24 24-24zm279.438-152H184.98l-31.31-160h368.548l-34.78 160zM272 200v-16c0-6.627 5.373-12 12-12h32v-32c0-6.627 5.373-12 12-12h16c6.627 0 12 5.373 12 12v32h32c6.627 0 12 5.373 12 12v16c0 6.627-5.373 12-12 12h-32v32c0 6.627-5.373 12-12 12h-16c-6.627 0-12-5.373-12-12v-32h-32c-6.627 0-12-5.373-12-12z"></path></svg>
    </div>
  </div>
</div>
`;

// This is the HTML of the headers of the table
const stringHeadersSimple = `
<div class="d-flex col-auto full-width item-container" style="font-weight: bold;">
  <div class="span2 mb-3 mt-3 d-flex justify-content-center align-items-center" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${imageTxt}</span></div>
  <div class="span4" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${nameTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${priceTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${amountTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${stockTxt}</span></div>
  <div class="span3" style="margin: 0 !important;"><span class="item-name" style="font-size: 14px;">${quantityTxt}</span></div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsSimple = `
<div class="col-auto full-width item-container d-flex ">
  <div class="js-item-image-container item-image-container span2 overflow-hidden" style="height: 100px;" id="item-image">
    <div class="js-item-image-padding p-relative" style="padding-bottom: 100%; padding-left: 20%;"></div>
  </div>
  <div class="span10 align-items-center" style="display: flex !important; margin: 0 !important;">
    <div class="js-item-name span3 item-name" style="margin: 0 !important;"></div>
    <div class="span1 px-2" style="margin: 0 !important;"></div>
    <div class="span4 px-2" style="margin: 0 !important;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
    <div class="span2 px-2" style="margin: 0 !important;"></div>
    <div class="span3" style="margin: 0 !important;">
      <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="cart-quantity-btn-substract">
        <div class="cart-quantity-svg-icon svg-icon-text" data-component="quantity.minus"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
      </span>
      <div class="cart-quantity-input-container d-inline-block pull-left">
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 5<path d="M483.515 28.485L431.35 80.65C386.475 35.767 324.485 8 256 8 123.228 8 14.824 112.338 8.31 243.493 7.971 250.311 13.475 256 20.301 256h28.045c6.353 0 11.613-4.952 11.973-11.294C66.161 141.649 151.453 60 256 60c54.163 0 103.157 21.923 138.614 57.386l-54.128 54.129c-7.56 7.56-2.206 20.485 8.485 20.485H492c6.627 0 12-5.373 12-12V36.971c0-10.691-12.926-16.045-20.485-8.486zM491.699 256h-28.045c-6.353 0-11.613 4.952-11.973 11.294C445.839 370.351 360.547 452 256 452c-54.163 0-103.157-21.923-138.614-57.386l54.128-54.129c7.56-7.56 2.206-20.485-8.485-20.485H20c-6.627 0-12 5.373-12 12v143.029c0 10.691 12.926 16.045 20.485 8.485L80.65 431.35C125.525 476.233 187.516 504 256 504c132.773 0 241.176-104.338 247.69-235.493.339-6.818-5.165-12.507-11.991-12.507z"></p</svg></span>
        <input class="cart-quantity-input"id="cart-quantity-input"type="number" name="quantity_aux_variants" value="0" min="0" step="1" style="width: 90px; height: 18px; border-top: 1px solid rgba(67, 67, 67, 0.8); border-bottom: 1px solid rgba(67, 67, 67, 0.8);"/>
      </div>
      <span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="cart-quantity-btn-add">
        <div class="cart-quantity-svg-icon svg-icon-text"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
      </span>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputSimple = `
<span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="substract-btn">
  <div class="cart-quantity-svg-icon svg-icon-text" data-component="quantity.minus"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
</span>
<div class="cart-quantity-input-container d-inline-block pull-left">
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M483.515 28.485L431.35 80.65C386.475 35.767 324.485 8 256 8 123.228 8 14.824 112.338 8.31 243.493 7.971 250.311 13.475 256 20.301 256h28.045c6.353 0 11.613-4.952 11.973-11.294C66.161 141.649 151.453 60 256 60c54.163 0 103.157 21.923 138.614 57.386l-54.128 54.129c-7.56 7.56-2.206 20.485 8.485 20.485H492c6.627 0 12-5.373 12-12V36.971c0-10.691-12.926-16.045-20.485-8.486zM491.699 256h-28.045c-6.353 0-11.613 4.952-11.973 11.294C445.839 370.351 360.547 452 256 452c-54.163 0-103.157-21.923-138.614-57.386l54.128-54.129c7.56-7.56 2.206-20.485-8.485-20.485H20c-6.627 0-12 5.373-12 12v143.029c0 10.691 12.926 16.045 20.485 8.485L80.65 431.35C125.525 476.233 187.516 504 256 504c132.773 0 241.176-104.338 247.69-235.493.339-6.818-5.165-12.507-11.991-12.507z"></path></svg></span>
  <input class="cart-quantity-input"type="number" name="quantity_aux" value="0" min="0" step="1" style="width: 70px; height: 18px; border-top: 1px solid rgba(67, 67, 67, 0.8); border-bottom: 1px solid rgba(67, 67, 67, 0.8);"/>
</div>
<span class="js-cart-quantity-btn cart-quantity-btn ajax-cart-quantity-btn not-cart-quantity-btn" id="add-btn">
  <div class="cart-quantity-svg-icon svg-icon-text"><svg class="" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div>
</span>
`;

// InnerHTML of theme Trend
// This is the HTML of the form to add to cart
const stringFormInnerTrend = `
<input type="hidden" name="add_to_cart" value="122979185">
<input  type="submit"  class="js-addtocart js-prod-submit-form btn btn-primary full-width font-small-xs cart"  value="${addToCartTxt}"  style="display: block;">
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-transition btn-block m-none disabled font-small-xs" style="display: none;">
  <div class="d-inline-block"><span class="js-addtocart-text btn-transition-start transition-container active">${addToCartTxt}</span>
    <span class="js-addtocart-success transition-container btn-transition-success"><svg class="svg-check-circle-icon fa-lg svg-icon-invert  m-left-quarter" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"></path></svg></span>
    <div class="js-addtocart-adding transition-container btn-transition-progress"><span class="m-left">${addTxt}...</span>
      <span class="pull-right m-right">
        <div class="spinner inverse">
          <div class="spinner-circle-wrapper active d-inline-block p-relative full-height full-width">
            <div class="spinner-layer d-inline-block full-height full-width p-absolute">
              <div class="spinner-circle-clipper left d-inline-block p-relative"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-gap-patch d-inline-block full-height"><div class="spinner-circle d-inline-block p-relative"></div></div>
              <div class="spinner-circle-clipper right d-inline-block p-relative full-height"><div class="spinner-circle d-inline-block p-relative full-height"></div></div>
            </div>
          </div>
        </div>
      </span>
    </div>
  </div>
</div>
<input  class="js-quantity-input hidden"  type="number"  name="quantity"  id="input_quantity"  value="0" style="display: none;">
`;

// This is the HTML of the headers of the table
const stringHeadersTrend = `
<div class="d-flex item-container" style="flex: 0 0 100%; max-width: 100%;">
  <div class="d-flex" style="flex: 0 0 16.666667%; max-width: 16.666667%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;"><span class="item-name h2">${imageTxt}</span></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div style="flex: 0 0 25%; max-width: 25%;"><span class="item-name h2">${nameTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${priceTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${amountTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${stockTxt}</span></div>
    <div style="flex: 0 0 16.666667%; max-width: 16.666667%;"><span class="item-name h2">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsTrend = `
<div class="item item-container d-flex" style="flex: 0 0 100%; max-width: 100%;">
  <div style="height: 100px; width: auto; position: relative; flex: 0 0 16.666667%; max-width: 16.666667%; overflow: hidden;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="d-flex" style="flex: 0 0 83.333333%; max-width: 83.333333%; margin-top: 1rem; margin-bottom: 1rem; align-items: center; justify-content: center;">
    <div class="d-flex"  style="width: 100%; align-items: center; justify-content: center;">
      <div class="js-item-name item-name h2" style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
      <div style="flex: 0 0 50%; max-width: 50%;"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div style="flex: 0 0 auto; max-width: 8.33333333%;"></div>
    </div>
    <div style="flex: 0 0 25%; max-width: 25%;">
      <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-left small" id="cart-quantity-btn-substract"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
      <div class="cart-quantity-input-container d-inline-block">        
        <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 39.056v16.659c0 10.804 7.281 20.159 17.686 23.066C383.204 100.434 440 171.518 440 256c0 101.689-82.295 184-184 184-101.689 0-184-82.295-184-184 0-84.47 56.786-155.564 134.312-177.219C216.719 75.874 224 66.517 224 55.712V39.064c0-15.709-14.834-27.153-30.046-23.234C86.603 43.482 7.394 141.206 8.003 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-115.633-79.14-212.779-186.211-240.236C302.678 11.889 288 23.456 288 39.056z"></path></svg></span>
        <input class="js-cart-quantity-input cart-quantity-input small" id="cart-quantity-input" type="number" name="quantity_aux_variants" value="0" min="0" step="1" style="width: 100px !important;">
      </div>
      <button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-right small" id="cart-quantity-btn-add"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
    </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputTrend = `
<button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-left small" id="substract-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h1216c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
<div class="cart-quantity-input-container d-inline-block">        
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="svg-inline--fa fa-spin svg-text-fill" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 39.056v16.659c0 10.804 7.281 20.159 17.686 23.066C383.204 100.434 440 171.518 440 256c0 101.689-82.295 184-184 184-101.689 0-184-82.295-184-184 0-84.47 56.786-155.564 134.312-177.219C216.719 75.874 224 66.517 224 55.712V39.064c0-15.709-14.834-27.153-30.046-23.234C86.603 43.482 7.394 141.206 8.003 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-115.633-79.14-212.779-186.211-240.236C302.678 11.889 288 23.456 288 39.056z"></path></svg></span>
  <input  class="js-cart-quantity-input cart-quantity-input small" type="number"  name="quantity_aux"  value="0"  min="0"  step="1" style="width: 100px !important;">
</div>
<button class="js-cart-quantity-btn cart-quantity-btn cart-quantity-btn-right small" id="add-btn"><div class="cart-quantity-svg-icon"><svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="598 -476.1 1792 1792"><path d="M2198 259.9v192c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-416v416c0 26.7-9.3 49.3-28 68s-41.3 28-68 28h-192c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-416H886c-26.7 0-49.3-9.3-68-28s-28-41.3-28-68v-192c0-26.7 9.3-49.3 28-68s41.3-28 68-28h416v-416c0-26.7 9.3-49.3 28-68 18.7-18.7 41.3-28 68-28h192c26.7 0 49.3 9.3 68 28 18.7 18.7 28 41.3 28 68v416h416c26.7 0 49.3 9.3 68 28s28 41.3 28 68z"></path></svg></div></button>
`;

// InnerHTML of theme Rio
// This is the HTML of the form to add to cart
const stringFormInnerRio = `
<input type="hidden" name="add_to_cart" value="119938588">
<input type="number" name="quantity" value="0" class="js-quantity-input hidden" aria-label="Cambiar cantidad" id="input_quantity" />                                              
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-primary btn-small w-100 mb-2 cart" value="${buyTxt}"  style="display: inline-block;">
<div class="js-addtocart js-addtocart-placeholder btn btn-primary btn-block btn-transition btn-small mb-2 disabled" style="display: none;">
    <div class="d-inline-block">
        <span class="js-addtocart-text" data-prev-visibility="inline" style="display: inline;">${buyTxt}</span>
        <span class="js-addtocart-success transition-container">¡${readyTxt}!</span>
        <div class="js-addtocart-adding transition-container">${addTxt}...</div>
    </div>
</div>
`;

// This is the HTML of the headers of the table
const stringHeadersRio = `
<div class="col-12 d-flex item-container text-center">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsRio = `
<div class="col-12 item-container d-flex ">
  <div class="col-2 overflow-hidden" style="height: 100px; width: auto; position: relative;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="js-item-name col-4 item-name text-center"></div>
      <div class="col-2"></div>
      <div class="col-6"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
      <div class="col-2"></div>
    </div>
      <div class="col-3 d-flex align-items-center justify-content-center">
        <div class="form-group float-left form-quantity cart-item-quantity small mb-0">
          <div class="row m-0 align-items-center ">
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-substract"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
            <div class="form-control-container js-cart-quantity-container col px-1">
              <input type="number" class=" form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux_variants" value="0" min="0" step="1" id="cart-quantity-input" style="visibility: visible; pointer-events: all;">
            </div>
            <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-add"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
          </div>
        </div>
      </div>
  </div>
</div>
`;

// This is the HTML of the quantity input
const stringQuantityInputRio = `
<div class="form-group float-left form-quantity cart-item-quantity small mb-0">
  <div class="row m-0 align-items-center ">
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="substract-btn"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
    <div class="form-control-container js-cart-quantity-container col px-1">
      <input type="number" class=" form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux" value="0" min="0" step="1" style="visibility: visible; pointer-events: all;">
    </div>
    <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
    <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline icon-lg svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
  </div>
</div>
`;

// InnerHTML of theme Lima
// This is the HTML of the form to add to cart
const stringFormInnerLima = `
<input type="hidden" name="add_to_cart" value="119938588" id="input_id">                                                  
<input type="submit" class="js-addtocart js-prod-submit-form btn btn-secondary btn-medium w-100 py-2 cart" value="Comprar">
<div class="js-addtocart js-addtocart-placeholder btn btn-secondary btn-block btn-transition btn-medium mb-1 py-2 disabled" style="display: none;">
  <div class="d-inline-block">
    <span class="js-addtocart-text">Comprar</span>
    <span class="js-addtocart-success transition-container">¡Listo!</span>
    <div class="js-addtocart-adding transition-container transition-icon">
      <svg class="icon-inline btn-icon icon-spin font-body" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg>
    </div>
  </div>
</div>
`;

// This is the HTML of the headers of the table
const stringHeadersLima = `
<div class="col-12 d-flex item-container text-center">
  <div class="col-2 mb-3 mt-3 d-flex justify-content-center align-items-center"><span class="item-name">${imageTxt}</span></div>
  <div class="col-10 d-flex mb-3 mt-3 d-flex justify-content-center align-items-center">
    <div class="d-flex justify-content-around align-items-center w-100">
      <div class="col-4"><span class="item-name">${nameTxt}</span></div>
      <div class="col-3"><span class="item-name">${priceTxt}</span></div>
      <div class="col-3"><span class="item-name">${amountTxt}</span></div>
      <div class="col-3"><span class="item-name">${stockTxt}</span></div>
    </div>
    <div class="col-3"><span class="item-name">${quantityTxt}</span></div>
  </div>
</div>
`;

// This is the HTML of the variants of the products
const stringVariantsLima = `
<div class="js-item-product col-12 item-product col-grid">
  <div classs="item col-12">
    <div class="js-quickshop-container js-quickshop-has-variants position-relative row">
      <div class="item-image col-2" style="height: 100px; width: auto;" id="item-image"><div style="padding-bottom: 100%;" class="p-relative"></div></div>
      <div class="col-10 row justify-content-around align-items-center text-center">
        <div class="d-flex justify-content-around align-items-center w-100">
          <div class="js-item-name col-3 item-name text-center"></div>
          <div class="col-1"></div>
          <div class="col-5"><button class="btn btn-primary btn-block" id="see-variants">${seeVariatsTxt}</button></div>
          <div class="col-1"></div>
          <div class="col-3 form-group form-quantity cart-item-quantity small m-auto mb-0">
            <div class="row m-0 align-items-center ">
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-substract"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
            <div class="form-control-container js-cart-quantity-container col px-1">
              <input type="number" class="form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux_variants" value="0" min="0" step="1" id="cart-quantity-input">
            </div>
            <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
            <span class="js-cart-quantity-btn form-quantity-icon btn" id="cart-quantity-btn-add"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
`

// This is the HTML of the quantity input
const stringQuantityInputLima = `
<div class="row m-0 align-items-center">
  <span class="js-cart-quantity-btn form-quantity-icon btn" id="substract-btn"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M486.4,269.27H25.6v-25.6H486.4Z"></path></svg></span>
  <div class="form-control-container js-cart-quantity-container col px-1">
    <input type="number" class="form-control js-cart-quantity-input text-center form-control-inline" name="quantity_aux" value="0" min="0" step="1">
  </div>
  <span class="js-cart-input-spinner cart-item-spinner" style="display: none;"><svg class="icon-inline icon-spin svg-icon-text" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M460.115 373.846l-6.941-4.008c-5.546-3.202-7.564-10.177-4.661-15.886 32.971-64.838 31.167-142.731-5.415-205.954-36.504-63.356-103.118-103.876-175.8-107.701C260.952 39.963 256 34.676 256 28.321v-8.012c0-6.904 5.808-12.337 12.703-11.982 83.552 4.306 160.157 50.861 202.106 123.67 42.069 72.703 44.083 162.322 6.034 236.838-3.14 6.149-10.75 8.462-16.728 5.011z"></path></svg></span>
  <span class="js-cart-quantity-btn form-quantity-icon btn" id="add-btn"><svg class="icon-inline icon-w-12 icon-xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M268.8,268.8V486.4H243.2V268.8H25.6V243.2H243.2V25.6h25.6V243.2H486.4v25.6Z"></path></svg></span>
</div>
`